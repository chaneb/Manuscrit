\begin{thebibliography}{10}

\bibitem{alvis_springers_1982}
D.~Alvis and G.~Lusztig.
\newblock On {S}pringer's correspondence for simple groups of type {$E\sb{n}$}
  {$(n=6,\,7,\,8)$}.
\newblock {\em Math. Proc. Cambridge Philos. Soc.}, 92(1):65--78, 1982.
\newblock With an appendix by N. Spaltenstein.

\bibitem{bonnafe_quasi-isolated_2005}
C.~Bonnaf\'{e}.
\newblock Quasi-isolated elements in reductive groups.
\newblock {\em Comm. Algebra}, 33(7):2315--2337, 2005.

\bibitem{bonnafe_representations_2011}
C.~Bonnaf\'{e}.
\newblock {\em Representations of {${\rm SL}_2(\Bbb F_q)$}}, volume~13 of {\em
  Algebra and Applications}.
\newblock Springer-Verlag London, Ltd., London, 2011.

\bibitem{bonnafe_categories_2003}
C.~Bonnaf\'{e} and R.~Rouquier.
\newblock Cat\'{e}gories d\'{e}riv\'{e}es et vari\'{e}t\'{e}s de
  {D}eligne-{L}usztig.
\newblock {\em Publ. Math. Inst. Hautes \'{E}tudes Sci.}, (97):1--59, 2003.

\bibitem{MR0240238}
N.~Bourbaki.
\newblock {\em \'{E}l\'{e}ments de math\'{e}matique. {F}asc. {XXXIV}. {G}roupes
  et alg\`ebres de {L}ie. {C}hapitre {IV}: {G}roupes de {C}oxeter et syst\`emes
  de {T}its. {C}hapitre {V}: {G}roupes engendr\'{e}s par des r\'{e}flexions.
  {C}hapitre {VI}: syst\`emes de racines}.
\newblock Actualit\'{e}s Scientifiques et Industrielles, No. 1337. Hermann,
  Paris, 1968.

\bibitem{broue_bloc_1989}
M.~Brou\'{e} and J.~Michel.
\newblock Blocs et s\'{e}ries de {L}usztig dans un groupe r\'{e}ductif fini.
\newblock {\em J. Reine Angew. Math.}, 395:56--67, 1989.

\bibitem{cabanes_representation_2004}
M.~Cabanes and M.~Enguehard.
\newblock {\em Representation theory of finite reductive groups}, volume~1 of
  {\em New Mathematical Monographs}.
\newblock Cambridge University Press, Cambridge, 2004.

\bibitem{carter_finite_1985}
R.~W. Carter.
\newblock {\em Finite groups of {L}ie type}.
\newblock Wiley Classics Library. John Wiley \& Sons, Ltd., Chichester, 1993.
\newblock Conjugacy classes and complex characters, Reprint of the 1985
  original, A Wiley-Interscience Publication.

\bibitem{ChangRee74}
B.~Chang and R.~Ree.
\newblock The characters of {$G\sb{2}(q)$}, 1974.

\bibitem{CR892316}
C.~W. Curtis and I.~Reiner.
\newblock {\em Methods of representation theory. {V}ol. {II}}.
\newblock Pure and Applied Mathematics (New York). John Wiley \& Sons, Inc.,
  New York, 1987.

\bibitem{MR463174}
P.~Deligne.
\newblock {\em Cohomologie \'{e}tale}, volume 569 of {\em Lecture Notes in
  Mathematics}.
\newblock Springer-Verlag, Berlin, 1977.
\newblock S\'{e}minaire de g\'{e}om\'{e}trie alg\'{e}brique du Bois-Marie SGA
  $4\frac{1}{2}$.

\bibitem{denoncin_stable_2017}
D.~Denoncin.
\newblock Stable basic sets for finite special linear and unitary groups.
\newblock {\em Adv. Math.}, 307:344--368, 2017.

\bibitem{digne_representations_1991}
F.~Digne and J.~Michel.
\newblock {\em Representations of finite groups of {L}ie type}, volume~21 of
  {\em London Mathematical Society Student Texts}.
\newblock Cambridge University Press, Cambridge, 1991.

\bibitem{digne_representations_2019}
F.~Digne and J.~Michel.
\newblock {\em Representations of {Finite} {Groups} of {Lie} {Type}, second
  edition}.
\newblock Cambridge University Press, to appear.

\bibitem{dipper_decomposition_1985}
R.~Dipper.
\newblock On the {Decomposition} {Numbers} of the {Finite} {General} {Linear}
  {Groups}.
\newblock {\em Transactions of the American Mathematical Society},
  290(1):315--344, 1985.

\bibitem{dipper_decomposition_1985-1}
R.~Dipper.
\newblock On the decomposition numbers of the finite general linear groups.
  {II}.
\newblock {\em Trans. Amer. Math. Soc.}, 292(1):123--133, 1985.

\bibitem{dudas_note_2013}
O.~Dudas.
\newblock A note on decomposition numbers for groups of {L}ie type of small
  rank.
\newblock {\em J. Algebra}, 388:364--373, 2013.

\bibitem{dudas_decomposition_2014}
O.~Dudas and G.~Malle.
\newblock Decomposition matrices for exceptional groups at {$d=4$}.
\newblock {\em J. Pure Appl. Algebra}, 220(3):1096--1121, 2016.

\bibitem{geck_decomposition_1991}
M.~Geck.
\newblock On the decomposition numbers of the finite unitary groups in
  nondefining characteristic.
\newblock {\em Math. Z.}, 207(1):83--89, 1991.

\bibitem{geck_basic_1993}
M.~Geck.
\newblock Basic sets of {B}rauer characters of finite groups of {L}ie type.
  {II}.
\newblock {\em J. London Math. Soc. (2)}, 47(2):255--268, 1993.

\bibitem{geck_basic_1994}
M.~Geck.
\newblock Basic sets of {B}rauer characters of finite groups of {L}ie type.
  {III}.
\newblock {\em Manuscripta Math.}, 85(2):195--216, 1994.

\bibitem{10.2307/118160}
M.~Geck.
\newblock Character sheaves and generalized {G}elfand-{G}raev characters.
\newblock {\em Proc. London Math. Soc. (3)}, 78(1):139--166, 1999.

\bibitem{Geck1999Jan}
M.~Geck.
\newblock Character sheaves and generalized {G}elfand-{G}raev characters.
\newblock {\em Proc. London Math. Soc. (3)}, 78(1):139--166, 1999.

\bibitem{ho2004finite}
M.~Geck.
\newblock On the {S}chur indices of cuspidal unipotent characters.
\newblock In {\em Finite groups 2003}, pages 87--104. Walter de Gruyter,
  Berlin, 2004.

\bibitem{geck_unipotent_2008}
M.~Geck and D.~H\'{e}zard.
\newblock On the unipotent support of character sheaves.
\newblock {\em Osaka J. Math.}, 45(3):819--831, 2008.

\bibitem{geck_basic_1991}
M.~Geck and G.~Hiss.
\newblock Basic sets of {B}rauer characters of finite groups of {L}ie type.
\newblock {\em J. Reine Angew. Math.}, 418:173--188, 1991.

\bibitem{geck_modular_1997}
M.~Geck and G.~Hiss.
\newblock Modular representations of finite groups of {L}ie type in
  non-defining characteristic.
\newblock In {\em Finite reductive groups ({L}uminy, 1994)}, volume 141 of {\em
  Progr. Math.}, pages 195--249. Birkh\"{a}user Boston, Boston, MA, 1997.

\bibitem{Hezard2004Jan}
D.~H{\ifmmode\acute{e}\else\'{e}\fi}zard.
\newblock {\em {Sur le support unipotent des
  faisceaux-caract{\ifmmode\grave{e}\else\`{e}\fi}res}}.
\newblock Lyon 1, Jan 2004.

\bibitem{HISS1990371}
G.~Hiss and J.~Shamash.
\newblock {$3$}-blocks and {$3$}-modular characters of {$G_2(q)$}.
\newblock {\em J. Algebra}, 131(2):371--387, 1990.

\bibitem{Hiss20102BLOCKSA2}
G.~Hiss and J.~Shamash.
\newblock {$2$}-blocks and {$2$}-modular characters of the {C}hevalley groups
  {$G_2(q)$}.
\newblock {\em Math. Comp.}, 59(200):645--672, 1992.

\bibitem{James1984Dec}
G.~James and A.~Kerber.
\newblock {\em The representation theory of the symmetric group}, volume~16 of
  {\em Encyclopedia of Mathematics and its Applications}.
\newblock Addison-Wesley Publishing Co., Reading, Mass., 1981.
\newblock With a foreword by P. M. Cohn, With an introduction by Gilbert de B.
  Robinson.

\bibitem{kawanaka_generalized_1985}
N.~Kawanaka.
\newblock Generalized {G}elfand-{G}raev representations and {E}nnola duality.
\newblock In {\em Algebraic groups and related topics ({K}yoto/{N}agoya,
  1983)}, volume~6 of {\em Adv. Stud. Pure Math.}, pages 175--206.
  North-Holland, Amsterdam, 1985.

\bibitem{kawanaka1986generalized}
N.~Kawanaka.
\newblock Generalized {G}elfand-{G}raev representations of exceptional simple
  algebraic groups over a finite field. {I}.
\newblock {\em Invent. Math.}, 84(3):575--616, 1986.

\bibitem{kleshchev_representations_2009}
A.~S. Kleshchev and P.~H. Tiep.
\newblock Representations of finite special linear groups in non-defining
  characteristic.
\newblock {\em Adv. Math.}, 220(2):478--504, 2009.

\bibitem{liebeck_unipotent_2012}
M.~W. Liebeck and G.~M. Seitz.
\newblock {\em Unipotent and nilpotent classes in simple algebraic groups and
  {L}ie algebras}, volume 180 of {\em Mathematical Surveys and Monographs}.
\newblock American Mathematical Society, Providence, RI, 2012.

\bibitem{Lusztig1978}
G.~Lusztig.
\newblock {\em Representations of finite {C}hevalley groups}, volume~39 of {\em
  CBMS Regional Conference Series in Mathematics}.
\newblock American Mathematical Society, Providence, R.I., 1978.
\newblock Expository lectures from the CBMS Regional Conference held at
  Madison, Wis., August 8--12, 1977.

\bibitem{LUSZTIG1979323}
G.~Lusztig.
\newblock A class of irreducible representations of a {W}eyl group.
\newblock {\em Nederl. Akad. Wetensch. Indag. Math.}, 41(3):323--335, 1979.

\bibitem{LUSZTIG1981169}
G.~Lusztig.
\newblock Green polynomials and singularities of unipotent classes.
\newblock {\em Adv. in Math.}, 42(2):169--178, 1981.

\bibitem{lusztig_g._characters_1984}
G.~Lusztig.
\newblock Characters of reductive groups over a finite field, 1984.

\bibitem{Lusztig1984}
G.~Lusztig.
\newblock Intersection cohomology complexes on a reductive group.
\newblock {\em Invent. Math.}, 75(2):205--272, 1984.

\bibitem{LUSZTIG1985193}
G.~Lusztig.
\newblock Character sheaves i.
\newblock {\em Advances in Mathematics}, 56(3):193 -- 237, 1985.

\bibitem{LUSZTIG1985226}
G.~Lusztig.
\newblock Character sheaves ii.
\newblock {\em Advances in Mathematics}, 57(3):226 -- 265, 1985.

\bibitem{LUSZTIG1986103}
G.~Lusztig.
\newblock Character sheaves, v.
\newblock {\em Advances in Mathematics}, 61(2):103 -- 155, 1986.

\bibitem{AST_1988__168__157_0}
G.~Lusztig.
\newblock On the representations of reductive groups with disconnected centre.
\newblock {\em Ast\'{e}risque}, (168):10, 157--166, 1988.
\newblock Orbites unipotentes et repr\'{e}sentations, I.

\bibitem{lusztig_unipotent_1992}
G.~Lusztig.
\newblock A unipotent support for irreducible representations.
\newblock {\em Adv. Math.}, 94(2):139--179, 1992.

\bibitem{malle_linear_2011}
G.~Malle and D.~Testerman.
\newblock {\em Linear {Algebraic} {Groups} and {Finite} {Groups} of {Lie}
  {Type}}.
\newblock Cambridge University Press, Sept. 2011.

\bibitem{michel_development_2015}
J.~Michel.
\newblock The development version of the {\tt {chevie}} package of {\tt
  {gap}3}.
\newblock {\em J. Algebra}, 435:308--336, 2015.

\bibitem{PMIHES_1994__80__81_0}
J.~Rickard.
\newblock Finite group actions and \'{e}tale cohomology.
\newblock {\em Inst. Hautes \'{E}tudes Sci. Publ. Math.}, (80):81--94 (1995),
  1994.

\bibitem{Rouquier2002Nov}
R.~Rouquier.
\newblock {Complexes de cha{\ifmmode\imath\else\i\fi}nes
  {\ifmmode\acute{e}\else\'{e}\fi}tales et courbes de
  Deligne{\textendash}Lusztig}.
\newblock {\em Journal of Algebra}, 257(2):482--508, Nov 2002.

\bibitem{serre_representations_1967}
J.-P. Serre.
\newblock Repr\'{e}sentations lin\'{e}aires des groupes finis, 1978.

\bibitem{shoji_conjugacy_1974}
T.~Shoji.
\newblock The conjugacy classes of {C}hevalley groups of type {$(F\sb{4})$}
  over finite fields of characteristic {$p\not=2$}.
\newblock {\em J. Fac. Sci. Univ. Tokyo Sect. IA Math.}, 21:1--17, 1974.

\bibitem{shoji_springer_1979}
T.~Shoji.
\newblock On the {S}pringer representations of the {W}eyl groups of classical
  algebraic groups.
\newblock {\em Comm. Algebra}, 7(16):1713--1745, 1979.

\bibitem{doi:10.1080/00927878008822466}
T.~Shoji.
\newblock On the {S}pringer representations of {C}hevalley groups of type
  {$F\sb{4}$}.
\newblock {\em Comm. Algebra}, 8(5):409--440, 1980.

\bibitem{SHOJI1995244}
T.~Shoji.
\newblock Character sheaves and almost characters of reductive groups. {I},
  {II}.
\newblock {\em Adv. Math.}, 111(2):244--313, 314--354, 1995.

\bibitem{SPALTENSTEIN1977203}
N.~Spaltenstein.
\newblock On the fixed point set of a unipotent element on the variety of
  {B}orel subgroups.
\newblock {\em Topology}, 16(2):203--204, 1977.

\bibitem{springer_trigonometric_1976}
T.~A. Springer.
\newblock Trigonometric sums, {G}reen functions of finite groups and
  representations of {W}eyl groups.
\newblock {\em Invent. Math.}, 36:173--207, 1976.

\bibitem{springer_construction_1978}
T.~A. Springer.
\newblock A construction of representations of {W}eyl groups.
\newblock {\em Invent. Math.}, 44(3):279--293, 1978.

\bibitem{srinivasan_characters_1968}
B.~Srinivasan.
\newblock The characters of the finite symplectic group {${\rm Sp}(4,\,q)$}.
\newblock {\em Trans. Amer. Math. Soc.}, 131:488--525, 1968.

\bibitem{taylor_unipotent_2012}
J.~Taylor.
\newblock {\em On {Unipotent} {Supports} of {Reductive} {Groups} {With} a
  {Disconnected} {Centre}}.
\newblock phdthesis, University of Aberdeen, Apr. 2012.

\bibitem{taylor_unipotent_2013}
J.~Taylor.
\newblock On unipotent supports of reductive groups with a disconnected centre.
\newblock {\em J. Algebra}, 391:41--61, 2013.

\bibitem{taylor_generalized_2016}
J.~Taylor.
\newblock Generalized {G}elfand-{G}raev representations in small
  characteristics.
\newblock {\em Nagoya Math. J.}, 224(1):93--167, 2016.

\bibitem{white_2-decomposition_1990}
D.~L. White.
\newblock The {$2$}-decomposition numbers of {${\rm Sp}(4,q)$}, {$q$} odd.
\newblock {\em J. Algebra}, 131(2):703--725, 1990.

\end{thebibliography}
