\documentclass[a4paper, twoside, draft, 12pt]{report}

% Options possibles : 10pt, 11pt, 12pt (taille de la fonte)
%                     oneside, twoside (recto simple, recto-verso)
%                     draft, final (stade de développement)
\usepackage[a4paper]{geometry}% Réduire les marges
\geometry{margin=1in}

\usepackage[utf8]{inputenc}   
\usepackage[T1]{fontenc}      % Police contenant les caractères français
\usepackage[all]{xy}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{varioref}
\usepackage{amsthm}


\usepackage{enumitem}
\usepackage{array}
\usepackage{ stmaryrd }
\usepackage{xcolor}
\usepackage{tikz}
\usepackage[colorlinks=true]{hyperref}
\usepackage{longtable}
\usepackage{tabularx}
\usepackage[all]{xy}
\usepackage{tikz-cd}
\allowdisplaybreaks

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}



% \pagestyle{headings}        % Pour mettre des entêtes avec les titres
                              % des sections en haut de page

\newtheorem{The}{Theorem}[section]
\newtheorem{Con}{Conjecture}[section]
\newtheorem{Pro}[The]{Proposition}
\newtheorem{Cor}[The]{Corollary}
\newtheorem{Lem}[The]{Lemma}
\theoremstyle{definition}
\newtheorem{Def}[The]{Definition}
\newtheorem{Rk}[The]{Remark}
\newtheorem{Ex}[The]{Example}

\newcommand{\F}{\mathbb{F}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\M}{\mathbf{L}}
\newcommand{\G}{\mathbf{G}}
\newcommand{\T}{\mathbf{T}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\B}{\mathbf{B}}
\newcommand{\Irr}{\mathrm{Irr}}
\newcommand{\psc}[2]{\left<#1\middle|#2\right>}

\renewcommand{\qedsymbol}{$\blacksquare$}
\renewenvironment{proof}{{\bfseries Proof.}}{\qed}

\newcommand{\overbar}[1]{\mkern 1.5mu\overline{\mkern-1.5mu#1\mkern-1.5mu}\mkern 1.5mu}

\title{Unipotent support and basic set}          
\author{Reda Chaneb}
\date{}                      
\sloppy                       % Ne pas faire déborder les lignes dans la marge



\begin{document}

\paragraph{Unipotent support and wave front set}
 Following \cite{lusztig_unipotent_1992}, we can associate to $\rho \in$ Irr$_K(G)$ a unique $F$-stable unipotent class $C_{\rho}$ of $\G$ satisfying the two following conditions:

\begin{enumerate}
\item $\sum_{x\in C_{\rho}^F} \rho(x) \neq 0$,
\item $C_{\rho}$ is of maximal dimension for the condition above.
\end{enumerate}

We say that $C_\rho$ is the \emph{unipotent support} of $\rho$. By using the GGGRs, we can also associate to $\rho$ another $F$-stable unipotent class $C_{\rho}^*$ of  $\G$, the \emph{wave front set} of $\rho$, which is the unique unipotent class satisfying the following conditions:

\begin{enumerate}
\item $(\rho, \gamma_u)\neq 0$ for some $u \in C_{\rho}^{*F}$,
\item$(\rho, \gamma_v) \neq 0$ implies $\dim C_v \leq  \dim C_{\rho}^*$,
\end{enumerate}
where $C_v$ is the unipotent class of $\G$ containing $v$. Actually, the unipotent support and the wave front set are closely related. More precisely, let $D_G$ be the Alvis-Curtis duality for representations of $G$ (see the definition in \cite[8.8]{digne_representations_1991}). It is known that $D_G$ maps an irreducible character to an irreducible character up to a sign. If $\rho \in \Irr_K(G)$, we denote by $\rho^*$ the irreducible character of $G$ such that $\rho^*= \pm D_G(\rho)$. We call $\rho^*$ the dual character of $\rho$. The relation between unipotent support and wave front set comes from this duality:
$$\forall \rho \in \Irr_K(G) \quad C^*_{\rho}=C_{\rho^*}.$$
Note that all these properties were first proved under the assumption that $p$ and $q$ were large enough. These were later generalised to the case where $p$ is good prime in \cite{taylor_generalized_2016}.
\end{document}
