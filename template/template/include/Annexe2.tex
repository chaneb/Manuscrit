% !TEX encoding = UTF-8 Unicode
\input{include/macros/macros-rubiks}

\selectlanguage{francais} % Pour que dans l'en-tête, "Annexe" soit en francais

\chapter{Du mélange des couleurs d'un Rubik's cube}

\selectlanguage{english} 

\begin{abstract}
For $n\geqslant 1$, we show that one can optimally distribute the colours of the $n\times n\times n$ Rubik's cube on its faces in $O(n)$ moves. We also provide explicit examples for small values of $n$.
\end{abstract}

\selectlanguage{francais}

\startcontents[ann2]
\printcontentschap[ann2]{1}

\section{Notion de mélange}

\subsection{Introduction}

Une fois résolu, le Rubik's cube -- comme tous les puzzles -- a vocation à être mélangé avant d'être de nouveau résolu. La question du \og mélange maximal\fg\ est évidement intrigante. À quelle point peut-on mélanger un Rubik's cube ? Cette notion est subjective, et une mesure classique du mélange est la distance d'une configuration, en nombre de mouvements, à l'état initial. Dans ce cas encore, il faut définir ce qu'est \og un mouvement\fg. Par exemple, dans le cas du cube $3\times 3\times 3$, il a récemment été démontré~\cite{godsnumber20} que le \emph{nombre de Dieu}, c'est-à-dire la plus grande distance possible d'une configuration à l'état résolu, vaut $20$, si l'on compte les demi-tours comme un seul coup. Si l'on considère en revanche que chaque quart de tour compte comme un mouvement, le nombre de Dieu vaut $26$~\cite{godsnumberqtm26}.

On étudie ici une notion plus visuelle du mélange. Précisément, on associe le mélange à la bonne répartition des couleurs sur toutes les faces. Par exemple, sur un Rubik's cube $3\times 3\times 3$, est-il possible que chaque couleur n'apparaisse au plus que $2$ fois sur chaque face ? La réponse est évidemment négative si l'on remplace $2$ par $1$, puisque chaque couleur apparaît $9$ fois, tandis qu'un cube n'a que $6$ faces. On généralise cette question aux Rubik's cube $n\times n\times n$ pour $n\geqslant 1$, en se demandant, lorsqu'il est possible de \og bien répartir\fg\ les couleurs, en combien de mouvements peut-on le faire. On commence donc par préciser la notion de mouvement.

\subsection{Notations}\label{rfzavdhjhb}

\noindent\begin{tabularx}{\columnwidth}{@{}Xc}
On définit un \emph{mouvement} comme tout quart de tour d'une couronne -- c'est-à-dire d'une \og tranche\fg\ -- du Rubik's cube. On a illustré ci-contre l'amorçage d'un mouvement sur le cube $5\times 5\times 5$ &
\adjustbox{valign=t}{\exemplemouvementquatre}
\end{tabularx}

\begin{definition}
Pour tout $n\geqslant 1$, on note $c_n$ le nombre minimal de mouvements nécessaires, partant du cube $n\times n\times n$ résolu, pour que chaque couleur n'apparaisse au plus que $\lceil n^2/6\rceil$ fois sur chaque face, lorsque cela est possible. Si ce n'est pas possible, on pose $c_n=+\infty$.
\end{definition}

\subsection{Résultats}

La question centrale est de savoir si tous les $c_n$ sont finis, et de les estimer. On parvient à monter cela, notamment par l'étude du cas $n=6$ qui joue un rôle particulier puisqu'il correspond à une répartition parfaite des couleurs. On donne par ailleurs des bornes explicites pour des petites valeurs de $n$. Précisément, on démontre les résultats suivants.

\begin{proposition}\label{tgezjfuhn}
On a les majorations suivantes.
\[\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}
\hline
$n$ & $2$ & $3$ & $4$ & $5$ & $6$ & $7$ & $8$ & $9$ & $10$ & $11$\\
\hline
$c_n\leqslant$ & $4$ & $5$ & $9$ & $10$ & $13$ & $13$ & $18$ & $21$ & $26$ & $28$\\
\hline
\end{tabular}\]
En particulier, il est possible que chaque couleur apparaisse exactement $6$ fois sur chacune des faces d'un Rubik's cube $6\times 6\times 6$.
\end{proposition}

\begin{proposition}\label{tyduzioanocn}
Pour tous $n, k\geqslant 1$, on a $c_{6nk}\leqslant nc_{6k}$.
\end{proposition}

\begin{theoreme}\label{sjkngskjdskd}
Il existe une constante $C>0$ vérifiant l'inégalité suivante. Pour $n\geqslant 2$, on a
\[\frac{5n}{4}\leqslant c_n\leqslant Cn.\]
\end{theoreme}

Il serait intéressant de préciser cette inégalité, et notamment la borne supérieure, qui peut être explicitée d'après notre preuve, mais qui fournirait une valeur de $C$ -- probablement -- très loin de la vérité.
\section{Démonstrations}

\subsection{Étude des petits cubes}

\begin{proof}[Démonstration de la Proposition~\ref{tgezjfuhn}]
On démontre les majorations énoncées en donnant des transformations explicites, obtenues par ordinateur. La méthode employée est la suivante. Dans un premier temps, étant donné $n$, on mélange aléatoirement le Rubik's cube $n\times n\times n$ jusqu'à ce qu'il vérifie la condition sur le mélange des couleurs. Une fois une bonne configuration atteinte, on obtient une première majoration pour $c_n$, mettons $c_n\leqslant M$. On continue alors la recherche de manière aléatoire, mais en réinitialisant le cube à chaque fois que $M-1$ mouvements ont été effectués.

Dans un second temps, on s'est rendu compte qu'il était beaucoup plus facile d'atteindre une bonne configuration de mélange en partant d'un Rubik's cube résolu, plutôt que d'une configuration aléatoire. Cela se remarque quand on essaie à la main de n'obtenir au plus que 2 fois chaque couleur sur toutes les faces d'un Rubik's cube $3\times 3\times 3$ mélangé -- ce n'est pas si facile ! On a donc choisi de réinitialiser le cube après un nombre arbitraire de mélange même si la configuration atteinte ne satisfaisait pas à l'hypothèse de mélange désirée. C'est grâce à cela qu'on a pu obtenir une majoration pour $c_6$. En effet, cela prend en général quelques minutes lorsqu'on recommence tout les 20 mouvements -- par exemple -- alors que si l'on mélange aléatoirement jusqu'à trouver une bonne configuration, cela semble beaucoup plus long.

On précise que l'on n'a pas cherché à minorer dans les détail les $c_n$ pour de petites valeurs de $n$. Cela est possible, par exemple, si l'on est capable de visiter tous les mélanges de moins de mouvements que la majoration obtenue. Cependant, au vu du \emph{problème du collectionneur de vignettes}, des symétries du problème et des bornes obtenues, il est presque certain que les majorations énoncées sont des égalités pour $n\leqslant 5$.

\noindent\begin{tabularx}{\columnwidth}{@{}X@{}c@{}}
\hspace{.5cm} Afin de décrire les solutions trouvées, on détaille la notation des quarts de tour sur un Rubik's cube $n\times n\times n$ avec $n\geqslant 1$, à l'aide du schéma ci-contre. On fixe arbitrairement l'orientation du cube, puis on nomme les $6$ faces $F$ (\emph{front}), $B$ (\emph{back}), $R$ (\emph{right}), $L$ (\emph{left}), $U$ (\emph{up}),  et $D$ (\emph{down}).  Le sens naturel de rotation d'une face est le sens des aiguilles d'une montre. Enfin, pour $1\leqslant j\leqslant n$, on note par exemple $R_j$ le quart de tour de la $j$-ième couronne de l'axe $R$-$L$ -- dans le sens des aiguilles  d'une montre --, comptée à partir de la face $R$. On a notamment $R_j=L_{n+1-j}^{-1}$. Par exemple, le mouvement illustré à la section~\ref{rfzavdhjhb} est $D_2$.
&
\adjustbox{valign=t}{\conventions{4}}
\end{tabularx}

Pour $n\leqslant 11$, les transformations suivantes satisfont au mélange optimal des couleurs -- cliquer pour vérifier sur la version digitale --, ce qui permet de conclure.
\begin{enumerate}[leftmargin=1cm, start=2, align=left]
% visualisation/test sur https://alg.cubing.net/
\item[$n=\liendeux$:] $R_1U_1^2B_1$,%2
% R U2 B
\item[$n=\lientrois$:] $F_1D_2L_2B_2U_1$,%3
% F 2-2Dw 2-2Lw 2-2Bw U
\item[$n=\lienquatre$:] $L_4R_2L_2D_1L_2F_2D_3U_3D_1$,%4
% R' 2-2Rw 2-2Lw D 2-2Lw 2-2Fw 3-3Dw 3-3Uw D
\item[$n=\liencinq$:] $R_1U_5D_5B_2D_2R_2L_2U_2F_2L_1$,%5
% R D' U' 2-2Bw 2-2Dw 2-2Rw 2-2Lw 2-2Uw 2-2Fw L
\item[$n=\liensix$:] $L_6R_6D_5^2U_4D_2L_4R_5D_6U_6R_2R_5R_6$,%6
% 6-6Lw 6-6Rw 5-5Dw2 4-4Uw 2-2Dw 4-4Lw 5-5Rw 6-6Dw 6-6Uw 2-2Rw 5-6Rw
\item[$n=\liensept$:] $R_5U_6D_5R_6B_6U_4B_5F_5L_5U_5U_6F_6L_6$,%7
% 5-5Rw 6-6Uw 5-5Dw 6-6Rw 6-6Bw 4-4Uw 5-5Bw 5-5Fw 5-5Lw 5-6Uw 6-6Fw 6-6Lw
\item[$n=\lienhuit$:] $L_8D_5D_7L_3R_3F_3U_3R_5U_7R_3R_4R_5F_5B_7D_3R_2U_1^2$,%8
% R' 5-5Dw 7-7Dw 3-3Lw 3-3Rw 3-3Fw 3-3Uw 5-5Rw 7-7Uw 3-5Rw 5-5Fw 7-7Bw 3-3Dw 2-2Rw U2
\item[$n=\lienneuf$:] $U_2U_9R_6R_8D_4L_6F_2F_5U_5D_4L_7F_8U_5U_7L_3D_7F_2B_7F_6L_6B_1$,%9
% 2-2Uw 9-9Uw  6-6Rw 8-8Rw 4-4Dw 6-6Lw 2-2Fw 5-5Fw 5-5Uw 4-4Dw 7-7Lw 8-8Fw 5-5Uw 7-7Uw 3-3Lw 7-7Dw 2-2Fw 7-7Bw 6-6Fw 6-6Lw B
\item[$n=\liendix$:] $R_7U_8R_9^2F_9D_5F_6^2R_4R_7B_3U_8F_2R_2F_9R_5L_5U_3D_6D_2B_7U_7F_7D_9D_5L_{10}$,%10
% 7-7Rw 8-8Uw 9-9Rw2 9-9Fw 5-5Dw 6-6Fw2 4-4Rw 7-7Rw 3-3Bw 8-8Uw 2-2Fw 2-2Rw 9-9Fw 5-5Rw 5-5Lw 3-3Uw 6-6Dw 2-2Dw 7-7Bw 7-7Uw 7-7Fw 9-9Dw 5-5Dw R'
\item[$n=\lienonze$:] $U_4F_4B_7U_7U_{10}B_3D_7U_8R_4B_{10}U_8F_8F_{10}D_2F_8U_2U_4D_3R_2U_3F_6R_4L_7L_6F_{10}R_3L_3B_9$.%11
% 4-4Uw 4-4Fw 7-7Bw 7-7Uw 10-10Uw 3-3Bw 7-7Dw 8-8Uw 4-4Rw 10-10Bw 8-8Uw 8-8Fw 10-10Fw 2-2Dw 8-8Fw 2-2Uw 4-4Uw 3-3Dw 2-2Rw 3-3Uw 6-6Fw 4-4Rw 6-7Lw 10-10Fw 3-3Rw 3-3Lw 9-9Bw
\end{enumerate}
\end{proof}

\subsection{Cas général}

On démontre ici le Théorème~\ref{sjkngskjdskd}. On note que la Proposition~\ref{tyduzioanocn} s'obtient directement par identification du cube de taille $6nk$ au cube de taille $6k$, en regroupant les étages par paquets de $n$.

\begin{proof}[Démonstration du Théorème~\ref{sjkngskjdskd}]
On commence par minorer $c_n$ pour $n\geqslant 2$. Partant de la configuration initiale, pour que le cube $n\times n\times n$ soit bien mélangé, il faut que sur chaque face, au moins $n^2-\lceil n^2/6\rceil$ facettes soient déplacées. Puisqu'un quart de tour peut au plus retirer $n$ vignettes d'une même couleur sur un face donnée, il faut que chaque face soit traversée par au moins
\[k:=\left\lceil\frac{n^2-\left\lceil\frac{n^2}{6}\right\rceil}{n}\right\rceil\]
mouvements. En considérant trois faces qui forment un coin, on obtient alors
\[c_n\geqslant\frac{3k}{2}+\frac{\mathds{1}_{2\nmid k}}{2}.\]
Puisque le membre de droite est supérieur ou égal à $5n/4$ lorsque $n\geqslant 2$ et $n\neq 5$, on obtient la minoration désirée en vérifiant à l'ordinateur que $6$ coups ne suffisent pas à mélanger convenablement les couleurs d'un Rubik's cube $5\times 5\times 5$. Cela se fait sans problème puisqu'il y a au plus $30^6$ mélanges de $6$ mouvements lorsque $n=5$.\\

On montre maintenant que l'on a bien $c_n\ll n$ pour $n\geqslant 2$. On suppose $n\geqslant 7$ et $6\nmid n$, les autres cas étant résolus. On traite différemment les cas $n\equiv r\mod 6$ selon les valeurs de $r\in[1, 5]$. Dans tous les cas, on commence par représenter le cube $n\times n\times n$ par le cube de taille $6+r$. Pour cela, on identifie le cube centré de taille $6\lfloor n/6\rfloor$ au cube $6\times 6\times 6$. Lorsque $r=2$ par exemple, cela revient d'une part à considérer les étages extérieurs, et d'autre part à \og coller\fg\ les étages $2$ à $n-1$ par paquets de $\lfloor n/6\rfloor$. Si $r=4$, le procédé est le même, mais avec $2$ étages sur les bords. Lorsque $r$ est impair, on considère à part la rangée centrale. Par exemple, lorsque $r=5$, on \og colle\fg\ les étages $3$ à $\lfloor n/2\rfloor$ et $\lfloor n/2\rfloor +2$ à $n-2$ par paquets de $\lfloor n/6\rfloor$, ce qui laisse $2$ rangées extérieurs de chaque côté, et une rangée centrale, nous permettant d'identifier le cube au $ 11\times 11\times 11$.

Pour $1\leqslant r\leqslant 5$, on note $S_r$ le sous-cube $6\times 6\times 6$ du Rubik's cube de taille $6+r$ décrit ci-dessus et $R_r$ le reste du cube. On partage $R_r$ en deux parties $R_r=S'_r\sqcup E_r$, où $S'_r$ contient exactement les parties de $R_r$ qui sont sur les étages définissant $S_r$. On illustre ce découpage ci-dessous. La partie bleue représente $S_r$ tandis que $S'_r$ est en jaune, et $E_r$ en orange.

\vspace{.5cm}
\noindent\begin{tabularx}{\columnwidth}{ccccc}
\defSSprimeE{7} & \defSSprimeE{8} & \defSSprimeE{9} & \defSSprimeE{10} & \defSSprimeE{11} \\
$r=1$ & $r=2$ & $r=3$ & $r=4$ & $r=5$
\end{tabularx}
\vspace{.2cm}

Puisque $c_6<+\infty$, il suffit de montrer que l'on peut répartir parfaitement les couleurs de $S'_r$, et celles de $E_r$ de manière optimale, tout en préservant $S_r$, à partir de n'importe quelle configuration atteignable du cube de taille $6+r$ ; c'est est une conséquence de~\cite{cubologynxnxn}.

\end{proof}

On note que les mélanges obtenus par cette méthode de majoration sont très \og ordonnés\fg, puisqu'ils consistent essentiellement en blocs de côtés $\lfloor n/6\rfloor$.\\

\noindent\textbf{Remerciements.} Merci aux doctorants du laboratoire pour m'avoir prêté la puissance de calcul de leur ordinateur de bureau, ainsi qu'à Omar pour avoir discuté avec moi de ce sujet.

\stopcontents[ann2]
