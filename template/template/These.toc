\select@language {french}
\select@language {francais}
\contentsline {chapter}{Remerciements}{v}{chapter*.1}
\contentsline {chapter}{R\IeC {\'e}sum\IeC {\'e}/Abstract}{vii}{chapter*.3}
\select@language {english}
\select@language {francais}
\contentsline {chapter}{Introduction}{5}{chapter*.6}
\contentsline {section}{\numberline {I.1}Un mod\IeC {\`e}le probabiliste pour les entiers et quelques conjectures}{6}{section.0.1}
\contentsline {subsection}{\numberline {I.1.1}Ind\IeC {\'e}pendance des nombres premiers}{6}{subsection.0.1.1}
\contentsline {subsection}{\numberline {I.1.2}Le mod\IeC {\`e}le de Cram\IeC {\'e}r}{7}{subsection.0.1.2}
\contentsline {subsection}{\numberline {I.1.3}Quelques conjectures}{7}{subsection.0.1.3}
\contentsline {subsection}{\numberline {I.1.4}Limites du mod\IeC {\`e}le}{8}{subsection.0.1.4}
\contentsline {section}{\numberline {I.2}\IeC {\'E}tat de l'art}{8}{section.0.2}
\contentsline {subsection}{\numberline {I.2.1}Petits intervalles}{8}{subsection.0.2.1}
\contentsline {subsection}{\numberline {I.2.2}Syst\IeC {\`e}mes translat\IeC {\'e}s}{11}{subsection.0.2.2}
\contentsline {section}{\numberline {I.3}R\IeC {\'e}sultats et m\IeC {\'e}thodes de la th\IeC {\`e}se}{12}{section.0.3}
\contentsline {subsection}{\numberline {I.3.1}Petits intervalles}{12}{subsection.0.3.1}
\contentsline {subsection}{\numberline {I.3.2}Syst\IeC {\`e}mes translat\IeC {\'e}s}{16}{subsection.0.3.2}
\contentsline {section}{\numberline {I.4}Plan de la th\IeC {\`e}se}{17}{section.0.4}
\contentsline {part}{I\hspace {1em}Nombre de facteurs premiers dans presque tous les petits intervalles}{19}{part.1}
\contentsline {chapter}{\numberline {1}Th\IeC {\'e}or\IeC {\`e}me d'Erd\H {o}s-Kac dans presque tous les petits intervalles}{21}{chapter.1}
\select@language {english}
\select@language {francais}
\contentsline {section}{\numberline {1.1}Introduction}{21}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Pr\IeC {\'e}sentation des r\IeC {\'e}sultats}{21}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Notations}{24}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Quelques lemmes classiques}{24}{section.1.2}
\contentsline {section}{\numberline {1.3}R\IeC {\'e}partition de $\omega $ dans les petits intervalles}{25}{section.1.3}
\contentsline {section}{\numberline {1.4}Lois locales}{28}{section.1.4}
\contentsline {section}{\numberline {1.5}Estimation du type Matom\"{a}ki-Radziwi\l \l }{28}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}R\IeC {\'e}sultats pr\IeC {\'e}liminaires}{28}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Application de la m\IeC {\'e}thode de Matom\IeC {\"a}ki-Radziwi\l \l }{32}{subsection.1.5.2}
\contentsline {chapter}{\numberline {2}Lois locales de la fonction $\omega $ dans presque tous les petits intervalles}{35}{chapter.2}
\select@language {english}
\select@language {francais}
\contentsline {section}{\numberline {2.1}Introduction}{35}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}R\IeC {\'e}sultats}{35}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Aper\IeC {\c c}u de la m\IeC {\'e}thode}{38}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Notations}{39}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Lemmes utiles}{39}{section.2.2}
\contentsline {section}{\numberline {2.3}R\IeC {\'e}sultat g\IeC {\'e}n\IeC {\'e}ral}{41}{section.2.3}
\contentsline {section}{\numberline {2.4}M\IeC {\'e}thode de Matom\IeC {\"a}ki-Radziwi\l \l \ adapt\IeC {\'e}e}{43}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Premi\IeC {\`e}re simplification}{43}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Utilisation de la borne de Parseval}{44}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Factorisation du polyn\IeC {\^o}me de Dirichlet associ\IeC {\'e} \IeC {\`a} $\mathcal {A}\cap {\mathcal {S}}$}{45}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Majoration de l'int\IeC {\'e}grale}{46}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Application aux entiers ayant $k\asymp \qopname \relax o{log}_2 X$ facteurs premiers}{49}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Propri\IeC {\'e}t\IeC {\'e}s classiques}{49}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Crible bi-dimensionnel pour les entiers ayant $k$ facteurs premiers}{55}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Application du Th\IeC {\'e}or\IeC {\`e}me~\ref {th1}}{59}{subsection.2.5.3}
\contentsline {section}{\numberline {2.6}Entiers friables}{60}{section.2.6}
\contentsline {section}{\numberline {2.7}Entiers avec un petit nombre de facteurs premiers}{65}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Red\IeC {\'e}finition de $\mathcal {S}$}{66}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}D\IeC {\'e}monstration du Th\IeC {\'e}or\IeC {\`e}me~$\ref {petitsk}$}{66}{subsection.2.7.2}
\contentsline {part}{II\hspace {1em}Syst\IeC {\`e}mes translat\IeC {\'e}s}{77}{part.2}
\contentsline {chapter}{\numberline {3}Concentrations simultan\IeC {\'e}es de fonctions additives}{79}{chapter.3}
\select@language {english}
\select@language {francais}
\contentsline {section}{\numberline {3.1}Introduction et \IeC {\'e}nonc\IeC {\'e} des r\IeC {\'e}sultats}{79}{section.3.1}
\contentsline {section}{\numberline {3.2}Majoration}{81}{section.3.2}
\contentsline {section}{\numberline {3.3}Minoration}{89}{section.3.3}
\contentsline {chapter}{\numberline {4}Th\IeC {\'e}or\IeC {\`e}me d'Erd\H {o}s-Kac pour les translat\IeC {\'e}s d'entiers ayant $k$ facteurs premiers}{93}{chapter.4}
\select@language {english}
\select@language {francais}
\contentsline {section}{\numberline {4.1}Pr\IeC {\'e}sentation des r\IeC {\'e}sultats}{93}{section.4.1}
\contentsline {section}{\numberline {4.2}\IeC {\'E}tude de la s\IeC {\'e}rie g\IeC {\'e}n\IeC {\'e}ratrice}{94}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Moyenne d'une fonction multiplicative sur $\mathcal {E}_k(x)$}{94}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Calcul de la s\IeC {\'e}rie g\IeC {\'e}n\IeC {\'e}ratrice}{96}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Lois locales et r\IeC {\'e}partition}{98}{section.4.3}
\contentsline {chapter}{Appendice}{}{section*.8}
\select@language {francais}
\contentsline {chapter}{\numberline {A}Highest perfect power of a product of integers less than $x$}{105}{appendix.A}
\select@language {english}
\select@language {francais}
\select@language {francais}
\contentsline {chapter}{Bibliographie}{107}{appendix*.9}
\select@language {english}
\select@language {francais}
\contentsfinish 
