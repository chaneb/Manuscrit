\documentclass[a4paper, twoside, draft, 12pt]{report}

% Options possibles : 10pt, 11pt, 12pt (taille de la fonte)
%                     oneside, twoside (recto simple, recto-verso)
%                     draft, final (stade de développement)
\usepackage[a4paper]{geometry}% Réduire les marges
\geometry{margin=1in}

\usepackage[utf8]{inputenc}   
\usepackage[T1]{fontenc}      % Police contenant les caractères français
\usepackage[all]{xy}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{varioref}
\usepackage{amsthm}


\usepackage{enumitem}
\usepackage{array}
\usepackage{ stmaryrd }
\usepackage{xcolor}
\usepackage{tikz}
\usepackage[colorlinks=true]{hyperref}
\usepackage{longtable}
\usepackage{tabularx}
\usepackage[all]{xy}
\usepackage{tikz-cd}
\allowdisplaybreaks

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}



% \pagestyle{headings}        % Pour mettre des entêtes avec les titres
                              % des sections en haut de page

\newtheorem{The}{Theorem}[section]
\newtheorem{Con}{Conjecture}[section]
\newtheorem{Pro}[The]{Proposition}
\newtheorem{Cor}[The]{Corollary}
\newtheorem{Lem}[The]{Lemma}
\theoremstyle{definition}
\newtheorem{Def}[The]{Definition}
\newtheorem{Rk}[The]{Remark}
\newtheorem{Ex}[The]{Example}

\newcommand{\F}{\mathbb{F}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\M}{\mathbf{L}}
\newcommand{\G}{\mathbf{G}}
\newcommand{\T}{\mathbf{T}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\B}{\mathbf{B}}
\newcommand{\Irr}{\mathrm{Irr}}
\newcommand{\psc}[2]{\left<#1\middle|#2\right>}

\renewcommand{\qedsymbol}{$\blacksquare$}
\renewenvironment{proof}{{\bfseries Proof.}}{\qed}

\newcommand{\overbar}[1]{\mkern 1.5mu\overline{\mkern-1.5mu#1\mkern-1.5mu}\mkern 1.5mu}

\title{Isolated classes}          
\author{Reda Chaneb}
\date{}                      
\sloppy                       % Ne pas faire déborder les lignes dans la marge


\begin{document}

We fix a connected reductive algebraic group $\G$, $\T \subseteq \G$ a maximal torus contained in a Borel subgroup $\B$. We denote by $X(\T)$ the group of characters and $Y(\T)$ the group of cocharacters. Let $W$ be the Weyl group of $G$, $\Phi \subset X(\T)$ the root system of $\G$ relatively to $\T$ , $\Phi^\vee$ the set of coroots, $\Delta \subseteq \Phi$ the basis of $\Phi$ relatively to $\B$. For $\alpha \in \Phi$, we denote by $s_\alpha \in W$ the corresponding reflection. Let $g \in \G$ and $g=su$ its Jordan decomposition, $g$ is called quasi-isolated if $C_\G(s)$ is not contained in a Levi subgroup of a proper parabolic subgroup of $\G$. We say that $g$ is isolated if $C_\G(s)^\circ$ is not contained in a Levi subgroup of a proper parabolic subgroup of $\G$. In [ref], Bonnafe provided a complete description of the conjugacy classes of semi simple quasi-isolated element in term of the root system of $\G$, the description of the Weyl groups of the centralizers, their order and a necessary and sufficient condition for a quasi-isolated element to be isolated.
\vspace{0.2cm}

Let us make the following remark about quasi isolated elements, let $\G_{ad}$ be the adjoint quotient of $\G$ and $\pi \G \to \G_{ad}$ the canonical morphism. Then according to [ref], is $g$ is quasi-isolated then $\pi(g)$ is quasi-isolated and $g$ is isolated if and only if  $\pi(g)$ is isolated. We are interested in the classification of quasi-isolated semisimple elements, according to the previous remark it is enough to focus on adjoint groups. Even if the classification theorem has been stated for any connected reductive group,  we will suppose that $\G$ is adjoint and simple for more convenience. Let $\iota:(\Q / \Z)_{p'} \hookrightarrow \overbar{\F}_q^*$ an isomorphism (see [ref]) and $\tilde{\iota}$ the composition
\begin{tikzcd}%[row sep=large, column sep = large]
  \Q \arrow[r] & \Q/\Z \arrow[r] & (\Q/\Z)_{p'}  \arrow[r, "\iota"] &\overbar{\F}_q^*.
\end{tikzcd}
We define $\tilde{\iota}_\T:\Q \otimes_\Z Y(\T) \to \T,$ $x \otimes_\Z \lambda \mapsto \lambda(\tilde{\iota}(x))$. Let $\tilde{\alpha}$ be the highest root of of $\Phi$, let us write
$$\tilde{\alpha_i}=\sum\limits_{\alpha \in \Delta} n_\alpha \alpha, \quad n_\alpha \in \mathbb{N}.$$
Let $\alpha_0:=-\tilde{\alpha}$ and $\tilde{\Delta}=\Delta \cup \{ \alpha_0 \}$. For $\alpha \in \Delta$ we denote by $\omega_{\alpha}^\vee \in Y(\T)$ the corresponding fundamental dominant coweight. By convention, we set $\omega_{\alpha_0}^\vee=0$ and $n_{\alpha_0}=1$. We denote by $\mathcal{A}$ the subgroup of $W$ whice leaves $\tilde{\Delta}$ stable, ie:

$$\mathcal{A}:=\{w \in W,  w(\tilde{\Delta})=\tilde{\Delta} \}$$. Let $\tilde{\Delta}_{p'}$ be the subset of $\tilde{Delta}$ consisting of elements $\alpha \in \tilde{\Delta}$ such that $\frac{\omega}{n_\alpha} \in \Z_{(p)} \otimes_\Z \Z \Phi^\vee$.  Let $\mathcal{Q}(\G)_{p'}$ the set of subset $\Omega \subseteq \tilde{\Delta}_{p'}$ such that the stabilizer of $\Omega$ in $\mathcal{A}$ acts transitively on $\Omega$ and such that $p$ does not divide $|\Omega|$. Let $\Omega \in  \mathcal{Q}(\G)_{p'}$, we define

$$t_\Omega:=\tilde{\iota}_\T\left (\sum \limits_{\alpha \in \Omega} \frac{1}{n_\alpha|\Omega|} \omega_{\alpha_0}^\vee \right)$$
\begin{The}[Bonnafe] Suppose that $\G$ is adjoint and simple. The the following holds:
  \begin{itemize}
  \item The map  $\mathcal{Q}(\G)_{p'} \to \T, \Omega \mapsto t_\Omega$ induces a bijections between the set or orbits of $\mathcal{A}_{p'}$-orbits of  $\mathcal{Q}(\G)_{p'}$ and the set of conjugacy classes of quasi-isolated semisimple elements in $\G$ 
  \item For $\Omega \in  \mathcal{Q}(\G)_{p'}$, we have
    \begin{enumerate}
    \item The Weyl group of $C_\G(t_\Omega)^\circ$ is generated by the reflections $s_\alpha$ for $\alpha \in \tilde{\Delta} \setminus \Omega$
    \item $n_\alpha$ is constant for $\alpha \in \Omega$ and the order of $t_\omega$ is $n_\alpha |\Omega|$.
    \end{enumerate}
  \end{itemize}
  
\end{The}

We put here a table describing quasi-isolated elements in simple adjoint classical groups. The second column describes the elements of  $\mathcal{Q}(\G)_{p'}$, the third one tells us for each $p$ $\Omega \in \mathcal{Q}(\G)_{p'}$, $o(t_\Omega)$ is the order of $t_\Omega$. We follow the notations of the tables of [ref]: we write $\Delta=\{\alpha_1,...,\alpha_n \}$. The original table of Bonnafé, for $\Omega:=\{alpha_{n/2} \}$ in $D_n$, describes the order of $t_\Omega$ as 4. Hower, in [ref] it is remarked that this is element is actually of order 2.

$$\begin{array}{|clcccc|}
    \hline
     \G & \Omega & p & o(t_\Omega) & C_G(t_\Omega)^\circ & \text{Isolated?}  \\
     \hline
   A_n & \{\alpha_{j(n+1)/d}|0 \leq j \leq d-1 \} & p \nmid d & d & (A_{(n+1)/d}-1)^d & \text{yes iff } d=1\\   
        & \text{for } d \mid n+1 & & & & \\   
        & & & & & \\   
   B_n & \{\alpha_0 \} & & 1 & B_n & \text{yes} \\   
        & \{\alpha_0, \alpha_1 \} & p \neq 2 & 2 & B_{n-1} &  \text{no} \\
        & \{\alpha_d \},2 \leq d \leq n & p \neq 2 & 2 & D_d \times B_{n-d} & \text{yes} \\
        & & & & & \\
   C_n & \{ \alpha_0 \} & & 1 & C_n & \text{yes} \\   
        & \{ \alpha_d \}, 1 \leq d < n/2 & p \neq 2 & 2 & C_d \times C_{n-d} & \text{yes} \\
        & \{ \alpha_{n/2} \} \text{ ($n$ even)} & p \neq 2 & 2 & C_{n/2} \times C_{n/2} & \text{yes} \\
        & \{ \alpha_0, \alpha_n \} & p  \neq 2 & 2 & A_{n-1} & \text{no} \\
        & \{ \alpha_d, \alpha_{n-d} \}, 1 \leq d <n/2 & p \neq 2 & 4 & (C_d)^2 \times A_{n-2d-1} & \text{no} \\
        & & & & & \\
   D_n & \{\alpha_0 \} & & 1 & D_n & \text{yes} \\
        & \{ \alpha_d \}, 1 \leq d < n/2 & p \neq 2 & 2 & D_d \times D_{n-d} & \text{yes} \\
        & \{ \alpha_{n/2} \} \text{ ($n$ even)} & p \neq 2 & 2 & D_{n/2} \times D_{n/2} & \text{yes} \\   
        & \{ \alpha_d, \alpha_{n-d} \}, 1 \leq d <n/2 & p \neq 2 & 4 & (D_d)^2 \times A_{n-2d-1} & \text{no} \\
        & \{ \alpha_0, \alpha_1, \alpha_{n-1}, \alpha_n \} & p \neq 2 & 4 & A_{n-3} & \text{no} \\
        & \{\alpha_0, \alpha_1 \} & p \neq 2 & 2 & A_{n-1} & \text{no} \\
        & \{\alpha_0, \alpha_{n-1}  \} \text{ ($n$ even)}  & p \neq 2 & 2 & A_{n-1} & \text{no} \\
        & \{ \alpha_0, \alpha_n \} \text{ ($n$ even)}  & p  \neq 2 & 2 & A_{n-1} & \text{no} \\   
   \hline
   \end{array}$$

   
% $\begin{array}{|clcccc|}
%     \hline
%      \G & \Omega & p & o(t_\Omega) & C_G(t_\Omega)^\circ & \text{Isolated?}  \\
%      \hline
%    G_2 & \{\alpha_0 \} & & 1 & G_2 & \text{yes}\\
%         & \{\alpha_1 \} & & 2 & A_1 \times A_1 & \text{yes}\\
%         & \{\alpha_2 \} & & 3 & A_2 & \text{yes}\\   
%         & & & & & \\   
%    F_4 & \{ \alpha_0 \} & & 1 &  F_4 & \text{yes}\\
%     & \{ alpha_0 \}
%   \end{array}$


\end{document}
