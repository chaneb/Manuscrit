(TeX-add-style-hook
 "Classification"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (LaTeX-add-labels
    "sec:spec-repr-famil"
    "Ex:famil-B2"
    "sec:extension-Q"
    "sec:groups-assoc-famil"
    "Def:MGamma"
    "Rk:MGamma"
    "sec:lusztigs-theorem"
    "sec:non-unip-char"
    "Ex:springer"
    "table:Springer-Sp4"
    "table:Springer-G2"
    "sec:spring-constr-repr"
    "Ex:speclass"
    "sec:canonical-quotient"
    "Def:canonical-quotient"
    "sec:param-unip"
    "sec:param-irrg")
   (LaTeX-add-index-entries
    "$\\mathcal{M}(\\Omega)$"
    "$\\tilde{\\Omega}$"
    "$\\tilde{\\mathcal{M}}(\\Omega),\\bar{\\mathcal{M}}(\\Omega)$"
    "$\\mathcal{E}(G,s)_{\\mathcal{F}}$"
    "$E_{u,\\phi}$"
    "$\\Omega_u$"
    "$\\Irr_\\C(G)_g$"))
 :latex)

