(TeX-add-style-hook
 "DL"
 (lambda ()
   (LaTeX-add-labels
    "sec:deligne-luszt-theory"
    "sec:ell-adic-cohomology-1"
    "The:ell-adic-cohomology"
    "sec:deligne-luszt-var"
    "sec:deligne-luszt-induct"
    "Pro:HC-ind"
    "The:scalar-product-dl"
    "The:regular-dl"
    "sec:rational-series"
    "Pro:dual-char-ss"
    "The:del-char-conj"
    "Pro:quotient-unipotent"
    "sec:rational-series-1"
    "The:jord-decomp"
    "The:Res-Deligne-Lusztig"
    "The:jord-decomp-dis"
    "sec:blocks-luszt-seri"
    "The:Broue-Michel"
    "The:Bonnafe-Rouquier")
   (LaTeX-add-index-entries
    "$B_s(G)$"))
 :latex)

