(TeX-add-style-hook
 "lspe"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (LaTeX-add-labels
    "sec:count-unip-brau"
    "The:bs-verygood"
    "Ex:bs-bad"
    "Def:Lusztig-map"
    "Pro:class-rep-lspe"
    "Def:ell-special-quo"
    "Rk:MGammal"
    "Con:count-unip"
    "The:count-unip-good"
    "The:count-unip-bad")
   (LaTeX-add-index-entries
    "$\\Phi$"
    "$\\Omega_u^{\\ell}$"
    "$\\bar{\\mathcal{M}}_\\ell(\\Omega)$"
    "$\\alpha_{\\ell,u},\\alpha_\\ell$"))
 :latex)

