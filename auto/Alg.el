(TeX-add-style-hook
 "Alg"
 (lambda ()
   (LaTeX-add-labels
    "sec:finite-groups-lie"
    "sec:algebraic-groups"
    "sec:def-reductive-groups"
    "sec:root-system"
    "The:classification"
    "sec:parab-levi-subgr"
    "sec:steinberg-map"
    "The:lang-steinb"
    "Cor:lang-steinb-orbits"
    "Ex:app-lang-steinb"
    "The:class-finite-groups"
    "sec:isolated-classes"
    "The:isolated-classes-1"
    "Rk:isolated-classes-1"))
 :latex)

