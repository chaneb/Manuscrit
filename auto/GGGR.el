(TeX-add-style-hook
 "GGGR"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (LaTeX-add-labels
    "sec:gener-gelf-graev"
    "sec:weight-dynk-diagr"
    "sec:gener-gelf-graev-1"
    "Pro:gener-gelf-graev-2"
    "sec:unip-supp-wave"
    "Pro:supp-dual-gggr"
    "Pro:unip-supp-wave-1"
    "sec:gggrs-regul-embedd"
    "sec:gggrs-conj"
    "Pro:gggr-ind")
   (LaTeX-add-index-entries
    "$\\gamma_u,\\Gamma_u$"))
 :latex)

