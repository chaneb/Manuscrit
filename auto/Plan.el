(TeX-add-style-hook
 "Plan"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "twoside" "draft" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "a4paper") ("inputenc" "utf8") ("fontenc" "T1") ("xy" "all") ("hyperref" "colorlinks=true")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "geometry"
    "inputenc"
    "fontenc"
    "xy"
    "amssymb"
    "amsmath"
    "amsfonts"
    "mathrsfs"
    "varioref"
    "amsthm"
    "enumitem"
    "array"
    ""
    "stmaryrd"
    "xcolor"
    "tikz"
    "hyperref"
    "longtable"
    "tabularx")
   (TeX-add-symbols
    '("overbar" 1)
    "F"
    "C"
    "Z"
    "Q"
    "M"
    "G"
    "T"
    "U"
    "Irr")
   (LaTeX-add-labels
    "sec:verif-conj")
   (LaTeX-add-bibliographies
    "bib")
   (LaTeX-add-amsthm-newtheorems
    "The"
    "Con"
    "Pro"
    "Cor"
    "Lem"
    "Def"
    "Rk"
    "Ex"))
 :latex)

