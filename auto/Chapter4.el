(TeX-add-style-hook
 "Chapter4"
 (lambda ()
   (LaTeX-add-labels
    "cha:decomp-matr"
    "sec:cohomology-complexes"
    "The:ell-adic-proj"
    "Cor:ell-adic-proj"
    "sec:appl-comp-decomp"
    "Pro:appl-comp-decomp"
    "sec:combinatorics-"
    "eq:3"
    "sec:unitr-basic-set"
    "sec:unkn-decomp-numb"))
 :latex)

