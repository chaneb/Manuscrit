(TeX-add-style-hook
 "Rep"
 (lambda ()
   (LaTeX-add-labels
    "sec:repr-finite-groups"
    "sec:decomposition-map"
    "Ex:decomposition-map"
    "Ex:blocks"
    "sec:def-basicset"
    "sec:induct-restr-cliff"
    "Rk:projind-lprimegroups"
    "sec:induct-restr-cliff-1")
   (LaTeX-add-index-entries
    "$P_k(G)$"))
 :latex)

