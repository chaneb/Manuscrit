(TeX-add-style-hook
 "Sp4blocks"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("babel" "francais") ("xy" "all") ("geometry" "a4paper")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "inputenc"
    "fontenc"
    "babel"
    "xy"
    "amssymb"
    "amsmath"
    "amsfonts"
    "mathrsfs"
    "varioref"
    "amsthm"
    "enumitem"
    "hyperref"
    "xcolor"
    "tikz"
    "geometry")
   (LaTeX-add-amsthm-newtheorems
    "The"
    "Pro"
    "Cor"
    "Lem"
    "Def"
    "Rq"
    "Ex"))
 :latex)

