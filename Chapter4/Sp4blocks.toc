\select@language {french}
\contentsline {section}{\numberline {1}Preliminary results}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Algebraic groups with a BN-pair}{1}{subsection.1.1}
\contentsline {section}{\numberline {2}Decomposition of Deligne-Lusztig Characters for element of order 2}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Centralizers of elements of order 2}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Jordan decomposition of character}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}2-decomposition numbers of Sp4(q), q odd}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Preliminary results}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}2-decomposition numbers of the principal block}{4}{subsection.3.2}
