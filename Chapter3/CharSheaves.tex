\section{Character Sheaves and GGGRs}\label{sec:character-sheaves-3}

We introduce in this section results of Geck--Hézard and Taylor on generalised Gelfand--Graev representations which will be essential to prove that GGGRs satisfies the assumption of Proposition \ref{sec:existence-basic-set}. After introducing some generalities on character sheaves and local systems, we introduce a theorem of Geck--Hézard which was proved under the assumption that $\G$ has connected center. Finally, we briefly explain how Taylor generalised that result for groups with non-connected center.


\subsection{Character sheaves and local systems}


We assume that the center of $\G$ is connected. Let $\hat{\G}$ be the set of character sheaves on $\G$. Characters sheaves, introduced by Lusztig in \cite{LUSZTIG1985193}, are certain irreducible perverse sheaves which are equivariant for the action of $\G$ by conjugation.  The structure of $\hat{\G}$ is in a certain sense very similar to $\Irr_K(G)$: Lusztig gave in \cite{LUSZTIG1985226} a partition
$$\hat{\G}=\bigsqcup \hat{\G}_s$$
where $s$ runs over a complete set of representatives of $\G^*$-conjugacy class of semi-simple elements of $G^*$. Moreover, for for each class $(s)_{\G^*}$, we have a partition
$$\hat{\G}_s=\bigsqcup \hat{\G}_g$$
where $g$ runs over representatives of special classes of $\G^*$ whose semi-simple part is $s$.

We say that $A \in \hat{\G}$ is $F$-stable if it isomorphic to its inverse image $F^*A$ under the Frobenius map $F$. We denote by $\hat{G}$ the set of $F$-stable character sheaves. If $A \in \hat{G}$, any isomorphism $\phi:F^*A\to A$ induces a class function $\chi_A \in \mbox{CF}(G)$. By \cite{LUSZTIG1985226}, we can chose $\phi$ such that $\langle \chi_A, \chi_A \rangle_{KG}=1$. The set obtained $\{\chi_A \mid A \in \hat{G} \}$ is an orthonormal basis of $\mbox{CF}_K(G)$. In particular, there are as many $F$-stable character sheaves as irreducible characters of $G$ over $K$. The structure of $\hat{\G}$ can be transferred to $\hat{G}$, we have a partition:
$$\hat{G}=\bigsqcup \hat{G}_g$$ 
where $g$ runs over a set of representatives of $F$-stable special $\G^*$-conjugacy classes. Moreover, according to \cite{SHOJI1995244}, if $A \in \hat{G}_g$ then $\chi_A$ then is a linear combination of irreducible characters of $\Irr_K(G)_g$.

Let $\mathcal{C}$ be an $F$-stable unipotent class of $\G$, let $I_{\mathcal{C}}$ the set of $\G$-equivariant irreducible local systems. We denote by $I_{\mathcal{C}}^F$ the local systems $\mathcal{E} \in I_{\mathcal{C}}$ such that $\mathcal{E} \simeq F^*\mathcal{E}$. Let $\mathcal{E} \in I_{\mathcal{C}}^F$, and let us fix an isomorphism $\psi: F^*:\mathcal{E}\to\mathcal{E}$. Let us define the following function $Y_{\mathcal{E}}$ on $G$:

\begin{equation*}
    Y_{\mathcal{E}}(g) = \begin{cases}
               \mbox{Trace}(\psi,\mathcal{E}_g)        & \mbox{if } g \in \mathcal{C}^F,\\
               0   & \text{otherwise.}
           \end{cases}
\end{equation*}

In the proof of \cite[24.2.7]{LUSZTIG1986103}, Lusztig proved the following statement about the functions $Y_\mathcal{E}$.

\begin{Pro}\label{sec:character-sheaves-1}
  The functions $Y_\mathcal{E}$, $\mathcal{E} \in I_\mathcal{C}^F$ form a basis for the vector space of $G$-invariant functions on $\mathcal{C}^F$.
\end{Pro}

\subsection{The unitriangularity condition}

 Let $\mathcal{C}$ be an $F$-stable unipotent class of $\G$, and $g$ be a special element of $G^*$ with Jordan decomposition $g=sv$. We say that $g$ satisfies the property $(P)$ with respect to $\mathcal{C}$ if:

\begin{itemize}
\item $\Phi((g)_{\G^*})=\mathcal{C}$.
\item $|\Omega_g|=|\Omega_u|$. Recall that $\Omega_u$ is the canonical quotion of $A_\G(u)$ and $\Omega_u(g)$ is the canonical quotient of $A_{C_{\G^*}(s)}(v)$.
\item The image of $s$ in the adjoint quotient of $\G^*$ is quasi-isolated.
\end{itemize}

By a case-by-case analysis, the existence of such a $g$ for any $F$-stable unipotent class $\mathcal{C}$ has been proved by Geck--Hézard.

\begin{Pro}[\cite{geck_unipotent_2008}]

Let $\mathcal{C}$ be an $F$-stable unipotent class of $\G$. There exists $g \in G^*$ special satisfying $(P)$ with respect to $\mathcal{C}$.
\end{Pro}


\begin{Rk}
In \cite{geck_unipotent_2008}, the semi-simple part $s$ of $g$ was mentioned to be isolated. But in \cite[2.28]{taylor_unipotent_2012}, Taylor noted that for $\G$ of type $C_n$, Hézard used a special element $g \in G^*$ with semi-simple part $s$ such that $C_{\G^*}(s)$ is a Levi subgroup of type $B_{n-1}$ of $\G^*$. However, the image of $s$ in $\G^*_{ad}$ is isolated. 
\end{Rk}

This result has many consequences on the restriction of character sheaves to unipotent classes.

\begin{The}[\cite{geck_unipotent_2008}]
  \label{sec:character-sheaves}
  Let $\mathcal{C}$ be an $F$-stable unipotent class of $\G$. Assume that $g=sv \in \G^*$ satisfies the property $(P)$ with respect to $\mathcal{C}$. Then
  \begin{itemize}
  \item For any $A \in \hat{\G}_g$, $A_{|\mathcal{C}}$ is either
    zero or an irreducible $\G$-equivariant local system up to
    shift. 
  \item There is a bijection

    $$\begin{array}{ccc}
        \{A \in \hat{\G}_g| \: A_{| \mathcal{C}} \neq 0 \} & \to & I_\mathcal{C}, \\
        A& \mapsto & A_{|\mathcal{C}}.
      \end{array}$$
    \end{itemize}

\end{The}

\begin{Rk}\label{sec:character-sheaves-2}
  \leavevmode
  \begin{enumerate}
  \item For any $\mathcal{E} \in I_\mathcal{C}^F$, the unique character sheaf $A \in \hat{\G}_g$ such that $A|_\mathcal{C}=\mathcal{E}$ is $F$-stable. Indeed, since $(s)_{\G^*}$ and $(v)_{C_{\G^*}(s)}$ are $F$-stable, $F^*A \in \hat{\G}_g$ by \cite[5.5]{SHOJI1995244}. Moreover, $F^*A|_\mathcal{C}=F^*\mathcal{E} \simeq \mathcal{E} = A|_{\mathcal{C}}$. Hence $F^*A \simeq A$ according to the theorem.
  \item Let us keep the notation of the theorem. We can translate those results as follows: for any $A \in \hat{G}_g$ we have  either $\chi_{A_{|\mathcal{C}}}=0$ or we can choose the isomorphism    $\phi:F^*A \to A$ such that $\chi_A(g)=Y_{\mathcal{E}}(g)$ for all    $g \in \mathcal{C}^F$ where $\mathcal{E}=A_{|\mathcal{C}}$.
    \end{enumerate}

    \vspace{0.3cm}

   Although the following statement only involves irreducible characters of $G$, the proof relies on character sheaves and local systems.

   \begin{Pro}
     \label{Pro:unitr-cond}
      Let $\mathcal{C}$ be an $F$-stable unipotent class, $u_1, \dots ,u_d$ be representatives of the $G$-conjugacy classes of $\mathcal{C}^F$ and $g=sv \in \G^*$. Asssume that $g$ satisfies $(P)$ with respect to $\mathcal{C}$. Then there exists $\rho_1, \dots, \rho_d \in \Irr_K(G)_g$ such that the matrix $(\rho_i(u_j))_{1\leq i,j \leq d}$ is invertible.

    \end{Pro}

\end{Rk}

\begin{proof}
  By Proposition \ref{sec:character-sheaves-1}, $I_{\mathcal{C}}^F$ has $d$ elements $\mathcal{E}_1, \dots, \mathcal{E}_d$. By Theorem \ref{sec:character-sheaves} and Remark \ref{sec:character-sheaves-2} 1), there exist $A_1, \dots, A_d \in \hat{G}_{g}$ such that ${A_{i}}_{|\mathcal{C}}=\mathcal{E}_i$. According to Remark \ref{sec:character-sheaves-2} 2), we can assume that $\chi_{A_i}=Y_{\mathcal{E}_i}$ on $\mathcal{C}^F$ and Proposition \ref{sec:character-sheaves-1} tells us that $(\chi_{A_i}(u_j))_{1 \leq i,j \leq d}$ is invertible. Since each $\chi_{A_i}$ is a linear combination of characters in $\Irr_K(G)_g$, there exist $\rho_1, \dots, \rho_d \in \Irr_K(G)_g$ such that the matrix $(\rho_i(u_j))_{1 \leq i, j \leq d}$ is invertible.  
\end{proof}

    \vspace{0.3cm}

We can now state the result which will provide the unitriangularity condition for groups with connected center.

\begin{The}[Geck--Hézard]\label{sec:theorem-gggrs}
  Assume that the center of $\G$ is connected and $\G$ has classical type. Let $\mathcal{C}$ be an $F$-stable unipotent class of $\G$ and $u_1, \dots, u_d$ be representatives of $G$-conjugacy classes of $\mathcal{C}^F$. Let $g=sv \in \G^*$ satisfying $(P)$ with respect to $\mathcal{C}$. Then, there exist $\chi_1, \dots, \chi_d \in \Irr_K(G)_g$ such that $\langle \chi_i^*, \gamma_{u_j} \rangle_{KG}=\delta_{i,j}$ for $1 \leq i,j \leq d$. 
\end{The}

\begin{proof}
According to Remark 3.8 of \cite{MR1475683}, for any $\rho \in \Irr_K(G)_g$,

  $$\sum \limits_{i=1}^d [A_\G(u_i):A_\G(u_i)^F]\langle \rho^*,\gamma_{u_i} \rangle_{KG}=n_\rho^{-1}|A_\G(u_1)|$$
 where $n_\rho$ is the minimal integer such that $n_\rho\rho(1)$ is a polynomial in $q$ with integer coefficients. More precisely, following the notation of Theorem \ref{sec:lusztigs-theorem},  we denote by $x_\rho$ the element of $ \bar{\mathcal{M}}(\Omega_g)$ corresponding to $\rho$ and  by $x \in \bar{\mathcal{M}}(\Omega_g)$ the element corresponding to the special representation of $W_s$ in the family corresponding to $(v)_{C_{\G^*}(s)}$ (\emph{i.e.} $E_{v,1} \otimes \varepsilon$), then $n_\rho=\{x_\rho,x \}$. Since $g$ has property $(P)$, $|\Omega_g|=|A_\G(u_1)|$ and the fact that we are in classical type implies that $\Omega_g$ is abelian (see \cite[4.5,4.6]{lusztig_g._characters_1984}). So $n_\rho=|A_\G(u_1)|$ according to the definition of $\{.,.\}$ (see Definition \ref{Def:MGamma}). Hence,  
  $$\sum \limits_{i=1}^d [A_\G(u_i):A_\G(u_i)^F]\langle \rho^*,\gamma_{u_i}\rangle_{KG}=1.$$
Since each term of the sum is non-negative, there is a unique $i \in \{1, \dots, d \}$ such that $\langle\rho^*, \gamma_{u_i}\rangle_{KG}=1$ and $\langle \rho^*, \gamma_{u_j} \rangle_{KG}=0$ for $j \neq i$. For $i \in \{1, \dots ,d \}$, let $I_i$ the subset of $\Irr_K(G)_g$ consisting of characters $\rho$ such that $\langle \rho, \gamma_{u_i} \rangle_{KG}=1$ and $\langle \rho, \gamma_{u_j} \rangle_{KG}=0$ for $j \neq i$. Assume that $I_r= \emptyset$ for some $r \in \{1, \dots, d \}$, \emph{i.e.} assume that for all $\rho \in \Irr_K(G)_g$, $\langle \rho^*,\gamma_{u_r}\rangle_{KG}=0$ or, equivalently,  $\langle \rho, D_G(\gamma_{u_r}) \rangle_{KG}=0$. Writing the definition of the scalar product for the last equality we have

  $$|G|^{-1}\sum \limits_{g \in G} \overbar{\rho(g)}D_G(\gamma_{u_r})(g)=0.$$
  Moreover, $D_G(\gamma_{u_r})$ is $0$ on non-unipotent elements and by Proposition \ref{Pro:supp-dual-gggr}, $D_G(\gamma_{u_r})$ is non-zero only on unipotent classes $\mathcal{C}$ such that $\mathcal{C} \subseteq \overbar{\mathcal{C}'}$.  On the other hand, by Proposition \ref{Pro:unip-supp-wave-1}, $\mathcal{C}$ is the unipotent support of $\rho$. Then, according to \cite[11.2]{lusztig_unipotent_1992}, $\rho$ is non-zero only on classes $\mathcal{C}'$ such that $\mathrm{dim \,} \mathcal{C'} < \mathrm{dim \,} \mathcal{C}$ or $\mathcal{C'}=\mathcal{C}$. Hence, we only need to sum over elements of $\mathcal{C}^F$ and we get

  $$ \sum \limits_{j=1}^d|C_G(u_j)|^{-1} \overbar{\rho(u_j)}D_G(\gamma_{u_r})(u_j)=0.$$
Applying this sum to the characters $\rho_1, \ldots, \rho_d$ of Proposition \ref{Pro:unitr-cond}, we get a linear relation on columns of the invertible matrix $(\rho_i(u_j))_{1\leq i,j \leq d}$ so $D_G(\gamma_{u_r})(u_j)=0$ for $j \in \{ 1, \dots, d \}$. According to \cite[2.4]{Geck1999Jan}, for any $\mathcal{E} \in I_\mathcal{C}^F,$ $\langle D_G(\gamma_{u_r}),Y_{\mathcal{E}}) \rangle_{KG}$ equals $ \zeta q^{b} \overbar{Y_{\mathcal{E}}(u_r)}$ where $\zeta$ is a 4-th root of unity and $b$ is an integer depending on $\mathcal{E}$. Hence, $Y_{\mathcal{E}}(u_r)=0$ for any $\mathcal{E} \in I_\mathcal{C}^F$ but this is in contradiction with Proposition \ref{sec:character-sheaves-1}. That contradiction implies that $I_i \neq \emptyset$ for $i \in \{1, \dots, d \}$ and for each $i \in \{1, \dots, d \}$ we choose $\chi_i \in I_i$ such that $\langle \chi_i^*,\gamma_{u_j} \rangle_{KG}=\delta_{i,j}$ for $j \in \{1, \dots, d \}$.
\end{proof}

\subsection{Unitriangularity for groups with disconnected center}

In \cite{taylor_unipotent_2013} Taylor generalised Theorem \ref{sec:theorem-gggrs} for groups with disconnected center,  we will briefly discuss this generalisation. Suppose that $\G$ is a simple classical group with disconnected center, let $\tilde{\G}$ be a regular embedding of $\G$ and $\tilde{G}=\tilde{\G}^F$ the associated finite reductive group. This property on the regular embedding and the Alvis-Curtis duality will be needed in the proof of the next theorem.

\begin{Lem}[{\cite[5.3]{taylor_unipotent_2013}}]
  \label{Lem:Duality-embedding}
  Let $\tilde{\chi} \in \Irr_K(\tilde{G})$ and let us write
  $$\emph{Res}^{\tilde{G}}_G(\tilde{\chi})=\chi_1+\cdots+\chi_r$$
 where $\chi_i \in \Irr_K(G)$. Then $\emph{Res}^{\tilde{G}}_G(\tilde{\chi}^*)=\chi_1^*+\cdots+\chi_r^*$.
\end{Lem}

Recall that for a unipotent element $u \in \tilde{G}$, we denote by $\gamma_{u}$ the character of the associated GGGR of $G$ and by $\tilde{\gamma}_{u}$ the character of the associated GGGR of $\tilde{G}$. We have  $\tilde{\gamma}_u=\mbox{Ind}_G^{\tilde{G}}(\gamma_u)$ by Proposition \ref{Pro:gggr-ind}. Let \index{$Z_\G(u)$} $Z_\G(u)$ be the image of $Z(\G)$ in $A_\G(u)$.  Let $\mathcal{C}=(u)_\G$ be an $F$-stable conjugacy class of $\G$ such that $A_\G(u)$ is abelian. Let us write the partition of $\mathcal{C}^F$ into $\tilde{G}$-conjugacy classes: $\mathcal{C}=\mathcal{\tilde{O}}_1 \sqcup \dots \sqcup \mathcal{\tilde{O}}_d$. Then:

\begin{The}[\cite{taylor_unipotent_2013}]
\label{sec:non-connected-center}

  There exists a semi-simple element $s \in G^*$ and $\chi_1, \dots, \chi_d \in \mathcal{E}(G,s)$ such that $\langle \chi_i^*, \gamma_{u_j} \rangle_{KG}=\delta_{i,j}$ for $1 \leq i,j \leq d$.  
\end{The}

\begin{proof}
For $i \in \{1, \dots, d \}$, let us decompose $\mathcal{\tilde{O}}_i \cap G = \mathcal{C}_{i,1} \cup \dots \cup \mathcal{C}_{i,k_i}$ into $G$-classes, where $k_i$ depends on $i$. For each $i \in \{1, \dots, d\}$ and $j \in \{1, \dots, k_i \}$, let $u_{i,j}$ be a representative of $\mathcal{C}_{i,j}$. According to \cite[2.8]{taylor_unipotent_2013}, we can chose a representative $u \in \mathcal{C}^F$ such that  $F$ acts trivially on $A_{\tilde{\G}}(u)$ and

\begin{equation}
|A_\G(u)^F|=|Z_\G(u)^F||A_{\tilde{\G}}(u)|.\label{eq:1}
\end{equation}


Because $\tilde{G}$-classes of $\mathcal{C}^F$ are in bijection with $F$-classes of $A_{\tilde{\G}}(u)$ and, $A_{\tilde{\G}}(u)$ is an abelian group on which $F$ acts trivially, we have that $d=|A_{\tilde{\G}}(u)|$. According to \cite[3.2]{taylor_unipotent_2013}, we can chose $\tilde{g}=\tilde{s}\tilde{v} \in \tilde{G}^*$ such that
\begin{itemize}
\item $\tilde{g}$ satisfies $(P)$ with respect to $\mathcal{C}$.
\item The restriction of any character of $\Irr_K(\tilde{G})_{\tilde{g}}$ to $G$ has $|Z_\G(u)^F|$ irreducible constituents, without multiplicities. 
\end{itemize}

Hence, we can use \ref{sec:theorem-gggrs}  to exhibit irreducible characters $\tilde{\chi}_{1}, \dots , \tilde{\chi}_{d} \in \Irr_K(\tilde{G})_{\tilde{g}}$ such that $\langle\tilde{\chi}^*_{i}, \tilde{\gamma}_{u_{x,1}}\rangle_{K\tilde{G}}=\delta_{i,x}$ for $i,x \in \{1, \dots, d \}$. Using Frobenius reciprocity and the fact that $\tilde{\gamma}_{u_{x,1}}=\mbox{Ind}_G^{\tilde{G}}(\gamma_{u_{x,1}})$ we deduce that $\langle\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i}),\gamma_{u_{x,1}}\rangle_{KG}=\delta_{i,x}$. Since the number of irreducible constituents of $\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i,1}) $ is $|Z_\G(u)^F|$, we can write by Lemma \ref{Lem:Duality-embedding}

$$\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i})=\chi^*_{i,1}+ \dots+ \chi^*_{i,|Z_\G(u)^F|}$$ 
where the right part is a sum of distinct irreducible characters of $G$. Hence, 
$$\delta_{i,x}=\sum \limits_{j=1}^{|Z_\G(u)^F|}\langle \chi^*_{i,j},\gamma_{u_{x,1}}\rangle_{KG}.$$
We can assume for more convenience that $\langle \chi_{i,j}^*,\gamma_{u_{i,1}}\rangle_{KG}=\delta_{j,1}$. Since the characters $\chi_{i,j}$ are irreducible constituents of $\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i,1})$, we  can choose $t_{i,1}=1, \dots, t_{i,|Z_\G(u)^F|} \in \tilde{T}$ such that $\chi_{i,j}^*=(\chi_{i,1})^{* t_{i,j}}$. Using \ref{sec:gggrs-conj}, we have
$$1=\langle \chi^*_{i,1},\gamma_{u_{i,1}} \rangle_{KG}=\langle (\chi_{i,1})^{*t_{i,j}},(\gamma_{u_{i,1}})^{t_{i,j}}\rangle_{KG}=\langle \chi^*_{i,j},\gamma_{t_{i,j}u_{i,1}t_{i,j}^{-1}} \rangle_{KG}.$$
Moreover, if $j \neq k$, $t_{i,j}u_{i,1}t_{i,j}^{-1}$ and $t_{i,k}u_{i,1}t_{i,k}^{-1}$ lie in distinct $G$-conjugacy classes. Indeed, if $t_{i,j}u_{i,1}t_{i,j}^{-1}$ and $t_{i,k}u_{i,1}t_{i,k}^{-1}$ are in the same $G$-conjugacy class then $ \gamma_{u_{i,1}} = \gamma_{t_{i,j}^{-1}t_{i,k}u_{i,1}t_{i,k}^{-1}t_{i,j}}$ and
$$  1=\langle \chi_{i,1}^*, \gamma_{t_{i,j}^{-1}t_{i,k}u_{i,1}t_{i,k}^{-1}t_{i,j}} \rangle_{KG}=\langle(\chi_{i,1}^*)^{t_{i,j}t_{i,k}^{-1}},\gamma_{u_{i,1}}\rangle_{KG}.$$
Hence, $(\chi_{i,1})^{*t_{i,j}t_{i,k}^{-1}}$ is one of the characters $\chi_{i,1}, \dots, \chi_{i,|Z_\G(u)^F|}$ but amongst those characters $\chi_{i,1}$ is the only one satisfying the above equality. Then $\chi_{i,1}=(\chi_{i,1}^*)^{t_{i,j}t_{i,k}^{-1}}$ and $t_{i,j}=t_{i,k}$. Therefore, $k_i \leq |Z_\G(u)^F|$ and we can use the inequality below to deduce that $k_i=|Z_\G(u)^F|$.
$$|A_\G(u)^F|=|Z_\G(u)^F||A_{\tilde{\G}}(u)|=d|Z_\G(u)^F|\leq \sum \limits_{i=1}^d k_i=|A_\G(u)^F|$$
where the first equality comes from equality \eqref{eq:1}. Now, by changing the numbering if necessary, we can assume that $u_{x,y}=t_{x,y}u_{x,1}t_{x,y}^{-1}$ for $x \in \{1, \dots, d \}$ and $y \in \{1, \dots, |Z_\G(u)^F| \}$. We have $\langle \chi_{i,j}^*,\gamma_{u_{x,y}}\rangle_{KG}=\langle(\chi_{i,j}^*)^{t_{x,y}^{-1}},\gamma_{u_{x,1}}\rangle_{KG}$. Since $(\chi_{i,j}^*)^{t_{x,y}^{-1}}$ is an irreducible constituent of $\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i,1})$, we have that $\langle\chi_{i,j}^*,\gamma_{u_{x,y}}\rangle_{KG}=0$ if $i\neq x$. We can prove similarly that $\langle\chi_{i,j}^*,\gamma_{u_{i,y}}\rangle_{KG}=0$ if $j \neq y$. Hence, the characters $\chi^*_{i,j}, \quad 0 \leq i \leq d, 0 \leq j \leq |Z_\G(u)^F|$ respect the first assumption of the theorem. Let $s$ be the image of $\tilde{s}$ in $G^*$, then the characters $\chi^*_{i,j}$ belong to the rational series $\mathcal{E}(G,s)$. Moreover, since $s$ and $\tilde{s}$ have the same image under an adjoint quotient and $\tilde{g}$ satisfies the property $(P)$, the image of $s$ under an adjoint quotient is isolated.
\end{proof}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End: 