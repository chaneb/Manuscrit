\contentsline {section}{\numberline {0.1}Introduction to the main theorem}{3}{section.0.1}
\contentsline {subsection}{\numberline {0.1.1}Review of results}{3}{subsection.0.1.1}
\contentsline {subsection}{\numberline {0.1.2}Statement of the theorem and strategy of proof}{4}{subsection.0.1.2}
\contentsline {section}{\numberline {0.2}Generalized Gelfand Graev Representations}{5}{section.0.2}
\contentsline {subsection}{\numberline {0.2.1}Weighted Dynkin diagrams}{5}{subsection.0.2.1}
\contentsline {subsection}{\numberline {0.2.2}Generalized Gelfand Graev representations}{6}{subsection.0.2.2}
\contentsline {subsection}{\numberline {0.2.3}Unipotent support and wave front set}{8}{subsection.0.2.3}
\contentsline {subsection}{\numberline {0.2.4}GGGRs and regular embedding}{8}{subsection.0.2.4}
\contentsline {section}{\numberline {0.3}Character Sheaves and GGGRs}{9}{section.0.3}
\contentsline {subsection}{\numberline {0.3.1}Character sheaves and Local systems}{9}{subsection.0.3.1}
\contentsline {subsection}{\numberline {0.3.2}The unitriangularity condition}{10}{subsection.0.3.2}
\contentsline {subsection}{\numberline {0.3.3}Unitriangularity for groups with disconnected center}{12}{subsection.0.3.3}
\contentsline {section}{\numberline {0.4}Main result}{14}{section.0.4}
