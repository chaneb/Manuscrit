\section{Introduction to the main theorem}\label{sec:intr-main-theor}


 \subsection{Statement of the theorem }
Let $\G$ be a connected reductive group defined over $\F_q$ where $q$ is the power of a good prime number $p$. As in $\S$ \ref{sec:decomposition-map}, we fix a prime number $\ell \neq p$ and an $\ell$-modular system $(K, \mathcal{O}, k)$ for $G$. If $B$ is a union of $\ell$-blocks, we recall that a basic set for $B$ is a subset $\mathcal{B} \subset \Irr_K(B)$ such that $d(\mathcal{B})$ is a $\Z$-basis of $R_k(B)$. We recall that according to Theorem \ref{The:Broue-Michel}

$$B_s=B_s(G)=\bigcup \limits_t \mathcal{E}(G^F, t)$$
where $t$ runs over semi-simple $\ell$-elements of $C_{G^*}(s)$ is a union of blocks. Under some restrictions on $\ell$, Geck and Hiss proved that there is a natural basic set for $B_s$. By Theorem \ref{The:bs-verygood}, $\mathcal{E}(G,s)$ is a basic set for $B_s$ when $\ell$ is very good for $G$ but the theorem does not indicate whether the basic set is unitriangular or not. However, it was conjectured by Geck to be the case, at least for unipotent blocks.

\begin{Con}[Geck  {\cite[3.4]{geck_modular_1997}}]
 Assume that $\ell$ is good for $G$ and does not divide the order of $(Z(\G)/Z(\G)^{\circ})^F$. Then unipotent characters form a unitriangular basic set of $B_1$.
\end{Con}

Our strategy for constructing a unitriangular basic set consists in counting the number of irreducible unipotent modular representations and then finding enough projective modules which will satisfy some unitriangularity condition. Under the condition that the center of $\G$ is connected, Geck achieved this for classical groups in \cite{geck_basic_1994}. Following the same strategy, we generalise those results to cases where the center is disconnected:

\begin{The*}\label{sec:main-the}
  Assume that $p$ is an odd prime number, $\ell=2$ and let $\G$ be a simple group of type $B$, $C$ or $D$ which is neither spin or half-spin. Then there exists a unitriangular basic set for the unipotent blocks of $G$.
\end{The*}

\subsection{Strategy of proof}
\label{sec:strategy-proof}
 
We start off with some results which are valid for arbitrary finite groups.

\begin{Pro}\label{sec:existence-basic-set}
Let $B$ be a union of blocks and $n:=|\Irr_k(B)|$. Assume that there exist ordinary characters $\chi_1, \ldots, \chi_n$ and projective $kG$-modules  $P_1, \ldots, P_n$ in $B$ such that the matrix of scalar products $\big(\langle\chi_i, e(P_j) \rangle_{KG}\big)_{1 \leq i,j \leq n}$ has lower unitriangular shape. Then the characters $\chi_1, \ldots, \chi_n$ form a unitriangular basic set for $B$.
\end{Pro}

%\vspace*{0.5cm}

\begin{proof}
Let $d: R_K(G) \longrightarrow R_k(G)$ be the decomposition map. Since $e$ and $d$ are in duality, we have the relation $\langle \chi_i, e(P_j)\rangle_{KG} = \langle d(\chi_i), P_j\rangle_{kG}$. 
Let us denote by $\varphi_1, \ldots, \varphi_n$ the irreducible modular representations lying in $B$. For $ 1 \leq i \leq n$, we can write  $d(\chi_i)=\sum_{l=1}^n d_{\chi_i, \varphi_l} \varphi_l$. So we have
$$\langle\chi_i, e(P_j)\rangle_{KG} =  \sum_{l=1}^n d_{\chi_i, \varphi_l}\langle\varphi_l, P_j\rangle_{kG}.$$ 
In terms of matrices, this equality is:
$$(d_{\chi_i, \varphi_j})_{1 \leq i,j \leq n} \times \big(\langle\varphi_i, P_j\rangle_{kG}\big)_{1 \leq i, j \leq n} = \big(\langle \chi_i, e(P_j)\rangle_{KG} \big)_{1 \leq i,j \leq n}.$$
We want to show that $(d_{\chi_i, \varphi_j})$ is lower unitriangular, using the fact that the matrix on the right side of the equality is lower unitriangular. The result follows from the lemma below.
  \end{proof}

  \vspace*{0.5cm}

  \begin{Lem}
Let $A$, $B$ and $C$ be three square matrices of size $n \times n$ with non-negative integer coefficients such that $AB=C$ with $C$ lower unitriangular. Then, up to permutations of columns, $A$ is unitriangular as well.
\end{Lem}

\begin{proof}
  Let us write $A=(a_{i,j})_{1 \leq i,j \leq n}$, $B=(b_{i,j})_{1 \leq i,j \leq n}$, $C=(c_{i,j})_{1 \leq i,j \leq n}$. The matrix $C$ is lower unitriangular so 
  \begin{align*}
    0&=c_{i,j}= \sum \limits_{k=1}^n a_{i,k}b_{k,j} \quad \forall 1 \leq i<j \leq n.\\
    1&=c_{i,i}= \sum \limits_{k=1}^na_{i,k}b_{k,i}  \quad \forall 1 \leq i \leq n.
    \end{align*}
    All the coefficients are non-negative so we have
    \begin{enumerate}
    \item $a_{i,k}b_{k,j}=0$ for all $1 \leq k \leq n$ and $1 \leq i<j \leq n$,\label{item:1}
     \item Given $1 \leq i \leq n$, there is a unique integer $k_i$ with $1\leq k_i \leq n$ such that $a_{i,k_i}=b_{k_i,i}=1$. If $k \neq k_i$, then $a_{i,k}b_{k,i}=0$.\label{item:2}
     \end{enumerate}
     
     Note that if $i < j$, then $k_i \neq k_j$. Indeed if $k_i=k_j$, then $a_{i,k_i}b_{k_j,j}=0$ by \ref{item:1} which is impossible. Let us fix $i \in \{1, \ldots, n \}$. We have $a_{i,k_i}=1$. Let $l \in \{1, \ldots,n\}$. There is a unique $j \in \{1, \ldots, n \}$ such that $l=k_j$ so $b_{l,j}=1$ by \ref{item:2}. By \ref{item:1}, whenever $j >i$, $a_{i,l}b_{l,j}=0$ so $a_{i,l}=0$. Therefore, in the $i$-th row of $A$, there is at least $j-i$ zero coefficients. In particular, the first row of $A$ has a unique non-zero coefficient whose value is 1. If $\sigma \in \mathfrak{S}_n$, we have
     $$\sum \limits_{k=1}^n a_{i,k}b_{k,j}=\sum \limits_{k=1}^n a_{i,\sigma(k)}b_{\sigma(k),j}.$$
     Then, we can apply the same permutation to the columns of $A$ and to the rows of $B$ without changing the equality $AB=C$. In particular, permuting the columns of $A$ and the rows of $B$ if necessary, we can assume that $a_{1,j}=\delta_{1,j}$. Then, by \ref{item:1} and \ref{item:2} we also  have $b_{1,j}=\delta_{i,j}$. Now, we just have to show that the submatrix $(a_{i,j})_{2 \leq i,j \leq n}$ is lower unitriangular up to permutations of columns. We can conclude by an inductive argument since we have
      $$(a_{i,j})_{2 \leq i,j \leq n} \times (b_{i,j})_{2 \leq i,j \leq n}=(c_{i,j})_{2 \leq i,j \leq n}.$$
     % Similarly, in each column of $B$ there is a coefficient whose value is $1$ and such that every coefficient above is zero. Every coefficient of the first row of $A$ (resp. the first column of $B$) should be either $0$ or $1$. Since $\det(A)$ (resp. $\det(B)$) is non-zero, there is at least one coefficient in the forst row of $A$ (resp. first row of $B$) whose value is $1$. Let us show that the first row of $A$ has a unique coefficient whose value is $1$: let $k_1,\ldots,k_s$ be the indices such that $a_{1,k_i}=1$ and assume $s\geq2$. By 1., when $1 \leq i \leq s$, and $2 \leq j \leq n$, $0=a_{1,k_i}b_{k_i,j}=b_{k_i,j}$ so every coefficient the $k_i$th line of $B$ is $0$ except the first one, whose value is $1$. So $c_{1,1}= \sum_{i=1}^s a_{1,k_i}b_{k_i,1}=s$, which gives a contradiction. The same argument applied to every row of $A$ shows that, up to permutation of columns, $A$ is lower unitriangular.
   \end{proof}

   \vspace*{0.5cm}

Consequently, in order to find a unitriangular basic set of characters for unipotent blocks it is enough to:
   \begin{itemize}
   \item Compute the cardinal of $\Irr_k(B_1)$. We have seen in Theorem \ref{The:Countclasses} that the cardinal of $\Irr_k(B_1)$ is the number of unipotent classes of $G$ when $G$ is of type $B,C,D$.
   \item Find projective and ordinary representations satisfying the assumptions of Proposition \ref{sec:existence-basic-set}. This can be done using theory of generalised Gelfand--Graev representations and results of Geck, H\'ezard and Taylor that we will introduce in Section \ref{sec:character-sheaves-3}.
   \end{itemize}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
