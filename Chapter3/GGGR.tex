
\section{Generalised Gelfand--Graev representations}\label{sec:gener-gelf-graev}

 We fix an $F$-stable maximal torus $\T$ of $\G$ contained in a $F$-stable Borel subgroup $\B$ of $\G$. We denote by $\Phi$ a root system associated to $(\G,\T)$. The Borel subgroup $\B$ provides a set of simple roots $\Delta \subset \Phi$. To any unipotent element $u$ of $G$, Kawanaka associated in \cite{kawanaka_generalized_1985} a projective representation $\Gamma_{u}$ which depends only on the $G$-conjugacy class of $u$, called the generalised Gelfand--Graev representation of $G$ associated to $u$ (GGGR). Those representations will allows us to show the existence of a basic set for unipotent blocks of classical groups.

\subsection{Weighted Dynkin diagrams}\label{sec:weight-dynk-diagr}



We will introduce a description of unipotent classes of $\G$: to a unipotent class one can attach a combinatorial object called the \emph{weighted Dynkin diagram}.

\begin{Def}
  A weighted Dynkin diagram is a map $d: \Phi \to \Z$ such that
  \begin{itemize}
  \item For all $\alpha \in \Delta$, $d(\alpha) \in \{0,1,2 \}$,
  \item For all $\beta=\sum \limits_{\alpha \in \Delta} n_{\alpha} \alpha \in \Phi$, $d(\beta)=\sum \limits_{\alpha \in \Delta} n_{\alpha} d(\alpha)$.
  \end{itemize}
\end{Def}
We review the classification of unipotent classes by weighted Dynkin diagram below:

\begin{The}[{\cite[5.6]{carter_finite_1985}}]
  To a unipotent class $\mathcal{C}$ of $\G$, we can attach a weighted Dynkin diagram $d_{\mathcal{C}}$. Moreover, if $\mathcal{C'}$ is another unipotent class, we have
  $$d_{\mathcal{C}}=d_{\mathcal{C'}} \Longleftrightarrow \mathcal{C}=\mathcal{C'}. $$  
\end{The}

\begin{Rk}
  
\leavevmode
  \begin{itemize}
  \item Even though the proof of \cite{carter_finite_1985} relies on the fact that $p$ is large enough, the result still holds whenever $p$ is good \cite{kawanaka1986generalized}.
  \item The map  $\mathcal{C} \mapsto d_{\mathcal{C}}$ is not surjective.
  \item The weighted Dynkin diagrams for simple algebraic groups are listed in \cite[\S13.1]{carter_finite_1985}.
  \item    The  unipotent class $\mathcal{C}$  if $F$-stable if and only if $d_{\mathcal{C}}$ is stable under the action of $F$ on $\Phi$, see for example \cite[Lem 2.40]{taylor_unipotent_2012}.
\end{itemize}

\end{Rk}

Let $\mathcal{C}$ be a unipotent class of $\G$. Without giving a proof of the above result,  we will explain how we can describe $\mathcal{C}$ from $d_{\mathcal{C}}$. Let $\U$ be the unipotent radical of $\B$,  $\alpha \in \Phi$  and $\U_\alpha$ be the root subgroup of $\U$ attached to $\alpha$. Recall that we have an isomorphism $$ \G_a \xrightarrow \sim \U_\alpha,\quad x \mapsto u_\alpha(x)$$ and that $\U_\alpha$ is the unique minimal closed, connected unipotent subgroup of $\G$ normalised by $\T$ such that that $tu_\alpha(x)t^{-1}= u_\alpha(\alpha(t)x)$ for $x \in \G_a, t \in \T$.  We define the following groups:


\begin{align*}
  & \M_{\mathcal{C}}  :=\langle \T, \U_\alpha \mid \alpha \in \Phi, d_{\mathcal{C}}(\alpha) =0 \rangle, \\
& \U_{\mathcal{C},i}  := \prod\limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} \geq i}} \U_\alpha, \quad i \in \mathbb{N}.  
\end{align*}
By \cite{kawanaka1986generalized}, $\mathcal{C}$ is the unique unipotent class in $\G$ such that $\mathcal{C} \cap \U_{\mathcal{C},2}$ is dense in $\U_{\mathcal{C},2}$. Moreover, $\mathcal{C} \cap \U_{\mathcal{C},2}$ is a single conjugacy class of the parabolic group $\U_{\mathcal{C},1}\M_{\mathcal{C}}$.

\begin{Ex}\item
  \begin{enumerate}
  \item Assume that $\mathcal{C}$ is the trivial class. Then $d_{\mathcal{C}}(\alpha)=0$ for $\alpha \in \Phi$. Indeed, in that case $\U_{\mathcal{C},2}=\prod\limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} \geq 2}} \U_\alpha=\{1\}=\mathcal{C}$.
  \item Assume that $\mathcal{C}$ is the regular class, \emph{i.e.} the unique unipotent class of $\G$ of maximal dimension. Then $d_{\mathcal{C}}(\alpha)=2$ for $\alpha \in \Delta$, so that $\U_{\mathcal{C},2}=\U$.
  \end{enumerate}  
\end{Ex}

\subsection{Generalised Gelfand--Graev representations}\label{sec:gener-gelf-graev-1}

Let us introduce some further notation. Let $\g$ be the Lie algebra of $\G$ over $\overbar{\F}_q$, $\g$ also has a $\F_q$-rational structure and is equipped with a Frobenius map $F: \g \to \g$. Let $\t$ be the Lie algebra of $\T$, $\g$ has a Cartan decomposition

$$\g:=\t \oplus \bigoplus\limits_{\alpha \in \Phi} \g_{\alpha}$$
where  $\t$ is stable under $F$. The spaces $\g_{\alpha}$ are called the \emph{root subspaces} of $\g$ and have dimension 1 so for each $\alpha$ we can chose $e_{\alpha} \in \g_\alpha$ such that $\g_\alpha=\overbar{\F}_q e_\alpha$. Let $\rho$ be the permutation of $\Phi$ induced by $F$ (see  \ref{The:class-finite-groups}). Then $F(\g_\alpha)=\g_{\rho(\alpha)}$. According to \cite[5.6]{taylor_generalized_2016}, there exists a $\G$-invariant symmetric bilinear form defined over $\F_q$ denoted by $\kappa: \g \times  \g \to \overbar{\F}_q$ such that for $\alpha \in \Phi$ we have
$$\g_\alpha^\perp=\t \oplus \bigoplus\limits_{\alpha \in \Phi \setminus \{-\alpha\}} \g_{\alpha}$$
where $\g_\alpha^\perp:=\{x \in \g \mid \kappa(x,y)=0 \; \forall y \in \g_\alpha\}$. Moreover, according to \cite[5.2]{taylor_generalized_2016}, the map $\g \to \g, \, x \mapsto x^*$ defined by 
\begin{itemize}
\item $t^*=-t$ if $t \in \t$,
\item $e_\alpha^* =- e_{-\alpha}$ for every root $\alpha$,
\end{itemize}
 is an $\F_q$-opposition automorphism of $\g$. The reductive group $\G$ acts on $\g$ as follows. For $g \in \G$, let $\mathrm{Int}_g$ be defined by $\mathrm{Int}_g(x):=gxg^{-1}$. Let $\mathrm{Ad}_g: \g \to \g$ be the differential of $\mathrm{Int}_g$. Then $g \cdot x:=\mathrm{Ad}_g(x)$ for $g \in \G$ and $x \in \g$. We need the following definition.% Note that $\kappa(e_\alpha^*,e_\alpha) \in \F_{q^r}$ for all $\alpha \i n \Phi$. Let $\chi: (\F_q, +) \to (\C^*,.)$ be a non-trivial character.

\begin{Def}[{\cite[2.10, 2.15]{taylor_generalized_2016}}]
 Let $\G$ be a connected reductive group with root datum $(X, \Phi, Y, \Phi^\vee)$. We say that $\G$ is \emph{proximate} if $Y/ \Z \Phi^\vee$ has no $p$-torsion.
\end{Def}

Let $\mathcal{U}_{ni} \subseteq \G$ be the variety of unipotent elements of $\G$ and let $\mathcal{N}_{il}:=\mathcal{N}_{il} \subset \g$ be the nilpotent cone of $\g$. According to \cite[3.4]{taylor_generalized_2016}, if $\G$ is proximate there exists a $\G$-equivariant isomorphism of varieties $\phi: \mathcal{U}_{ni} \to \mathcal{N}_{il}$ which is compatible with the Frobenius endomorphisms on $\G$ and $\g$. Moreover, according to \cite[2.16]{taylor_generalized_2016}, for any connected reductive group $\G$, there exists a proximate algebraic group $\G'$ defined over $\F_q$ such that $G' \simeq G$. Hence, we can assume that $\G$ is proximate and that $\phi$ exists.

Let us fix a linear character $\chi_p: (\F_p, +) \to K^*$. Let $\mathrm{Tr}_{\F_q/ \F_p}: \F_q \to \F_p$ be the field trace. Let $\chi_q:= \chi_p \circ \mathrm{Tr}_{\F_q / \F_p}$. Let $u$ be an element of $\mathcal{C} \cap U_{\mathcal{C}, 2}$. Note that $u$ exists since $\mathcal{C} \cap \U_{\mathcal{C}, 2}$ is the orbit of some parabolic subgroup of $\G$. Let $\varphi_u: U_{\mathcal{C},2} \to K^*$ be the map defined by
$$\varphi_u(x)=\chi_q(\kappa(\phi(u)^*,\phi(x))).$$
It is a linear character of $U_{\mathcal{C},2}$ by \cite[5.10]{taylor_generalized_2016}. Moreover, according to \cite[5.14, 5.15]{taylor_generalized_2016}, \index{$\gamma_u,\Gamma_u$} $$\gamma_u:=[U_{\mathcal{C}, 1}, U_{\mathcal{C}, 2}]^{-1/2}\mbox{Ind}_{ U_{\mathcal{C}, 2}}^G(\varphi_u)$$
is a character of $G$ and $\Gamma_u$, the \emph{generalised Gelfand--Graev representation} associated to $u$ is the representation whose character is $\gamma_u$. When $F$ is split, \emph{i.e.} if $\rho$ acts trivially on $\Phi$, Geck gave in \cite{ho2004finite} an alternate definition for $\varphi_u$. In that case:

\begin{itemize}
\item $\kappa(e_\alpha, e_\beta) \in \F_q$ for any $\alpha, \beta \in \Phi$.
\item If $x \in U$ and if we write $x=\prod \limits_{\alpha \in \Phi^+}u_\alpha(\mu_\alpha)$, then $\mu_\alpha \in \F_q$.
\end{itemize}
Let us write 
  $u =\prod \limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}}(\alpha) \geq 2}} u_\alpha(\lambda_\alpha)$ and let $x=\prod \limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} \geq 2}} u_\alpha(\mu_\alpha) \in U_{C,2}$ where $\lambda_\alpha, \mu_\alpha \in \F_q$. Then
  $$\varphi_u(x)=\sum \limits_{\substack{\alpha \in \Phi^+ \\ d_{\mathcal{C}}(\alpha)=2}} \chi_q(\lambda_\alpha \mu_\alpha \kappa(e_\alpha^*,e_\alpha)).$$


\begin{Rk}
\leavevmode
  \begin{itemize}
  \item By \cite[3.1.12]{kawanaka1986generalized}, $[U_{\mathcal{C}, 1}, U_{\mathcal{C}, 2}]$ is an even power of $q$ so it has a square root in $\Z_{\geq 0}$.  
  \item In the original definition \cite[3.1.11]{kawanaka1986generalized}, GGGRs are constructed by induction of a linear character of a $p$-group so GGGRs are projective $kG$-modules whenever $p \neq \ell$.
    
  \item The character $\gamma_u$ is unipotently supported since it is the induction of a character of a unipotent subgroup of $G$.
  \item According to \cite[1.3.6]{kawanaka_generalized_1985},  $\Gamma_u \simeq \Gamma_{u'}$ if $u$ and $u'$ are $G$-conjugate. Moreover, according to \cite[2.2]{ho2004finite}, $\mathcal{C} \cap U_{\mathcal{C}, 2}$ contains a complete set of representatives of the $G$-conjugacy classes of $\mathcal{C}^F$. Hence, we can construct a GGGR from any unipotent class of $G$.  
  \end{itemize}
\end{Rk}

\begin{Ex}\item
  \begin{enumerate}
  \item Assume that $\mathcal{C}$ is the trivial class. Then $\U_{\mathcal{C},2}=\{1\}$ and $\gamma_1=\mbox{Ind}_{\{1\}}^G 1$ is the regular representation.
  \item We assume that $F$ is split for more convenience. Let $\mathcal{C}$ be the regular unipotent class and let $u=\prod \limits_{\alpha \in \Phi^+} u_{\alpha}(\lambda_\alpha) \in \mathcal{C} \cap U_{\mathcal{C},2}$. Then $\U_{\mathcal{C},2}=\U$. Let $\U^*:=\prod \limits_{\alpha \in \Phi^+ \setminus \Delta}\U_{\alpha}=\U_{\mathcal{C},3}$. Then,  $\varphi_u$  is trivial on $U^*$. Indeed, if $x:=\prod\limits_{\alpha \in \Phi^+}u_\alpha(\mu_\alpha) \in \U_{\mathcal{C},3}$, then $\mu_\alpha=0$ if $d_{\mathcal{C}}(\alpha)=2$. Moreover, $\varphi_u$ is clearly non trivial on $\prod \limits_{\alpha \in \Delta'}U_\alpha$ for any non-empty subset $\Delta'$ of $\Delta$. Hence, $\varphi_u$ is regular in the sense of \cite[14.27]{digne_representations_1991} and $\gamma_u$ is a Gelfand--Graev character \emph{i.e.} the induced character from $U^*$ to $G$ of a regular character.
  \end{enumerate}  
\end{Ex}



Value of the character a GGGR on a unipotent class is non-zero only under some conditions. For a subset $X$ of $\G$, we denote by $\overbar{X}$ its closure for the Zariski topology.

\begin{Pro}[{\cite[6.14]{lusztig_unipotent_1992}}]
\label{Pro:gener-gelf-graev-2}
  Let $\mathcal{C}, \mathcal{C'}$ be two $F$-stable unipotent classes, $u \in \mathcal{C}^F$ and $v\in \mathcal{C'}^F$. If $\gamma_u(v) \neq 0$, then $\mathcal{C'} \subseteq \overbar{\mathcal{C}}$.
\end{Pro}

% \begin{Rk}
% \label{Rk:gener-gelf-graev-3}
%   According to \cite[8.6]{lusztig_unipotent_1992}, $D_G(\gamma_u)(v)=\zeta \gamma_u(v)$ where $\zeta$ is a non-zero scalar. So we can replace $\gamma_u$ by $D_G(\gamma_u)$ in the proposition above. 
% \end{Rk}

\subsection{Unipotent support and wave front set}\label{sec:unip-supp-wave}
Following \cite{lusztig_unipotent_1992}, we can associate to $\rho \in$ $\Irr_K(G)$ a unique $F$-stable unipotent class $\mathcal{C}_{\rho}$ of $\G$ satisfying the two following conditions:

\begin{enumerate}
\item $\sum\limits_{x\in \mathcal{C}_{\rho}^F} \rho(x) \neq 0$,
\item For any unipotent class $\mathcal{C'}$,  $\sum\limits_{x\in \mathcal{C}'} \rho(x) \neq 0$ implies $ \mathrm{dim \,} \mathcal{C}' \leq \mathrm{dim \,} \mathcal{C}_{\rho}$.
\end{enumerate}
We say that $\mathcal{C}_\rho$ is the \emph{unipotent support} of $\rho$. Using GGGRs, we can also associate to $\rho$ another $F$-stable unipotent class $\mathcal{C}_{\rho}^*$ of  $\G$, the \emph{wave front set} of $\rho$, which is the unique unipotent class satisfying the following conditions:
\begin{enumerate}
\item $\langle \rho, \gamma_u\rangle_{KG}\neq 0$ for some $u \in \mathcal{C}_{\rho}^{*F}$,
\item For any unipotent element $v \in G$, $\langle \rho, \gamma_v\rangle_{KG} \neq 0$ implies $\mathrm{dim \,} (v)_{\G} \leq \mathrm{dim \,} \mathcal{C}_{\rho}^*$.
\end{enumerate}
Unipotent support and wave front set are closely related. More precisely, let $D_G$ be the Alvis--Curtis duality for representations of $G$. This is an operator on $R_K(G)$ with the following properties:
\begin{itemize}
\item $D_G \circ D_G = \mbox{Id}$,
\item $D_G(1_G)=\mbox{St}_G$,
\item For every $\chi \in \Irr_K(G)$, there exists a sign $\varepsilon$ such that $\varepsilon D_G(\chi) \in \Irr_K(G)$.
\end{itemize}

See \cite[\S 8]{digne_representations_1991} for more details.

\begin{Rk}
 $D_G(\gamma_u)$ is unipotently supported. Indeed, $D_G$ is constructed as a linear combination of composition of Harish-Chandra induction and restriction operators (see \cite[8.8]{digne_representations_1991}). Let $\M$ be an $F$-stable levi subgroup of $\G$ contained in an $F$-stable parabolic subgroup $\P$ of $\G$. Let us denote by $R_L^G$ (resp. $^*R_L^G$) the Harish-Chandra induction (resp. restriction). We have by Remark 8.9 of \cite{digne_representations_1991} that
  $R_L^G \circ ^* R_L^G(\gamma_u)$ is the induction of a character of $P$ which is zero on non-unipotent elements. Therefore, $R_L^G \circ ^* R_L^G(\gamma_u)$  is unipotently supported and so is $D_G$.
\end{Rk}

The following result can de viewed as dual to Proposition \ref{Pro:gener-gelf-graev-2}.

\begin{Pro}[{\cite[8.6, 6.13]{lusztig_unipotent_1992}}]
  \label{Pro:supp-dual-gggr}
   Let $\mathcal{C}, \mathcal{C'}$ be two $F$-stable unipotent classes, $u \in \mathcal{C}^F$ and $v\in \mathcal{C'}^F$. If $D_G(\gamma_u)(v) \neq 0$, then $\mathcal{C} \subseteq \overbar{\mathcal{C'}}$.
\end{Pro}
If $\rho \in \Irr_K(G)$, we denote by $\rho^*$ the irreducible character of $G$ such that $\rho^*= \pm D_G(\rho)$. We call $\rho^*$ the dual character of $\rho$. The relation between unipotent support and wave front set comes from this duality:
$$\forall \rho \in \Irr_K(G) \quad \mathcal{C}^*_{\rho}=\mathcal{C}_{\rho^*},$$
see \cite[14.15]{taylor_generalized_2016}. Note that all these properties were first proved under the assumption that $p$ and $q$ were large enough. These were later generalised to the case where $p$ is good prime in \cite{taylor_generalized_2016}. Let us make a important remark about unipotent support and wave front set in regard of the classification of irreducible characters of $G$ that we stated in \S\ref{sec:param-irrg} in the case $Z(\G)$ is connected. Let $g \in G^*$ be a special element with Jordan decomposition $g=su$. Recall that $\Irr_K(G)_g$ is the subset of $\mathcal{E}(G,s)$ which, by the Jordan decomposition, corresponds bijectively to the family of unipotent characters of $C_{G^*}(s)$ corresponding to $u$. Let ${\rho \in \Irr_K(G)_g}$ and  $\Phi$ be the map defined in \ref{Def:Lusztig-map} sending a special conjugacy class of $\G^*$ to a unipotent class of $\G$. According to \cite[11.1, 11.2, 10.5]{lusztig_unipotent_1992}  and the above observations we have:

\begin{Pro}\label{Pro:unip-supp-wave-1}
The unipotent class $\Phi((g)_{\G^*})$ is the unipotent support of $\rho$.
\end{Pro}


\subsection{GGGRs and regular embedding}\label{sec:gggrs-regul-embedd}

We review some properties of GGGRs which will be useful later. Let $\tilde{\G}=\G\tilde{\T}$ be a regular embedding of $\G$. If $g \in \tilde{G}$ and $\chi$ is a character of $G$, we denote by $\chi^g$ the central function of $G$ defined by $\chi^g(x)=\chi(gxg^{-1})$ for $x \in G$. It is a character of $G$. Since $G$ acts trivially on $\chi$, for any $g \in \tilde{G}$ there exists $t \in \tilde{T}$ such that $\chi^g=\chi^t$.

\begin{Pro}
  \label{sec:gggrs-conj}
  Let $u \in G$ be unipotent and $\tilde{t} \in \tilde{T}$. Then $\gamma_u^{\tilde{t}}=\gamma_{\tilde{t}u\tilde{t}^{-1}}$. 
\end{Pro}

\begin{proof}
  Since $\gamma_u=[U_{\mathcal{C}, 1}, U_{\mathcal{C}, 2}]^{-1/2}\mbox{Ind}_{ U_{\mathcal{C}, 2}}^G(\varphi_u)$ we just need to show that  $\varphi_u^{\tilde{t}}=\varphi_{\tilde{t}u\tilde{t}^{-1}}$. Note that since $\G$ is a normal subgroup of $\tilde{\G}$, the adjoint action of $\G$ on $\mathcal{N}_{il}$ can be extended to $\tilde{\G}$. Let $\tilde{g}=gz \in \tilde{\G}$ where $g \in \G$ and $z \in Z(\tilde{\G})$. Then for any $x \in \mathcal{U}_{ni}$ (resp. $n \in \mathcal{N}_{il}$), $\tilde{g} \cdot x= g \cdot x$ (resp. $\tilde{g} \cdot n = g \cdot n$). Hence, $\phi$ is $\tilde{\G}$-equivariant. Let $x \in U_{\mathcal{C},2}$. We have:
  \begin{align*}
    \varphi_{\tilde{t}u\tilde{t}^{-1}}(x)&=\chi_q(\kappa(\phi(\tilde{t}u\tilde{t}^{-1})^*,\phi(x)))\\
                                         &=\chi_q(\kappa(\tilde{t}^{-1} \cdot \phi(u)^*, \phi(x))=\kappa(\phi(u)^*,\tilde{t}\cdot \phi(x)))\\
                                         &=\chi_q(\kappa(\phi(u)^*,\tilde{t} \cdot \phi(x)))\\
                                         &=\chi_q(\kappa(\phi(u)^*, \phi(\tilde{t} x \tilde{t}^{-1})))\\
    &=\varphi_u(\tilde{t}x\tilde{t}^{-1})=\varphi_u^{\tilde{t}}(x).
  \end{align*}
  The second equality comes from the fact that  $(\tilde{t} \cdot n)^* =\tilde{t}^{-1} \cdot n^*$ for $n \in \mathcal{N}_{il}$  and $\tilde{t} \in \tilde{T}$. Indeed, without loss of generality, we can assume that $\tilde{t}=t \in \T$ and $n \in \g_{\alpha}$ for some root $\alpha$. Then, since $n^* \in \g_{-\alpha}$, we have
  $$(t \cdot n)^*=(\alpha(t) n)^*= \alpha(t) n^*=-\alpha(t^{-1}) n^*= t^{-1} \cdot n^*.$$
\end{proof}
%   We can assume that $u \in U_{\mathcal{C},2}$ so we can write $u=\prod \limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} \geq 2}} u_\alpha(\lambda_\alpha)$ where $\lambda_\alpha \in \bar{\F}_q$. Let $\tilde{\Phi}$ the root system of $\tilde{\G}$ determined by $\tilde{\T}$, if $\alpha \in \Phi$ we denote by $\tilde{\alpha} \in \tilde{\Phi}$ the unique root such that $\tilde{\alpha}_{|\T} =\alpha$. Let $t \in \tilde{T}$, then for any $\alpha \in \Phi$ and $\mu_\alpha \in \bar{\F}_q$, $tu_\alpha(\mu_{\alpha})t^{-1}=u_\alpha(\tilde{\alpha}(t)\mu_\alpha)$. Hence, if  $x=\prod \limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} \geq 2}} u_\alpha(\mu_\alpha) \in U_2$, $txt^{-1}=\prod \limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} \geq 2}} u_\alpha(\tilde{\alpha}(t)\mu_\alpha)$ and we have:
%   $$\varphi_u^t(x)= \chi(\kappa(\phi(u)^*,\phi(txt^{-1}))=\chi \left( \sum \limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} = 2}} \lambda_\alpha \tilde{\alpha}(t)\mu_\alpha \kappa(e_\alpha^*,e_\alpha) \right).$$
% Since $tut^{-1}=\prod\limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} \geq 2}} u_\alpha(\tilde{\alpha}(t) \lambda_\alpha))$, we have
% $$\varphi_{tut^{-1}}(x)=
%  \chi(\kappa(\phi(tut^{-1})^*,\phi(x))=\chi \left( \sum \limits_{\substack{\alpha \in \Phi^+ \\  d_{\mathcal{C}} = 2}} \tilde{\alpha}(t) \lambda_\alpha \mu_\alpha \kappa(e_\alpha^*,e_\alpha) \right).$$
% So we are done.  

%\vspace*{0.3cm}

Induction from $G$ to $\tilde{G}$ provides a relation between GGGRs of $\tilde{G}$ and GGGRs of $G$. If $u \in \tilde{G}$ is  unipotent, it also belongs to $G$ so we can associate to $u$ a GGGR of $G$ and a GGGR of $\tilde{G}$. We denote by $\Gamma_u$ the GGGR of $G$ associated to $u$ and by $\tilde{\Gamma}_u$ the GGGR of $\tilde{G}$ associated to  $u$.

\begin{Pro}\label{Pro:gggr-ind}
Let $u \in \tilde{G}$ be unipotent, then
 $$\tilde{\Gamma}_u=\emph{Ind}_G^{\tilde{G}}(\Gamma_u).$$
\end{Pro}

\begin{proof}
  That comes directly from the definition of $\Gamma_u$ and the fact that $\mbox{Ind}_{U_{\mathcal{C},2}}^{\tilde{G}}(\varphi_u)=\mbox{Ind}_G^{\tilde{G}} \circ \mbox{Ind}_{U_{\mathcal{C},2}}^G(\varphi_u)$.  
\end{proof}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../main"
%%% End: 