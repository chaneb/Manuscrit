(TeX-add-style-hook
 "CharSheaves"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (LaTeX-add-labels
    "sec:character-sheaves-3"
    "sec:character-sheaves-1"
    "sec:character-sheaves"
    "sec:character-sheaves-2"
    "Pro:unitr-cond"
    "sec:theorem-gggrs"
    "sec:non-connected-center"
    "eq:1"))
 :latex)

