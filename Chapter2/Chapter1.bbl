\begin{thebibliography}{{Lus}84b}

\bibitem[AL82]{alvis_springers_1982}
Dean Alvis and George Lusztig.
\newblock On {Springer}'s correspondence for simple groups of type {En}
  (n=6,7,8).
\newblock {\em Mathematical Proceedings of the Cambridge Philosophical
  Society}, 92(1):65--72, July 1982.

\bibitem[Bon05]{bonnafe_quasi-isolated_2005}
Cédric Bonnafé.
\newblock Quasi-{Isolated} {Elements} in {Reductive} {Groups}.
\newblock {\em Communications in Algebra}, 33(7):2315--2337, June 2005.

\bibitem[Bon11]{bonnafe_representations_2011}
Cédric Bonnafé.
\newblock {\em Representations of {SL}2({Fq})}.
\newblock Algebra and {Applications}. Springer-Verlag, London, 2011.

\bibitem[Car85]{carter_finite_1985}
R.~W. Carter.
\newblock {\em Finite {Groups} of {Lie} {Type}: {Conjugacy} {Classes} and
  {Complex} {Characters}}.
\newblock Wiley, New York, 1985.

\bibitem[CE04]{cabanes_representation_2004}
Marc Cabanes and Michel Enguehard.
\newblock {\em Representation {Theory} of {Finite} {Reductive} {Groups}}.
\newblock Cambridge University Press, January 2004.

\bibitem[CR87]{CR892316}
Charles~W. Curtis and Irving Reiner.
\newblock {\em Methods of representation theory. {V}ol. {II}}.
\newblock Pure and Applied Mathematics (New York). John Wiley \& Sons, Inc.,
  New York, 1987.
\newblock With applications to finite groups and orders, A Wiley-Interscience
  Publication.

\bibitem[Den17]{denoncin_stable_2017}
David Denoncin.
\newblock Stable basic sets for finite special linear and unitary groups.
\newblock {\em Advances in Mathematics}, 307:344--368, February 2017.

\bibitem[Dip85]{dipper_decomposition_1985}
Richard Dipper.
\newblock On the {Decomposition} {Numbers} of the {Finite} {General} {Linear}
  {Groups}.
\newblock {\em Transactions of the American Mathematical Society},
  290(1):315--344, 1985.

\bibitem[DM91]{digne_representations_1991}
François Digne and Jean Michel.
\newblock {\em Representations of {Finite} {Groups} of {Lie} {Type}}.
\newblock Cambridge University Press, April 1991.

\bibitem[Gec91]{geck_decomposition_1991}
Meinolf Geck.
\newblock On the decomposition numbers of the finite unitary groups in
  non-defining characteristic.
\newblock {\em Mathematische Zeitschrift}, 207(1):83--89, May 1991.

\bibitem[Gec93]{geck_basic_1993}
Meinolf Geck.
\newblock Basic {Sets} of {Brauer} {Characters} of {Finite} {Groups} of {Lie}
  {Type} {II}.
\newblock {\em Journal of the London Mathematical Society}, s2-47(2):255--268,
  April 1993.

\bibitem[Gec94]{geck_basic_1994}
Meinolf Geck.
\newblock Basic sets of {Brauer} characters of finite groups of {Lie} type,
  {III}.
\newblock {\em manuscripta mathematica}, 85(1):195--216, December 1994.

\bibitem[GH91]{geck_basic_1991}
Meinolf Geck and Gerhard Hiss.
\newblock Basic sets of {Brauer} characters of finite groups of {Lie} type.
\newblock {\em Journal für die reine und angewandte Mathematik}, 418:173--188,
  1991.

\bibitem[GH97]{geck_modular_1997}
Meinolf Geck and Gerhard Hiss.
\newblock Modular {Representation} of {Finite} {Groups} of {Lie} {Type} in
  {Non}-defining {Characteristic}.
\newblock In {\em Proceedings of an {International} {Conference} on {Finite}
  {Reductive} {Groups} : {Related} {Structures} and {Representations}:
  {Related} {Structures} and {Representations}}, pages 195--249, Cambridge, MA,
  USA, 1997. Birkhauser Boston Inc.

\bibitem[GM00]{10.2307/118160}
Meinolf Geck and Gunter Malle.
\newblock On the existence of a unipotent support for the irreducible
  characters of a finite group of lie type.
\newblock {\em Transactions of the American Mathematical Society},
  352(1):429--456, 2000.

\bibitem[HS10]{Hiss20102BLOCKSA2}
Gerhard Hiss and JOSEPHINE SHAMASH.
\newblock 2-blocks and 2-modular characters of the chevalley groups g 2 ( q ).
\newblock 2010.

\bibitem[KT09]{kleshchev_representations_2009}
Alexander~S. Kleshchev and Pham~Huu Tiep.
\newblock Representations of finite special linear groups in non-defining
  characteristic.
\newblock {\em Advances in Mathematics}, 220(2):478--504, January 2009.

\bibitem[LS12]{liebeck_unipotent_2012}
Martin~W. Liebeck and Gary~M. Seitz.
\newblock {\em Unipotent and {Nilpotent} {Classes} in {Simple} {Algebraic}
  {Groups} and {Lie} {Algebras}}.
\newblock American Mathematical Soc., January 2012.

\bibitem[Lus79]{LUSZTIG1979323}
G.~Lusztig.
\newblock A class of irreducible representations of a weyl group.
\newblock {\em Indagationes Mathematicae (Proceedings)}, 82(3):323 -- 335,
  1979.

\bibitem[Lus81]{LUSZTIG1981169}
G~Lusztig.
\newblock Green polynomials and singularities of unipotent classes.
\newblock {\em Advances in Mathematics}, 42(2):169 -- 178, 1981.

\bibitem[Lus84a]{Lusztig1984}
G.~Lusztig.
\newblock Intersection cohomology complexes on a reductive group.
\newblock {\em Inventiones mathematicae}, 75:205--272, 1984.

\bibitem[{Lus}84b]{lusztig_g._characters_1984}
{Lusztig, G.}
\newblock Characters of {Reductive} {Groups} over a {Finite} {Field}.
  ({AM}-107), {Volume} 107, 1984.

\bibitem[Mic15]{michel_development_2015}
Jean Michel.
\newblock The development version of the {CHEVIE} package of {GAP}3.
\newblock {\em Journal of Algebra}, 435:308--336, August 2015.

\bibitem[Sho79]{shoji_springer_1979}
Toshiaki Shoji.
\newblock On the springer representations of the weyl groups of classical
  algebraic groups.
\newblock {\em Communications in Algebra}, 7(16):1713--1745, January 1979.

\bibitem[Sho80]{doi:10.1080/00927878008822466}
Toshiaki Shoji.
\newblock On the {S}pringer representations of {C}hevalley groups of type
  ${F}_4$.
\newblock {\em Communications in Algebra}, 8(5):409--440, 1980.

\bibitem[SI74]{shoji_conjugacy_1974}
Toshiaki Shoji and N~Iwahori.
\newblock The conjugacy classes of {Chevalley} groups of type ${F}_4$ over
  finite fields of characteristic 2.
\newblock August 1974.

\bibitem[Spa77]{SPALTENSTEIN1977203}
Nicolas Spaltenstein.
\newblock On the fixed point set of a unipotent element on the variety of borel
  subgroups.
\newblock {\em Topology}, 16(2):203 -- 204, 1977.

\bibitem[Spr76]{springer_trigonometric_1976}
T.~A. Springer.
\newblock Trigonometric {Sums}, {Green} {Functions} on {Finite} {Groups} and
  {Representation} of {Weyl} {Groups}.
\newblock {\em Inventiones mathematicae}, 36:173--208, 1976.

\bibitem[Spr78]{springer_construction_1978}
T.~A. Springer.
\newblock A {Construction} of {Representations} of {Weyl} {Groups}.
\newblock {\em Inventiones mathematicae}, 44:279--293, 1978.

\end{thebibliography}
