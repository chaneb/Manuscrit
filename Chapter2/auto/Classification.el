(TeX-add-style-hook
 "Classification"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (LaTeX-add-labels
    "sec:spec-repr-famil"
    "Ex:famil-B2"
    "sec:extension-Q"
    "sec:groups-assoc-famil"
    "sec:MGamma"
    "sec:lusztigs-theorem"
    "sec:non-unip-char"
    "Ex:springer"
    "table:Springer-Sp4"
    "table:Springer-G2"
    "sec:spring-constr-repr"
    "Ex:speclass"
    "sec:canonical-quotient"
    "sec:param-unip"
    "sec:param-irrg"))
 :latex)

