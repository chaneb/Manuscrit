\section{Classical types}
\label{sec:classical-types}
In this section, unless otherwise stated, $\G$ will be a connected reductive group defined over $\F_q$ with $q$ odd whose simple components are of type $B$, $C$ or $D$. In that case $Z(\G)/Z(\G)^\circ$ is a 2-group so the only prime which is not very good for $G$ is 2. Hence, we  suppose $\ell=2$.

\paragraph{A remark on the groups $\Omega_u^2$.} Since $2$ is the only bad prime of $G$, every unipotent class of $\G$ is $2$-special (see Section \ref{sec:isolated-classes}). Let $\mathcal{C}$ be an $F$-stable unipotent class of $\G$ and $u \in \mathcal{C}^F$. The following lemma gathers information about $A_\G(u)$.

\begin{Lem}
  \label{Lem:AGu-ab-class}
 The group $A_\G(u)$ is a 2-group. Moreover if $\G$ is a simple group which is neither spin or half-spin, $A_\G(u)$ is isomorphic to a product of copies of $\Z / 2 \Z$.
\end{Lem}

\begin{proof}
The fact that $A_{\G}(u)$ is a 2-group has been shown in the proof of \ref{The:count-unip-good} so we just have to show the second assumption. The results in \cite[3.1, 3.3.5]{liebeck_unipotent_2012} show that $A_\G(u)$ is of the form $(\Z / 2 \Z)^k$ if $\G$ is a simple orthogonal or a symplectic group. The only groups left are the adjoint groups of type $C$ and $D$. If $\G$ is a simple adjoint group of type $C_n$ (resp. $D_n$), then $\G$ is a quotient of a symplectic group (resp. simple orthogonal group). Therefore, $A_\G(u)$ is the quotient of a group of the form $(\Z / 2 \Z)^k$ and the conclusion follows.  
\end{proof}

\vspace*{0.3cm}

The group $A_{\G}(u)$ being a $2$-group, the only indecomposable projective $kA_\G(u)$-module (resp. irreducible $kA_\G(u)$-module) is the regular representation (resp. the trivial representation). This shows that $\Omega_u^2 = A_\G(u)$ so  $\bar{\mathcal{M}}_2(\Omega^2_u)$ corresponds bijectively to the set of $F$-conjugacy classes of $A_{\G}(u)$. By \ref{Ex:app-lang-steinb}, the $F$-conjugacy classes of $A_\G(u)$ parametrise the $G$-orbits in $(u)_{\G}^F$. Summing over all unipotent $\G$-conjugacy classes, we have that $\alpha_2$ is the number of unipotent classes of $G$. Hence, showing that the conjecture holds for $G$ is equivalent to showing the following result.

\begin{The}\label{The:Countclasses}
 Assume that $\G/Z(\G)^\circ$ is a simple group of type $B$, $C$ or $D$. The number of unipotent modular representations of $G$ for $\ell=2$ equals the number of unipotent classes of $G$.
\end{The}

If $\G$ has connected center, that proposition was shown by Geck in \cite{geck_basic_1994}. The remainder of this section will be devoted to showing that we can generalise this result to simple classical groups with non-connected center.

\paragraph{Preliminary results.}\index{$m_s(G)$}From now on, for a semi-simple element of odd order of $G^*$, we will denote by $m_s$ the number of irreducible modular representations lying in $B_s$ (or $m_s(G)$ if we need to specify the underlying group $G$). In \cite{geck_basic_1994}, Geck showed that for $\G$ with connected center and  simple components of classical type, $m_1(G)$ is equal to the number of unipotent classes of $G$. We first recall that the centraliser in $\G$ of a semi-simple element of odd order is a rational Levi subgroup of $\G$.

\vspace{0.64cm}

\begin{Lem}
Let $\G$ be a connected reductive group such that $Z(\G)/Z(\G)^\circ$ is a $2$-group.

\begin{enumerate}
\item[$\mathrm{(1)}$] For any Levi subgroup $\M$ of $\G$, $Z(\M)/Z(\M)^\circ$ is a $2$-group.
\item[$\mathrm{(2)}$] If $s$ is a semi-simple element of odd order of $\G^*$, then $C_{\G^*}(s)$ is connected. Moreover, if the order of $s$ is divisible by good primes only then $C_{\G^*}(s)$ is a rational Levi subgroup of $\G$.
\end{enumerate}
\end{Lem}
\begin{proof}
%  \vspace{0.1cm}
Let $X$ be the group of characters of a rational torus $\T$ contained in $\M$, $\Phi$ be the corresponding set of roots of $\G$ and $\Phi_{\M} \subset \Phi$ be the set of roots of $\M$.  Given $A$ a subgroup of $X$ and $\mathbf{S}$ a subtorus of $\T$ we define:
$$\begin{aligned}
 A^{\perp}& \, :=\{t \in \T \, \mid\, \chi (t)=0 \ \forall \chi \in A\}, \\
\mathbf{S}^{\perp}& \, :=\{\chi \in X \, \mid\, \chi (t)=0 \ \forall t \in \mathbf{S} \}.
\end{aligned}$$    
Note that $A \subset A^{\perp \perp}$ but there is no equality in general. The following properties can be found for example in  \cite[ 0.24, 13.14, 13.15]{digne_representations_1991}.

 \begin{enumerate}[label=(\alph*)]
\item $Z(\G)/Z(\G)^{\circ}$ is isomorphic to the torsion group of $X/\Z \Phi^{\perp \perp}$.
\item $A^{\perp \perp}/A$ is the $p$-torsion subgroup of $X/A$.
\item If $(\G^*,\T^*)$ is dual to $(\G, \T)$, then for any $s \in \T^*$ the group $C_{\G^*}(s)/C_{\G^*}(s)^\circ$ is isomorphic to a subgroup of $Z(\G)/Z(\G)^{\circ}$.
\item The exponent of $C_{\G^*}(s)/C_{\G^*}(s)^{\circ}$ divides the order of $s$.
\end{enumerate}
\smallskip{}

By (a), the group $Z(\G)/Z(\G)^{\circ}$ is a $2$-group if and only if $X /\Z \Phi ^{\perp \perp}$ has only $2$-torsion. This is equivalent to $X/\Z \Phi$ having only $2$ and $p$-torsion by observing that  
  $$X /Z \Phi^{\perp \perp} \simeq (X/ \Z \Phi) / (\Z \Phi^{\perp \perp}/\Z \Phi)$$  
  and then applying (b) with $A=\Z \Phi$. Moreover, $\Z \Phi_{\M}$ is a direct summand of $\Z \Phi$. Indeed, we can assume that we are in the setting of \S \ref{sec:parab-levi-subgr} so that $\Delta$ is a basis of $\Phi$ and $\Z \Phi_L$ is generated by a subset $ \Delta_I$ of $\Delta$. Therefore, $X/\Z \Phi_{\M}$ has only $2$-torsion and $p$-torsion. Indeed, suppose $X/\Z \Phi_{\M}$ has $m$-torsion where $m$ is an integer prime to $2$ and $p$. Then there exists an element $\chi \in X$ such that $m \chi \in \Z \Phi_{\M}$, but  that implies that $\chi \in \Z \Phi$ because $X/\Z \Phi$ has only $2$ and $p$-torsion. The fact that $\chi \in \Z \Phi$ and $m \chi \in \Z \Phi_{\M}$  forces $\chi \in \Z \Phi_L$ since $\Z \Phi_{\M}$ is a direct summand of the free $\Z$-module $\Z \Phi$. Therefore $X /\Z \Phi_{\M}$ has no $m$-torsion. By (b), this is equivalent to $Z(\M)/Z(\M)^\circ$ being a $2$-group. That proves (1). 

\smallskip
By (c), $C_{\G^*}(s)/C_{\G^*}(s)^{\circ}$ is isomorphic to a subgroup of $Z(\G)/Z(\G)^{\circ}$, hence is a $2$-group. But the exponent of this group divides the order of $s$ by (d) so $C_{\G^*}(s)/C_{\G^*}(s)^{\circ}$ is trivial and $C_{\G^*}(s)$ is connected. Finally, the connectedness of $C_{\G^*}(s)$ and  Proposition 2.1 of \cite{geck_basic_1991} implies that $C_{\G^*}(s)$ is a rational Levi subgroup of $\G^*$ which gives (2).
\end{proof}

\vspace{0.3cm}

\begin{Cor}\label{cor:levi}
If $\G$ is a reductive group with simple components of type $B$, $C$ or $D$ and $s$ is a semi-simple element of odd order of $G$, then  $C_{\G}(s)$ is a rational Levi subgroup of $\G$.
\end{Cor}

%\vspace*{0.5cm}

Let $S_{2'}(G)$ be a set of representatives of $G$-conjugacy classes of semi-simple elements of odd order of $G$. The result in \cite[4.2]{geck_basic_1991} gives, under some conditions, a bijection between $S_{2'}(G)$ and $S_{2'}(G^*)$ which is one of the main ingredients to prove Theorem \ref{The:Countclasses}. We need a slight modification of the statement to be able to get the same bijection when the center is not connected. If $s \in S_{2'}(G)$, we denote by $C_{\G}(s)^*$ a Levi subgroup of $\G^*$ dual to $C_{\G}(s)$ (we recall that by the lemma above and under the assumptions on $\G$, $C_{\G}(s)$ is a rational Levi subgroup of $\G$). 

\vspace*{0.3cm}

\begin{Pro}[{\cite[14.1]{cabanes_representation_2004}}]
  \label{Pro:lelement}
  Let $\ell$ be a prime number dividing neither $q$, $|(Z(\G)/Z(\G)^\circ)^F|$ or $|(Z(\G^*)/Z(\G^*)^\circ)^F|$. There is a one-to-one map from the set of $G$-conjugacy classes of $\ell$-element of $G$ onto the set of $G^*$-conjugacy classes of $\ell$-elements of $G^*$. If the class of $x \in G_\ell$ maps to the class of  $y \in G^*_\ell$, then there is a isomorphism defined over $\F_q$ between ${C_\G(x)^{\circ}}^*$ and $C_{\G^*}(y)^{\circ}$. 
\end{Pro}


Applying this result for classical groups, we get:

\begin{Cor}
  \label{Cor:oddelement}
 Assume that $\G$ is a reductive group with simple components of type $B, C$ or $D$. There is a bijection $$S_{2'}(G) \to S_{2'}(G^*) \quad t \mapsto t'$$ such that there is an isomorphism $C_{\G}(t)^* \simeq C_{\G^*}(t')$ defined over $\F_q$. The results still holds for any $F$-stable  Levi subgroup of $\G$.
\end{Cor}

\begin{proof}
  Let $\ell$ be an odd prime number. Remark that if $\ell$ divides $q$, the only semi-simple $\ell$-element if trivial, so we can assume that $\ell$ does not divides $q$. The group $\G$ being of type $B$, $C$ or $D$, $(Z(\G)/Z(\G)^\circ)^F$ and $(Z(\G^*)/Z(\G^*)^\circ)^F$ are 2-groups. Moreover, the centralisers of semi-simple elements of odd order of $\G^*$ are connected by \ref{cor:levi} (2), so we can apply \ref{Pro:lelement} to $\G$ for any odd prime number to get the result. Using \ref{cor:levi} (1) for $\G$ and $\G^*$, we can use the same arguments for any $F$-stable Levi subgroup of $\G$. 
\end{proof}

\paragraph{Proof of  Theorem \ref{The:Countclasses}.}
We will proceed by induction on the dimension of $\G$: let $s \in S_{2'}(G^*)$ and $\M_s$ be a rational Levi subgroup of $\G$ dual to the Levi subgroup  $C_{\G^*}(s)$ of $\G^*$. We denote by $u(L_s)$ the number of unipotent classes of $L_s$. If $s$ in non-central, then $\dim \M_s <  \dim \G$, so
  $$u(L_s)=m_1(L_s)=m_s(G).$$
The first equality comes from the induction hypothesis, the second one from the Morita equivalence between $B_1(L_s)$ and $B_s(G)$ (see Theorem \ref{The:Bonnafe-Rouquier}). Let $a$ be the number of $2$-regular classes of $G$. Using the fact that $a$ is also the number of elements in $\Irr_k(G)$, we have
$$\begin{aligned}
    a & =\sum_{s \in S_{2'}(G^*)} m_s(G)\\
        &=\sum_{\underset{s\notin Z(G^*)}{s \in S_{2'}(G^*)}} u(L_s) + \sum \limits_{s\in Z(G^*)_{2'}} m_s(G)\\
    & =\sum_{\underset{s\notin Z(G^*)}{s \in S_{2'}(G^*)}} u(L_s)  + |Z(G^*)_{2'}| \cdot m_1(G)  .  
 \end{aligned}$$
The last equality comes from the fact that for any central element $s$  of odd order of $G^*$, tensoring by the linear character of $C_{\G^*}(s)$ attached to $s$ as in \cite[13.30]{digne_representations_1991} provides a natural isomorphism between $B_1(G)$ and $B_s(G)$, so $m_1(G)=m_s(G)$. By using the fact that $a$ is the number of $2$-regular classes of $G$ we also have:
$$\begin{aligned} a = &\, \sum_{\underset{s\notin Z(G)}{s \in S_{2'}(G)}} u(C_G(s)) + |Z(G)_{2'}| \cdot u(G)  \\
= & \, \sum_{\underset{s\notin Z(G)}{s \in S_{2'}(G)}} u(L_{s'}) + |Z(G)_{2'}| \cdot u(G)
\end{aligned}$$
where the second equality comes from the fact that the isomorphism $C_\G(s)^* \simeq C_{\G^*}(s')$ is defined over $\F_q$. 
\smallskip

To conclude it remains to show that $|Z(G)_{2'}|=|Z(G^*)_{2'}|$. This comes from the fact that the bijection  $S_{2'}(G^*) \simeq S_{2'}(G)$ of Corollary \ref{Cor:oddelement} induces a bijection between central elements of odd order of $G$ and $G^*$.

\rightline{$\qed$}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
