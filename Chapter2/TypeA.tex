\section{Type $A_n$}
\label{sec:type-a_n}

Throughout this section, $\tilde{\G}$ will denote $\mathrm{GL}_n(\overline{\F}_q)$, $F$ will be a Frobenius endomorphism of $\tilde{\G}$ and, depending on $F$, $\tilde{G}:=\tilde{\G}^F$ will be either $\mathrm{GL}_n(q)$ or GU$_n(q)$. Similarly, $\G$ will denote SL$_n(\overline{\F}_q)$ and $G$ will be either SL$_n(q)$ or SU$_n(q)$. Note that according to Theorem \ref{The:count-unip-good}, the conjecture holds for $\tilde{G}$. Actually, Dipper and Geck have shown a stronger result.

\begin{The}[\cite{dipper_decomposition_1985, geck_decomposition_1991}]
The set  $\mathcal{U}(\tilde{G})$ is a unitriangular basic set for unipotent blocks of $\tilde{G}$.  
\end{The}


\paragraph{Restriction to $G$.} Kleshchev--Tiep \cite{kleshchev_representations_2009} and Denoncin \cite{denoncin_stable_2017} showed the existence of a unitriangular basic set for unipotent blocks of $G$. We will briefly explain how they proceeded. Since $G$ is of type $A$, any unipotent class is special and $F$-stable so we can directly work with unipotent classes of $\G$ which are parametrised by partitions of $n$ via the Jordan normal form. For any partition $\lambda$, we denote by $u_{\lambda}$ a representative of the corresponding unipotent class in $\G$ and by $\rho_{\lambda}$ the unipotent character corresponding to $(u_{\lambda})_{\tilde{\G}}$.

\begin{Ex}
  \item
  \begin{enumerate}
  \item The class $(u_{1^n})_{\tilde{\G}}$ is the class containing the
    unit element and $\rho_{1^n}$ is the Steinberg representation.
\item The class $(u_{n})_{\tilde{\G}}$ is the regular class, \emph{i.e.} the unique class of maximal dimension of $\tilde{\G}$. The representation $\rho_{n}$ is the trivial one.
\end{enumerate}

  
\end{Ex}

\vspace*{0.2cm}

Even if unipotent characters form a unitriangular basic set for the union of unipotent blocks $B_1(\tilde{G})$ of $\tilde{G}$, irreducible constituents of their restriction to $G$ do not form a basic set for $B_1(G)$. That is why, starting from $\mathcal{U}(G)$, we need to construct another basic set for  $B_1(\tilde{G})$. Let $\lambda:=(\lambda_1, \dots, \lambda_r)$ be a partition of $n$,  $m_{\lambda}:=\gcd( \lambda_1, \dots, \lambda_r)_{p'}$ and $m_{\lambda,F}:=\gcd(|Z(\tilde{G})|, \lambda_1, \dots, \lambda_r)_{p'}$. If the number of irreducible constituents of Res$_{G}^{\tilde{G}}(\rho_\lambda)$ is different from $(m_{\lambda,F})_\ell$, we replace $\rho_{\lambda}$ by another character as follows. Let us choose a semi-simple $\ell$-element $s$ as in \cite[4.4]{denoncin_stable_2017}. More precisely, if $\omega$ is a primitive $(m_{\lambda,F})_\ell$-root of unity, we choose $s$ such that the set of its eigenvalues is $\{\omega^i \mid 0\leq i \leq (m_{\lambda,F})_\ell-1 \}$, each eigenvalue having multiplicity $d_\lambda:=\frac{n}{(m_{\lambda,F})_\ell}$. Using the Jordan decomposition and the fact that  $C_{\tilde{\G}}(s)$ is isomorphic to $(m_{\lambda,F})_\ell$ copies of GL$_{d_\lambda}(\bar{\F}_q)$, we can parametrise $\mathcal{E}(\tilde{G},s)$ by collections  $\{ \delta^i \mid 0 \leq i \leq (m_{\lambda,F})_\ell-1\}$ of partitions of $d_\lambda$. If $\delta$ is such a collection we denote by $\rho_{s,\delta}$ the corresponding character. Let us denote by $\frac{\lambda}{(m_{\lambda,F})_\ell}$ the partition $(\frac{\lambda_1}{(m_{\lambda,F})_\ell}, \dots, \frac{\lambda_r}{(m_{\lambda,F})_\ell})$ of $\frac{n}{(m_{\lambda,F})_\ell}$. Then we  replace $\rho_\lambda$, by $\rho_{s,\delta}$ where $\delta^i:=\frac{\lambda}{(m_{\lambda,F})_\ell},0 \leq i \leq n$.

\vspace*{0.2cm}

Applying the above procedure for each element $\mathcal{U}(G)$, we obtain a set $\mathcal{B}_{\tilde{G}}$ of irreducible characters of $\tilde{G}$ which is a unitriangular basic set for $B_1(\tilde{G})$. Let $\mathcal{B}_G$ be the set consisting of irreducible constituents of the restrictions of characters of $\mathcal{B}_{\tilde{G}}$ to $G$. Using Clifford Theory, Kleshchev--Tiep and Denoncin showed that $\mathcal{B}_G$ is a unitriangular basic set for $B_1(G)$. Moreover, for any partition $\lambda$ of $n$, the restriction to $G$ of the character of $\mathcal{B}_{\tilde{G}}$ corresponding to $\lambda$ has $(m_{\lambda,F})_\ell$ irreducible constituents. Hence, to show that Conjecture \ref{Con:count-unip} holds for $G$, it is enough to show that for any partition $\lambda$ of $n$, $\alpha_{\ell,u_{\lambda}}(G)=(m_{\lambda,F})_\ell$.


\paragraph{$\ell$-canonical quotient for $G$.} If $\lambda$ is a partition of $n$, $A_{\G}(u_\lambda)$ is a cyclic group of order $m_\lambda$ \cite[10.3]{Lusztig1984}. Let us now compute $\Omega^{\ell}_{u_{\lambda}}$. With the notation of \ref{Def:ell-special-quo}, recall that $\Omega^{\ell}_{u_{\lambda}}$ is the smallest quotient of $A_{\G}(u_{\lambda})$ through which every projective character in $\mathcal{S}_{\ell}$ factors. The only representation of $A_{\G}(u_{\lambda})$ whose Springer correspondent is non-zero is the trivial representation. Using the fact that the decomposition of the projective indecomposable into irreducible is given by the transpose of the decomposition map, projective characters in $\mathcal{S}_\ell$ are sum of the irreducible characters whose image by the decomposition map are trivial. Since $A_\G(u_\lambda)\simeq \Z /(m_\lambda)_\ell \Z \times \Z / (m_\lambda)_{\ell'} \Z$, the only projective representation in $\mathcal{S}_\ell$ is $k (\Z /(m_\lambda)_\ell \Z) \otimes_k 1_{\Z / (m_\lambda)_{\ell'} \Z}$
whose kernel is the cyclic subgroup of $A_{\G}(u_{\lambda})$ of order $(m_{\lambda})_{\ell'}$. Hence, $\Omega^{\ell}_{u_{\lambda}}$ is a cyclic group of order $(m_{\lambda})_{\ell}$, in particular it is an $\ell$-group so for any $x \in \tilde{\Omega}^{\ell}_u$, $|\Irr_k(C_{\Omega^{\ell}_{u_{\lambda}}}(x))|=1$. Hence, $|\bar{\mathcal{M}}_\ell(\Omega^{\ell}_{u_{\lambda}})|$ is the number of $F$-classes of $\Omega^{\ell}_{u_{\lambda}}$, that is $(m_{\lambda})_\ell$. Indeed, $F$ acts on $\Omega^\ell_{u_\lambda}$ by multiplication by $\varepsilon q$ where $\varepsilon=1$ if $\tilde{G}=\mbox{GL}_n(q)$ and $\varepsilon=-1$ if $\tilde{G}=\mbox{GU}_n(q)$. The $F$-conjugacy classes of $\Omega^{\ell}_{u_{\lambda}}$ are of the form $x+ (q+\varepsilon).\Omega^\ell_{u_\lambda}$ where $x \in \Omega^\ell_{u_\lambda}$. Hence, since $q+\varepsilon=|Z(\tilde{G})|$, the number of $F$-conjugacy classes of  is $\frac{|\Omega^\ell_{u_\lambda}|}{|(q+\varepsilon).\Omega^\ell_{u_\lambda}|}=(m_{\lambda,F})_\ell$. Finally, as conjectured, the number of unipotent modular representations of $G$ is $\alpha_\ell(G)$. 

% Let  $\xi \in \mathcal{O}^*$ be a primitive $m$-th root of unity, $\gamma$ be a generator of $A_G(u)$ and $\phi :A_\G(u) \to K^*$ be the linear character defined by $\phi(\gamma)=\xi$. Let $\bar{\xi}$ be the image of $\xi$ in $k$. It is an $(m_\lambda)_{\ell'}$-root of unity. Let $\bar{\phi}:=d(\phi)$ the linear modular representation sending $\gamma$ to $\bar{\xi}$. Then the decomposition numbers of $A_\G(u)$ are the following: 

% $$d_{\phi^k,\bar{\phi}^i}=\begin{cases} 1 &\mbox{if } i=k \quad\mbox{mod } (m_{\lambda})_{\ell'}, \\ 
% 0 & \mbox{otherwise.}  \end{cases}$$

% %\begin{blockarray}{cccccc}
% %   & 1 & \cdots & \bar{\phi}^i & \cdots & \bar{\phi}^{(m_\lambda)_{\ell'}-1} \\
% %   1      & 1 & \cdots & 0 & \cdots & 0 \\
% %   \vdots & \vdots  & \ddots & \vdots  & \ddots & \vdots \\
% %   \phi^i & 0 & \cdots & 1 & \cdots & 0  \\
% %   \vdots & \vdots  & \ddots & \vdots  & \ddots & \vdots \\
% %   \phi^{(m_\lambda)_{\ell'}-1} & 0 & \cdots & 0 & \cdots & 1 \\
% %   \end{blockarray}

%   Hence, the only indecomposable projective character in $\mathcal{S}_\ell$ is $\sum\limits_{i=0}^{(m_{\lambda})_{\ell}-1}\phi^{i(m_{\lambda})_{\ell'}}$
\begin{Ex}
  Assume $G=\mbox{SL}_2(q)$, $\tilde{G}=\mbox{GL}_2(q)$ and $\ell=2$. Let $\tilde{T}$ (resp. $T$) the maximal torus consisting of diagonal matrices of $\tilde{G}$ (resp. $G$). Since $\tilde{\chi}_2$ and $\tilde{\chi}_{1^2}$ are respectively the trivial and the Steinberg characters, their restriction to $G$ remain irreducible. But $m_2=\gcd(2,q-1)_2=2$ so we should replace the Steinberg character by an irreducible character lying in $\mathcal{E}(\tilde{G},s)$ where

  $$s=
  \begin{pmatrix}
    1 &0 \\ 0&-1
  \end{pmatrix}.$$
Since $C_{\tilde{G}}(s) = \tilde{T} $, $\mathcal{E}(\tilde{G},s)$ contains a unique character that we will denote by $\tilde{\chi}_s$. Moreover, $\tilde{\chi}_s=R_{\tilde{T}}^{\tilde{G}}(\tilde{\theta})$ where $\tilde{\theta}$ is a character of order 2 of $\tilde{T}$ whose restriction $\theta$ to $T$ is not trivial. According to \cite[15.15]{cabanes_representation_2004}, Res$_G^{\tilde{G}}(\tilde{\chi}_s)=R_T^G(\theta)$. Moreover, by \cite[\S 15.9]{digne_representations_1991}, $R_T^G(\theta)$ has two irreducible constituents that we will denote by $\chi_s^+$ and $\chi_s^-$. Hence, $\{\chi_{1^2}, \chi_s^+, \chi_s^-\}$ form a unitriangular basic set for the unipotent blocks of $G$ and we can view  $\chi_{1^2}$ as "attached" to $u_{1^2}$ and $\{\chi_s^+, \chi_s^- \}$ as "attached" to $u_2$.

  
\end{Ex}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
