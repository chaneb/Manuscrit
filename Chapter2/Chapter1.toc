\contentsline {section}{\numberline {0.1}Classification of unipotent characters}{2}{section.0.1}
\contentsline {subsection}{\numberline {0.1.1}On representations of $W$}{2}{subsection.0.1.1}
\contentsline {paragraph}{Fake and generic degrees}{2}{section*.3}
\contentsline {paragraph}{Special representations and families}{2}{section*.4}
\contentsline {subsection}{\numberline {0.1.2}Lusztig's theorem}{4}{subsection.0.1.2}
\contentsline {paragraph}{Families of unipotent characters}{4}{section*.5}
\contentsline {paragraph}{Groups associated to families}{5}{section*.6}
\contentsline {paragraph}{Lusztig's theorem}{6}{section*.7}
\contentsline {paragraph}{Non unipotent characters}{7}{section*.8}
\contentsline {section}{\numberline {0.2}Springer correspondence}{7}{section.0.2}
\contentsline {subsection}{\numberline {0.2.1}Springer's construction of representations of $W$}{7}{subsection.0.2.1}
\contentsline {subsection}{\numberline {0.2.2}Canonical quotient}{10}{subsection.0.2.2}
\contentsline {subsection}{\numberline {0.2.3}Parametrisation of $\mathrm {Irr}_\mathbb {C}(G)$ by special classes}{11}{subsection.0.2.3}
\contentsline {section}{\numberline {0.3}$\ell $-special classes and $\ell $-canonical quotient}{11}{section.0.3}
\contentsline {paragraph}{Truncated induction}{12}{section*.9}
\contentsline {paragraph}{$\ell $-special classes and $\ell $-canonical quotient}{13}{section*.10}
\contentsline {section}{\numberline {0.4}Type $A_n$}{14}{section.0.4}
\contentsline {paragraph}{Restriction to $G$}{15}{section*.11}
\contentsline {paragraph}{$\ell $-canonical quotient for $G$}{15}{section*.12}
\contentsline {section}{\numberline {0.5}Classical type}{16}{section.0.5}
\contentsline {paragraph}{A remark on the groups $\Gamma _u^2$}{16}{section*.13}
\contentsline {paragraph}{Preliminary results}{17}{section*.14}
\contentsline {paragraph}{Proof of Theorem \ref {The:Countclasses}}{18}{section*.15}
\contentsline {section}{\numberline {0.6}Exceptional groups}{19}{section.0.6}
\contentsline {paragraph}{Image of the Springer correspondence }{19}{section*.16}
\contentsline {paragraph}{Computing $\alpha _\ell $}{20}{section*.17}
