\section{Exceptional groups}
\label{sec:exceptional-groups}
For simple exceptional groups of adjoint  type, the number of unipotent modular representation was determined by Geck--Hiss (see \cite[6.6]{geck_modular_1997}).

$$\begin{array}{|c|cccc|}
  \hline
  \text{Type} & \ell=2 & \ell=3 & \ell=5 & \ell \text{ good}\\
    \hline
  G_2 & 8 & 9 & & 10\\
  \hline
  F_4 & 28 & 35 & & 37\\
  \hline
  E_6, {^2E_6} & 27 & 28 & & 30 \\
  \hline
  E_7 & 64 & 72 & & 76 \\
  \hline
  E_8 & 131 & 150 & 162 & 166\\
  \hline
\end{array}$$

\vspace{0.3cm}

Using this table, we will show that Conjecture \ref{Con:count-unip}  holds for any exceptional adjoint group $G$ and any prime number $\ell$. The following section introduces a method for computing $\alpha_\ell$.

\begin{Rk}
 By \cite[2.4]{taylor_unipotent_2013}, for each $F$-stable unipotent class $\mathcal{C}$, there exists $u \in \mathcal{C}$ such that $F$ acts trivially on $A_\G(u)$. Hence, we can replace the set $\bar{\mathcal{M}}_{\ell}(\Omega_u^\ell)$ of Conjecture \ref{Con:count-unip} by the set $\mathcal{M}_{\ell}(\Omega_u^\ell)$ according to Remark \ref{Rk:MGammal} .\end{Rk}


\paragraph{Computing $\alpha_\ell$.}
 Let $(\T, \B)$ be an $F$-stable pair where $\T$ is a maximal torus contained in a Borel subgroup $\B$ of $\B$. Let $\Phi$ be the root system of $\G$ associated to $\T$ and $\Delta= \{\alpha_1,\ldots, \alpha_n \}$ be the set of simple roots induced by $\B$. Let $\alpha_0$ the highest short root. We recall that the groups
$$W_i=\langle s_{\alpha_j} \mid 0 \leq j \neq i \leq n \rangle$$
are isomorphic to Weyl groups of centralisers of isolated semi-simple elements of $\G^*$. Since $\G$ is adjoint, Proposition \ref{Pro:class-rep-lspe} states that a unipotent class is $\ell$-special if and only if its Springer correspondent is $\ell$-special. Using this property, we can sketch a strategy to compute $\alpha_{\ell}$. 
% Moreover, by [ref] for any unipotent classes $\mathcal{O}$ , we can chose a representatives $u$ in $\mathcal{O}^F$ such that $F$ acts trivially on $A_{\G}(u)$, in particular $F$-conjugacy classes are simply conjugacy classes and if $x \in \Omega^\ell_u$, $C_{\Omega^\ell_u,F}(x)=C_{\Omega_{^\ell_u}}(x)$. Using those facts, we can sketch a general strategy to compute $\alpha_\ell$.

\medskip
\noindent {\bf First step.} Finding all $\ell$-special unipotent classes:
  \begin{enumerate}
  \item Determine all subgroups $W_i^*$ of the Weyl group $W^*$ of $\G^*$ for $i \in \{0, \ldots, n\}$.
  \item Using $j$-induction tables, detect which irreducible characters of $W^*$ are $\ell$-special.
  \item For every $\ell$-special character $E$, use the Springer correspondence to get the unipotent class $(u)_\G$ of $\G$ such that $E=E_{u,1}$ where $E$ is viewed as a representation of $W$ via the natural isomorphism $W \simeq W^*$.
  \end{enumerate}
\noindent{\bf Second step.} Now that we have every $\ell$-special class, compute $\alpha_{\ell}$:
  \begin{enumerate}
  \item For each $F$-stable $\ell$-special class $C_u$, compute the
    $\ell$-special quotient $\Omega^{\ell}_u$ of $A_\G(u)$.
  \item For each conjugacy class $(x)$ of $\Omega^\ell_u$, compute $|\Irr_k(C_{\Omega^\ell_u}(x))$| by counting the number of $\ell'$-classes. Let $\alpha_{\ell, u}$ be the sum over conjugacy classes of the numbers obtained this way.
  \item Then $\alpha_\ell$ is equal to $\sum_u \alpha_{\ell, u}$ where $u$ runs over the set of $\ell$-special classes.
  \end{enumerate}

  \begin{Ex}    Assume that $\G=G_2$ and $\ell=2$, we can identify $\G$ with $\G^*$. Then $\G$ has 3 special classes: $1$, $G_2$ and $G_2(a_1)$ and the class  $\tilde{A}_1$ is 2-special. Indeed, $\G$ has a unique class of non-trivial isolated semi-simple $2$-elements $(s)_\G$ and the Weyl group $W_s$ of $C_\G(s)$ is of type $A_1 \times A_1$. Therefore, irreducible characters of $W_s$ are special. If $u \in \tilde{A}_1$, $E_{u,1}=\phi_{2,2}=j_{W_s}^W(\phi_{1^2} \otimes \phi_{1^2})$ is 2-special so is $\tilde{A}_1$.

    For each of those classes, $A_\G(u)$ is trivial excepted for $(u)_\G=G_2(a_1)$ where $A_\G(u)=\mathfrak{S}_3$, let us compute $\Omega^2_u$. Then $\mathfrak{S}_3$ has two indecomposable projective characters: $\Phi_1=\phi_{3}+\phi_{1^3}$ and $\Phi_2=\phi_{12}$. The Springer correspondence for $G_2$ described in  the Table \ref{table:Springer-G2}  gives us $E_{u,\phi_{3}}=\phi_{2,1}$, $E_{u,\phi_{1^3}}=0$ and $E_{u,\phi_{12}}=\phi_{1,3'}$. According to \ref{sec:spec-repr-famil}, $a_{\phi_{12}}=a_{\phi_{1,3'}}=1$, so $a_{\Phi_1}=a_{\Phi_2}=1$ and $\mathcal{S}_2=\{\Phi_1, \Phi_2\}$. Hence, $\Omega^2_u=\mathfrak{S}_3$ and $\alpha_{2,u}=5$, adding 1 for each other 2-special classes we get $\alpha_2=8$.
   \end{Ex}

 
 Computations were made for adjoint exceptional groups using CHEVIE \cite{michel_development_2015}. For each group of exceptional type $\G$ and each bad prime $\ell$, we provide in the appendix tables listing $\ell$-special classes of $\G$ (we use the same labelling as in CHEVIE) and for each special class the groups $A_{\G}(u)$, $\Omega^\ell_u$ and the number $\alpha_{\ell,u}$. Finally, the proof of Theorem \ref{The:count-unip-bad} is complete.

 

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
