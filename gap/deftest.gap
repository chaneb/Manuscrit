Quotient:= function(G, L)
#Prends en argument un groupe et une liste de représentations tels que paramétrées dans CharTable. Retourne le quotient de G par l'intersection des noyaux de ces représentations
    local e, C, T, K, H;
    if L=[] then 
        return G;
    else
        e:=Elements(G);
        C:=ConjugacyClasses(G);
        T:=Transposed(CharTable(G).irreducibles);
        C:=C{Filtered([1..Length(C)], i -> T[i]{L}=T[1]{L})};
        H:=Flat(List(C, i->Elements(i)));
        return G/H;
    fi;    
end;

lQuotient:= function(G, L, l)
#Même chose que quotient, mais prends un argument un nombre premier et quotiente par la l' partie de l'intersection des noyaux
    local e, C, T, K, H;
    if L=[] then 
        return G;
    else
        e:=Elements(G);
        C:=ConjugacyClasses(G);
        T:=Transposed(CharTable(G).irreducibles);
        C:=C{Filtered([1..Length(C)], i -> T[i]{L}=T[1]{L})};
        H:=Flat(List(C, i->Elements(i)));
        H:=Filtered(H,i -> Gcd(Order(G,i),l)=1);
        return G/H;
    fi;    
end;

lprimeConj:=function(G,l)#Donne les classes de conjugaisons d'éléments d'ordre premier à l
    return Filtered(ConjugacyClasses(G), i -> Gcd(Order(G, Representative(i)),l)=1);
end;

lnumber:=function(G,l)#Compte le nombre de l-représentations des centralisateurs d'éléments d'ordre premier à l
    return Sum(ConjugacyClasses(G),i -> Length(lprimeConj(Centralizer(G,Representative(i)),l)));
    end;

Aubar:= function(W,x) 
# W est un groupe de coxeter et x est la position d'une classe unipotente dans UnipotentClasses(W).classes. Retourne le quotient spécial
    local ucl, L, u, locsys;
    ucl:=UnipotentClasses(W); u:=ucl.classes[x];
    locsys:=ucl.springerSeries[1].locsys;
    L:=locsys{Filtered([1..Length(locsys)], i -> locsys[i][1]=x and ChevieCharInfo(W).a[i]=u.dimBu)};
    return Quotient(u.Au, List(L, i->i[2]));
end;

lAubar:= function(W,x,l) #Idem que précédemment mais prends un nombre premier l en argument et retourne le quotient l-spécial.
    local ucl, L, u, locsys;
    ucl:=UnipotentClasses(W); u:=ucl.classes[x];
    locsys:=ucl.springerSeries[1].locsys;
    L:=locsys{Filtered([1..Length(locsys)], i -> locsys[i][1]=x and ChevieCharInfo(W).a[i]=u.dimBu)};
    return lQuotient(u.Au, List(L, i->i[2]),l);
end;

lAubar2:= function(W,x,l) #Idem que précédemment mais prends un nombre premier l en argument et retourne le quotient l-spécial.
    local ucl, L, u, locsys;
    ucl:=UnipotentClasses(W); u:=ucl.classes[x];
    locsys:=ucl.springerSeries[1].locsys;
    p:=PositionProperty(locsys,i-> (i[1]=x and i[2]=Length(ConjugacyClasses(UnipotentClasses(W).classes[x].Au))));
    L:=locsys{Filtered([1..Length(locsys)], i -> locsys[i][1]=x and ChevieCharInfo(W).a[i]=ChevieCharInfo(W).a[p])};
    return lQuotient(u.Au, List(L, i->i[2]),l);
end;


leviquos:=function(V,y,l)
    local C,spec,p,q,jind,M,lspec,levi,i,j,k,locsys,locsysl;
    lspec:=[];
    locsys:=UnipotentClasses(V).springerSeries[1].locsys;
    p:=PositionProperty(locsys,i-> (i[1]=y and i[2]=Length(ConjugacyClasses(UnipotentClasses(W).classes[y].Au))));#Donne la position du système local trivial associé à la classe y
    if p=false then
        return [];
    else    
        levi:=List([1..V.semisimpleRank],i->ReflectionSubgroup(V,V.reflectionsLabels{Concatenation([1..i-1],[i+1..V.semisimpleRank])}));#Liste les sous-groupes de levi
    C:=List(levi,i -> ChevieCharInfo(i));#Caractères des groupes de Weyl des levis
    for k in [1..Length(levi)] do         
        spec:=Filtered([1..Length(C[k].a)],i->C[k].a[i]=C[k].b[i]);#Liste des caractères spéciaux du Levi
        locsysl:=UnipotentClasses(levi[k]).springerSeries[1].locsys;#Liste des systèmes locaux du levi
        jind:=jInductionTable(levi[k],V);
        M:=Transposed(Transposed(jind.scalar){spec});
        if Sum(M[p])>0 then #Si la ligne associé à la représentation est non nulle sur les colonnes spéciales
            q:=PositionsProperty([1..Length(jind.scalar[p])], j -> jind.scalar[p][j]>0 and j in spec);
           for j in q do
               if locsysl[j][2]=Length(ConjugacyClasses(UnipotentClasses(levi[k]).classes[locsysl[j][1]].Au)) then
                   Add(lspec, [levi[k].semisimpleRank,lAubar(levi[k], locsysl[j][1],l)]); 
                   if levi[k].semisimpleRank>1 then lspec:=Concatenation(lspec,leviquos(levi[k],locsysl[j][1],l)); 
                   fi;              
               fi;
           od;
        fi;  
    od; 
    return lspec;  
    fi;
end;

lquos:=function(W, x, l)
    local C,spec,p,q,jind,M,lspec,u,sgmax,i,j,k,n,locsys,locsysv,R;
    lspec:=[];
    n:=PositionProperty([1..Length(W.roots)],i ->Sum(W.roots[W.N])=Sum(W.coroots[i]));#Donne la position de la coracine la plus longue 
    locsys:=UnipotentClasses(W).springerSeries[1].locsys;
    p:=PositionProperty(locsys,i-> (i[1]=x and i[2]=Length(ConjugacyClasses(UnipotentClasses(W).classes[x].Au))));#Donne la position du système local trivial associé à la classe x
    if p=false then
        return [];
    else    
        sgmax:=List([1..W.rank],i->[ReflectionSubgroup(W,Concatenation([[1..i-1],[i+1..W.rank],[HighestShortRoot(W)]])),W.coroots[n][i]]);#Liste les sous-groupes maximaux du dual, et donne l'ordre de l'élément isolé associé        
        Add(sgmax,[W,1]);  
        R:=List([1..W.rank],i -> Concatenation([[1..i-1],[i+1..W.rank],[HighestShortRoot(W)]]));
        Add(R, [1..W.rank]);
        C:=List(sgmax,i -> ChevieCharInfo(i[1]));
        for k in [1..Length(sgmax)] do 
            spec:=Filtered([1..Length(C[k].a)],i->C[k].a[i]=C[k].b[i]);
            locsysv:=UnipotentClasses(sgmax[k][1]).springerSeries[1].locsys; 
            jind:=jInductionTable(sgmax[k][1],W);
            M:=Transposed(Transposed(jind.scalar){spec});
            if Sum(M[p])>0 and ((sgmax[k][2] mod l = 0 and IsPrimePower(sgmax[k][2])) or sgmax[k][2]=1) then
                q:=PositionsProperty([1..Length(jind.scalar[p])], j -> jind.scalar[p][j]>0 and j in spec);
                for j in q do
                    if locsysv[j][2]=Length(ConjugacyClasses(UnipotentClasses(sgmax[k][1]).classes[locsysv[j][1]].Au)) then
                        Add(lspec, [sgmax[k][1].semisimpleRank,lAubar(sgmax[k][1], locsysv[j][1],l)]);
                        
                           if sgmax[k][1].semisimpleRank >1 then lspec:=Concatenation(lspec,leviquos(sgmax[k][1],locsysv[j][1],l)); 
                           fi;
                    fi;  
                od;
            fi;
        od;
    return lspec;
    fi;
end;



    
