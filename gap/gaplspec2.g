W:=CoxeterGroup("F",4);
ucl:=UnipotentClasses(W);
#Display(ucl);
l:=ucl.springerSeries[1].locsys;
m:=List(ucl.springerSeries[1].locsys,i->Length(ChevieClassInfo(ucl.classes[i[1]].Au).classtext));
Filtered([1..Length(l)],i->l[i][2]=m[i]);
sgmax:=List([1..W.rank],i->[ReflectionSubgroup(W,Concatenation([[1..i-1],[i+1..W.rank],[HighestShortRoot(W)]])),W.roots[W.N][i]]);
Add(sgmax,[W,1]);

classeslspec:=function(V)
  local C,spec,jind,M,lspec;
  C:=ChevieCharInfo(V);
  spec:=Filtered([1..Length(C.a)],i->C.a[i]=C.b[i]);
  jind:=jInductionTable(V,W);
  M:=Transposed(Transposed(jind.scalar){spec});
  lspec:=Filtered([1..Length(M)],i->Sum(M[i])>0);
  return List(l{lspec},i->i[1]);
end;

classeslspecnotspec:=function(W,V)
  local C,spec,jind,M,lspec,N;
  N:=classeslspec(W);
  C:=ChevieCharInfo(V);
  spec:=Filtered([1..Length(C.a)],i->C.a[i]=C.b[i]);
  jind:=jInductionTable(V,W);
  M:=Transposed(Transposed(jind.scalar){spec});
  lspec:=Filtered([1..Length(M)],i->Sum(M[i])>0);
  return Filtered(List(l{lspec},i->i[1]),j->Position(N,j)=false);
end; 

List(sgmax,i->[i[2],classeslspecnotspec(W,i[1])]);

#Filtered([1..Length(l)],i->C.a[i]=ucl[l[i][1]].dimBu);

 # ordre, numéro de classes unip non spéciales mais spéciales dans le #centralisateur

#Pour chaque Au regarde les classes de conjugaison et regarder les classes de conj pour l
#Regarder les représentations des centralisateurs des classes de conjugaison de Au

#Nombre de caractères unipotents
#-différence entre caractères ordinaires et caractères de Brauer des centralisateurs des classes de conjugaisons des Au
#+pour chaque classe lspéciale, chaque classe de Au, nombre de Brauer irréductible du centralisateur
#+La différence entre Au et Aubar pour certaines classes non spéciales
