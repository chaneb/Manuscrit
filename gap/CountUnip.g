lspec:=function(W, l) #Donne la liste des classes unipotentes l-speciales
    local C,spec,jind,M,lspec,u,sgmax,k,n;
    lspec:=[];
    n:=PositionProperty([1..Length(W.roots)],i ->Sum(W.roots[W.N])=Sum(W.coroots[i]));#Donne la position de la coracine la plus longue 
    sgmax:=List([1..W.rank],i->[ReflectionSubgroup(W,Concatenation([[1..i-1],[i+1..W.rank],[HighestShortRoot(W)]])),W.coroots[n][i]]);#Liste les sous-groupes maximaux du dual, et donne l'ordre de l'élément isolé associé
    Add(sgmax,[W,1]);    
    C:=List(sgmax,i -> ChevieCharInfo(i[1]));
    for k in [1..Length(sgmax)] do         
        spec:=Filtered([1..Length(C[k].a)],i->C[k].a[i]=C[k].b[i]);
        jind:=jInductionTable(sgmax[k][1],W);
        M:=Transposed(Transposed(jind.scalar){spec});
        u:=Filtered([1..Length(M)],i->Sum(M[i])>0);        
        Add(lspec, [sgmax[k][2],List(UnipotentClasses(W).springerSeries[1].locsys{u},i -> i[1])]);
    od;
    lspec:=Filtered(lspec,j -> ((j[1] mod l =0) and IsPrimePower(j[1])) or j[1]=1);
    return Set(Flat(List(lspec, j->j[2])));
end;

lprimeConj:=function(G,l)#Donne les classes de conjugaisons d'éléments d'ordre premier à l
    return Filtered(ConjugacyClasses(G), i -> Gcd(Order(G, Representative(i)),l)=1);
end;

lnumber:=function(G,l)#Compte le nombre de l-représentations des centralisateurs d'éléments d'ordre premier à l
    return Sum(ConjugacyClasses(G),i -> Length(lprimeConj(Centralizer(G,Representative(i)),l)));
    end;
        
