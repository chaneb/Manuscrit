lspec:=function(W, l) #Give the list of l-speciale unipotent classes
    local C,spec,jind,M,lspec,u,sgmax,k,n;
    lspec:=[];
    n:=PositionProperty([1..Length(W.roots)],i ->Sum(W.roots[W.N])=Sum(W.coroots[i]));#position of the longest coroot
    sgmax:=List([1..W.rank],i->[ReflectionSubgroup(W,Concatenation([[1..i-1],[i+1..W.rank],[HighestShortRoot(W)]])),W.coroots[n][i]]);#List maximal subgroups of the dual and give the order of the associated isolated element
    Add(sgmax,[W,1]);    
    C:=List(sgmax,i -> ChevieCharInfo(i[1]));
    for k in [1..Length(sgmax)] do         
        spec:=Filtered([1..Length(C[k].a)],i->C[k].a[i]=C[k].b[i]);
        jind:=jInductionTable(sgmax[k][1],W);
        M:=Transposed(Transposed(jind.scalar){spec});
        u:=Filtered([1..Length(M)],i->Sum(M[i])>0);        
        Add(lspec, [sgmax[k][2],List(UnipotentClasses(W).springerSeries[1].locsys{u},i -> i[1])]);
    od;
    lspec:=Filtered(lspec,j -> ((j[1] mod l =0) and IsPrimePower(j[1])) or j[1]=1);
    return Set(Flat(List(lspec, j->j[2])));
end;

lprimeConj:=function(G,l)#Give the list of conjugacy classes of l'-elements.
    return Filtered(ConjugacyClasses(G), i -> Gcd(Order(G, Representative(i)),l)=1);
end;

lnumber:=function(G,l)#Count the number of l-modular representations of centralisers of l'-elements.
    return Sum(ConjugacyClasses(G),i -> Length(lprimeConj(Centralizer(G,Representative(i)),l)));
    end;
        
Quotient:= function(G, L)#G is a group and L a list of representations parametrised as in CharTable. Retour the quotient of G by the intersection of kernels of representations in L.
    local e, C, T, K, H;
    if L=[] then 
        return G;
    else
        e:=Elements(G);
        C:=ConjugacyClasses(G);
        T:=Transposed(CharTable(G).irreducibles);
        C:=C{Filtered([1..Length(C)], i -> T[i]{L}=T[1]{L})};
        H:=Flat(List(C, i->Elements(i)));
        return G/H;
    fi;    
end;

lQuotient:= function(G, L, l)#Same as Quotient but take only the l'-part of the intersection of kernels.
    local e, C, T, K, H;
    if L=[] then 
        return G;
    else
        e:=Elements(G);
        C:=ConjugacyClasses(G);
        T:=Transposed(CharTable(G).irreducibles);
        C:=C{Filtered([1..Length(C)], i -> T[i]{L}=T[1]{L})};
        H:=Flat(List(C, i->Elements(i)));
        H:=Filtered(H,i -> Gcd(Order(G,i),l)=1);
        return G/H;
    fi;    
end;

lAubar:=function(W,x,l)#Return the group Gamma_x^l
    local ucl,u,locsys,C, Au,Pos, i,j,a, D,Fil, Max,E,L;    
    ucl:=UnipotentClasses(W);
    u:=ucl.classes[x];
    locsys:=ucl.springerSeries[1].locsys;
    C:=ChevieCharInfo(W);
    Au:=u.Au;
    Pos:=List([1..Length(ConjugacyClasses(Au))], i -> PositionProperty(locsys,j -> j[1]=x and j[2]=i));#Give the position of the Springer correspondent E_{x,i}
    D:=Transposed(DecompositionMatrix(Au,l));
    a:=List([1..Length(D)], i -> -1);
    for i in [1..Length(D)] do
        Fil:=Filtered([1 .. Length(Pos)], j -> Pos[j] <> false and D[i][j]<>0);
        if Fil <> [] then
            a[i]:=Minimum(List(Pos{Fil}, j->C.a[j]));
        fi;
    od;
    Max:=Maximum([0,Maximum(a)]);
    E:=Transposed(D{Filtered([1..Length(D)], i -> a[i]=Max)});    
    L:=List(E, i->Maximum(i));
    return Quotient(Au,PositionsProperty(L,i -> i<>0));
    
end;


countublock:= function(W,l) # Return the number unipotent l modular characters of the simple adjoint exceptionnal group of type W.
    local i,ls;
    ls:=lspec(W,l);
    return Sum(ls, i -> lnumber(lAubar(W,i,l),l));
end;

             
            
