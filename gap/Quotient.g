Quotient:= function(G, L)
#Prends en argument un groupe et une liste de représentations tels que paramétrées dans CharTable. Retourne le quotient de G par l'intersection des noyaux de ces représentations
    local e, C, T, K, H;
    if L=[] then 
        return G;
    else
        e:=Elements(G);
        C:=ConjugacyClasses(G);
        T:=Transposed(CharTable(G).irreducibles);
        C:=C{Filtered([1..Length(C)], i -> T[i]{L}=T[1]{L})};
        H:=Flat(List(C, i->Elements(i)));
        return G/H;
    fi;    
end;

lQuotient:= function(G, L, l)
#Même chose que quotient, mais prends un argument un nombre premier et quotiente par la l' partie de l'intersection des noyaux
    local e, C, T, K, H;
    if L=[] then 
        return G;
    else
        e:=Elements(G);
        C:=ConjugacyClasses(G);
        T:=Transposed(CharTable(G).irreducibles);
        C:=C{Filtered([1..Length(C)], i -> T[i]{L}=T[1]{L})};
        H:=Flat(List(C, i->Elements(i)));
        H:=Filtered(H,i -> Gcd(Order(G,i),l)=1);
        return G/H;
    fi;    
end;

Aubar:= function(W,x) 
# W est un groupe de coxeter et x est la position d'une classe unipotente dans UnipotentClasses(W).classes. Retourne le quotient spécial
    local ucl, L, u, locsys;
    ucl:=UnipotentClasses(W); u:=ucl.classes[x];
    locsys:=ucl.springerSeries[1].locsys;
    L:=locsys{Filtered([1..Length(locsys)], i -> locsys[i][1]=x and ChevieCharInfo(W).a[i]=u.dimBu)};
    return Quotient(u.Au, List(L, i->i[2]));
end;

lAubar:= function(W,x,l) #Idem que précédemment mais prends un nombre premier l en argument et retourne le quotient l-spécial.
    local ucl, L, u, locsys;
    ucl:=UnipotentClasses(W); u:=ucl.classes[x];
    locsys:=ucl.springerSeries[1].locsys;
    L:=locsys{Filtered([1..Length(locsys)], i -> locsys[i][1]=x and ChevieCharInfo(W).a[i]=u.dimBu)};
    return lQuotient(u.Au, List(L, i->i[2]),l);
end;

    
            
