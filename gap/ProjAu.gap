lAubar:=function(W,x,l)
    local ucl,u,locsys,C, Au,Pos, i,j,a, D,Fil, Max,E,L;    
    ucl:=UnipotentClasses(W);
    u:=ucl.classes[x];
    locsys:=ucl.springerSeries[1].locsys;
    C:=ChevieCharInfo(W);
    Au:=u.Au;
    Pos:=List([1..Length(ConjugacyClasses(Au))], i -> PositionProperty(locsys,j -> j[1]=x and j[2]=i));#Pos[i] donne la position dans locsys du  système local (x,i) (retourne faux si (x,i) n'est pas dans locsys).
    D:=Transposed(DecompositionMatrix(Au,l));
    a:=List([1..Length(D), i -> 0]);
    for i in [1..Length(D)] do
        Fil:=Filtered([1 .. Length(Pos)], j -> Pos[j] <> false and D[i][j]<>0);
        Add(a,Minimum(List(Pos{Fil}, j-> C.a[j])));
    od;
    Max:=Maximum(a);
    E:=Transposed(D{Filtered([1..Length(D)], i -> a[i]=Max)});    
    L:=List(E, i->Maximum(i));
    return PositionsProperty(L,i -> i<>0);
    
end;

        
