 W:=CoxeterGroup("F",4);
ucl:=UnipotentClasses(W);
Display(ucl);
l:=ucl.springerSeries[1].locsys;
m:=List(ucl.springerSeries[1].locsys,i->Length(ChevieClassInfo(ucl.classes[i[1]].Au).classtext));
Filtered([1..Length(l)],i->l[i][2]=m[i]);
sgmax:=List([1..W.rank],i->[ReflectionSubgroup(W,Concatenation([[1..i-1],[i+1..W.rank],[HighestShortRoot(W)]])),W.roots[W.N][i]]);
Add(sgmax,[W,1]);

classeslspec:=function(V)
  local C,spec,jind,M,lspec;
  C:=ChevieCharInfo(V);
  spec:=Filtered([1..Length(C.a)],i->C.a[i]=C.b[i]);
  jind:=jInductionTable(V,W);
  M:=Transposed(Transposed(jind.scalar){spec});
  lspec:=Filtered([1..Length(M)],i->Sum(M[i])>0);
  return List(l{lspec},i->i[1]);
end; 
