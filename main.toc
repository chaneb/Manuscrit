\contentsline {chapter}{Introduction}{5}{chapter*.2}
\contentsline {paragraph}{Case $G=\mathrm {Sp}_4(q)$ and $\ell =2$.}{10}{paragraph*.3}
\contentsline {paragraph}{Case $G=G_2(q)$ and $\ell =2,3$.}{10}{paragraph*.4}
\contentsline {chapter}{\numberline {1}Finite groups of Lie type and their representations}{14}{chapter.1}
\contentsline {section}{\numberline {1.1} Representations of finite groups}{15}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Modular representations of finite groups}{15}{subsection.1.1.1}
\contentsline {paragraph}{Decomposition map.}{15}{paragraph*.5}
\contentsline {paragraph}{Blocks.}{17}{paragraph*.6}
\contentsline {subsection}{\numberline {1.1.2}Clifford theory}{18}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Finite groups of Lie type}{19}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Reductive algebraic groups}{19}{subsection.1.2.1}
\contentsline {paragraph}{Definitions and properties.}{20}{paragraph*.7}
\contentsline {paragraph}{Root subgroups and root data}{20}{paragraph*.8}
\contentsline {paragraph}{Fundamental group.}{22}{paragraph*.9}
\contentsline {paragraph}{Parabolic and Levi subgroups. }{23}{paragraph*.10}
\contentsline {subsection}{\numberline {1.2.2}Finite groups of Lie type}{23}{subsection.1.2.2}
\contentsline {paragraph}{Frobenius endomorphism.}{23}{paragraph*.11}
\contentsline {paragraph}{Lang-Steinberg theorem and applications.}{24}{paragraph*.12}
\contentsline {paragraph}{Classification of finite groups of Lie type.}{25}{paragraph*.13}
\contentsline {subsection}{\numberline {1.2.3}Isolated classes}{25}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Deligne--Lusztig theory}{27}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}$\ell $-adic cohomology}{27}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Deligne--Lusztig induction}{28}{subsection.1.3.2}
\contentsline {paragraph}{Bruhat decomposition.}{28}{paragraph*.14}
\contentsline {paragraph}{Deligne--Lusztig varieties.}{29}{paragraph*.15}
\contentsline {paragraph}{Deligne--Lusztig induction.}{29}{paragraph*.16}
\contentsline {paragraph}{Properties of Deligne--Lusztig characters.}{30}{paragraph*.17}
\contentsline {subsection}{\numberline {1.3.3}Rational series}{31}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}Jordan decomposition}{33}{subsection.1.3.4}
\contentsline {paragraph}{Connected center case.}{33}{paragraph*.18}
\contentsline {paragraph}{Disconnected center case.}{33}{paragraph*.19}
\contentsline {subsection}{\numberline {1.3.5}Blocks and rational series}{35}{subsection.1.3.5}
\contentsline {chapter}{\numberline {2}Counting unipotent modular representations}{37}{chapter.2}
\contentsline {section}{\numberline {2.1}Classification of unipotent characters}{39}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}On representations of $W$}{39}{subsection.2.1.1}
\contentsline {paragraph}{Fake degrees and generic degrees.}{39}{paragraph*.20}
\contentsline {paragraph}{Special representations and families.}{39}{paragraph*.21}
\contentsline {subsection}{\numberline {2.1.2}Lusztig's theorem}{41}{subsection.2.1.2}
\contentsline {paragraph}{Families of unipotent characters.}{41}{paragraph*.22}
\contentsline {paragraph}{Finite groups associated to families.}{42}{paragraph*.23}
\contentsline {paragraph}{Lusztig's theorem.}{43}{paragraph*.24}
\contentsline {paragraph}{Non unipotent characters.}{44}{paragraph*.25}
\contentsline {section}{\numberline {2.2}Springer correspondence}{45}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Springer's construction of representations of $W$}{45}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Canonical quotient}{47}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Parametrisation of $\mathrm {Irr}_\mathbb {C}(G)$ by special classes}{48}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}$\ell $-special classes and $\ell $-canonical quotient}{49}{section.2.3}
\contentsline {paragraph}{Truncated induction.}{50}{paragraph*.26}
\contentsline {paragraph}{$\ell $-special classes.}{50}{paragraph*.27}
\contentsline {paragraph}{Image of the Springer correspondence.}{51}{paragraph*.28}
\contentsline {paragraph}{$\ell $-canonical quotient.}{51}{paragraph*.29}
\contentsline {section}{\numberline {2.4}Type $A_n$}{53}{section.2.4}
\contentsline {paragraph}{Restriction to $G$.}{54}{paragraph*.30}
\contentsline {paragraph}{$\ell $-canonical quotient for $G$.}{54}{paragraph*.31}
\contentsline {section}{\numberline {2.5}Classical types}{55}{section.2.5}
\contentsline {paragraph}{A remark on the groups $\Omega _u^2$.}{55}{paragraph*.32}
\contentsline {paragraph}{Preliminary results.}{56}{paragraph*.33}
\contentsline {paragraph}{Proof of Theorem \ref {The:Countclasses}.}{58}{paragraph*.34}
\contentsline {section}{\numberline {2.6}Exceptional groups}{58}{section.2.6}
\contentsline {paragraph}{Computing $\alpha _\ell $.}{59}{paragraph*.35}
\contentsline {chapter}{\numberline {3}Basic sets for unipotent blocks of classical groups}{61}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction to the main theorem}{63}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Statement of the theorem }{63}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Strategy of proof}{63}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Generalised Gelfand--Graev representations}{65}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Weighted Dynkin diagrams}{65}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Generalised Gelfand--Graev representations}{66}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Unipotent support and wave front set}{68}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}GGGRs and regular embedding}{69}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Character Sheaves and GGGRs}{70}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Character sheaves and local systems}{70}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}The unitriangularity condition}{71}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Unitriangularity for groups with disconnected center}{73}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Main result}{75}{section.3.4}
\contentsline {chapter}{\numberline {4}Decomposition matrices and basic sets for groups of small rank}{76}{chapter.4}
\contentsline {section}{\numberline {4.1}Cohomology complexes}{77}{section.4.1}
\contentsline {paragraph}{Decomposition map and Deligne--Lusztig Induction.}{77}{paragraph*.36}
\contentsline {paragraph}{Complex of projective modules.}{78}{paragraph*.37}
\contentsline {paragraph}{Application to computation of decomposition numbers.}{79}{paragraph*.38}
\contentsline {section}{\numberline {4.2}Basic set and decomposition matrix of $\mathrm {Sp}_4$}{80}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Description of the basic set of Sp$_{2n}$}{80}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Semi-simple elements of order 2 of $\unhbox \voidb@x \hbox {Sp}_4$}{82}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Unitriangular basic set and decomposition numbers for unipotent blocks of $\unhbox \voidb@x \hbox {Sp}_4$}{83}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Basic sets and decomposition matrices for $G_2$}{85}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Unitriangular basic set for $G_2$}{85}{subsection.4.3.1}
\contentsline {subsubsection}{Isolated elements of $G_2$}{85}{subsubsection*.39}
\contentsline {subsubsection}{Notation for irreducible characters of $G_2$}{85}{subsubsection*.40}
\contentsline {subsubsection}{Basic set for unipotent blocks}{87}{subsubsection*.41}
\contentsline {paragraph}{Basic set for $\ell =2$.}{87}{paragraph*.42}
\contentsline {paragraph}{Basic set for $\ell =3$.}{88}{paragraph*.43}
\contentsline {subsection}{\numberline {4.3.2}Decomposition numbers}{89}{subsection.4.3.2}
\contentsline {paragraph}{Case $\ell =2$.}{90}{paragraph*.44}
\contentsline {paragraph}{Case $\ell =3, \tmspace +\thinmuskip {.1667em} q \equiv 1 \mathrm { \tmspace +\thinmuskip {.1667em} mod \tmspace +\thinmuskip {.1667em}}3$.}{90}{paragraph*.45}
\contentsline {paragraph}{Case $\ell =3, \tmspace +\thinmuskip {.1667em} q \equiv -1 \mathrm { \tmspace +\thinmuskip {.1667em} mod \tmspace +\thinmuskip {.1667em}}3$.}{91}{paragraph*.46}
\contentsline {chapter}{Appendix}{92}{chapter*.47}
