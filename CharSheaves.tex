\documentclass[a4paper, twoside, draft, 12pt]{report}

% Options possibles : 10pt, 11pt, 12pt (taille de la fonte)
%                     oneside, twoside (recto simple, recto-verso)
%                     draft, final (stade de développement)
\usepackage[a4paper]{geometry}% Réduire les marges
\geometry{margin=1in}

\usepackage[utf8]{inputenc}   
\usepackage[T1]{fontenc}      % Police contenant les caractères français
\usepackage[all]{xy}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{varioref}
\usepackage{amsthm}


\usepackage{enumitem}
\usepackage{array}
\usepackage{ stmaryrd }
\usepackage{xcolor}
\usepackage{tikz}
\usepackage[colorlinks=true]{hyperref}
\usepackage{longtable}
\usepackage{tabularx}
\usepackage[all]{xy}
\usepackage{tikz-cd}
\allowdisplaybreaks

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}



% \pagestyle{headings}        % Pour mettre des entêtes avec les titres
                              % des sections en haut de page

\newtheorem{The}{Theorem}[section]
\newtheorem{Con}{Conjecture}[section]
\newtheorem{Pro}[The]{Proposition}
\newtheorem{Cor}[The]{Corollary}
\newtheorem{Lem}[The]{Lemma}
\theoremstyle{definition}
\newtheorem{Def}[The]{Definition}
\newtheorem{Rk}[The]{Remark}
\newtheorem{Ex}[The]{Example}

\newcommand{\F}{\mathbb{F}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\M}{\mathbf{L}}
\newcommand{\G}{\mathbf{G}}
\newcommand{\T}{\mathbf{T}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\B}{\mathbf{B}}
\newcommand{\Irr}{\mathrm{Irr}}
\newcommand{\psc}[2]{\left<#1\middle|#2\right>}

\renewcommand{\qedsymbol}{$\blacksquare$}
\renewenvironment{proof}{{\bfseries Proof.}}{\qed}

\newcommand{\overbar}[1]{\mkern 1.5mu\overline{\mkern-1.5mu#1\mkern-1.5mu}\mkern 1.5mu}

\title{Unipotent support and basic set}          
\author{Reda Chaneb}
\date{}                      
\sloppy                       % Ne pas faire déborder les lignes dans la marge



\begin{document}

\subsection{Character sheaves and Local systems}


We assume that the center of $\G$ is connected. Let $\hat{\G}$ be the set of character sheaves on $\G$. Characters sheaves, introduced by Lusztig in [ref], are certains irreductible perverse sheaves which are equivarient for the action of $\G$ by conjugation.  The structure of $\hat{G}$ is in a certain sense very similar to $\Irr(G)$: Lusztig in [ref] gave a partition
$$\hat{\G}=\bigsqcup \hat{\G}_s$$

where $s$ runs over a complete set of representatives of $\G^*$-conjugacy class of semisimple element of $G^*$. Moreover, for for each class $(s)_{G^*}$, we have a partition

$$\hat{\G}_s=\bigsqcup \hat{\G}_{s,u}$$

where $u$ runs over representatives of special unipotent classes of $C_{\G^*}(s)$.

We say that $A \in \hat{\G}$, is $F$-stable if it isomorphic to its inverse image $F^*A$ under the Frobenius map $F$, we denote by $\hat{G}$ the set of $F$-stable character sheaf. If $A \in \hat{G}$, any isomorphism $\phi:F^*A\to A$ induces a class function $\chi_A \in \mbox{CF}(G)$, by [ref] we can chose $\phi$ such that $(\chi_A, \chi_A)_G=1$. The set obtained $\{\chi_A, A \in \hat{G} \}$ is an orthonormal basis of $\mbox{CF}(G)$, in particular there is as many $F$-stable character sheaves as irreducible characters of $G$. The structure of $\hat{\G}$ can be transferred to $\hat{G}$, we have a partition:

$$\hat{G}=\bigsqcup \hat{G}_{s,u}$$ 

where $s$ runs over a set of representatives of $F$-stable $\G^*$-conjugacy class of semisimple elements of $\G^*$ and $u$ runs over representatives $F$-stable special unipotent classes $C_{\G^*}(s)$. Moreover, according to [ref], if $A \in \hat{G}_{s,u}$, $\chi_A$ is a linear combination of irreducible characters of $\mathcal{E}_{s,u}$.

Let $\mathcal{O}$ be a $F$-stable unipotent class of $\G$, let $I_{\mathcal{O}}$ the set of $\G$-equivarient irreducible local system. We denote by $I_{\mathcal{O}}^F$ the local system $\mathcal{E} \in I_{\mathcal{O}}$ such that $\mathcal{E} \simeq F^*\mathcal{E}$. Let $\mathcal{E} \in I_{\mathcal{O}}^F$, and let us fix an isomorphism $\psi: F^*:\mathcal{E}\to\mathcal{E}$. Let us define the following function $Y_{\mathcal{E}}$ on $G$:

\begin{equation*}
    Y_\mathcal{E}(g) = \begin{cases}
               \mbox{Trace}(\psi,\mathcal{E}_g)        & \mbox{if } g \in \mathcal{O}^F\\
               0   & \text{otherwise}
           \end{cases}
\end{equation*}

In [ref], Lusztig proved the following statement about the functions $Y_\mathcal{E}$:

\begin{Pro}\label{sec:character-sheaves-1}
  The functions $Y_\mathcal{E}$, $\mathcal{E} \in I_\mathcal{O}^F$ form a basis for the vector space of $G$-invariant functions on $\mathcal{O}^F$.
\end{Pro}

\subsection{A theorem on GGGRs}

Recall that we introduced in [ref] a surjective map $\Phi$ sending a special conjugacy class of $\G^*$ to unipotent class of $\G$.

\begin{The}\label{sec:character-sheaves}
  Let $\mathcal{O}$ be a $F$-stable unipotent class of $\G$. Then there exists $g=su \in G^*$ special such that
  \begin{itemize}
  \item $\Phi((g)_{\G^*})=\mathcal{O}$
  \item For any $A \in \hat{\G}_{s,u}$, $A|_{\mathcal{O}}$ is either
    zero or an irreducible $\G$ equivariant local system up to
    shift. 
  \item The map $A \mapsto A|_\mathcal{O}$ is a bijection from
    $A \in \hat{\G}_{s,u}$ with non zero restriction onto
    $I_\mathcal{O}$
\end{itemize}

\end{The}

\vspace{0.965cm}

\begin{Rk}\label{sec:character-sheaves-2}
  \leavevmode
  \begin{enumerate}
  \item Let us keep the notations of the Theorem. We can translate
    those results as follows: for any $A \in \hat{G}_{s,u}$ we have   either $\chi_A=0$ or we can chose the isomorphism    $\phi:F^*A \to A$ such that $\chi_A(g)=Y_{\mathcal{E}}(g)$ for all    $g \in \mathcal{O}^F$ where $\mathcal{E}=A|\mathcal{O}$.   \item For any $\mathcal{E} \in I_\mathcal{O}^F$, the unique character sheaf $A \in \hat{\G}_{s,u}$ such that $A|_\mathcal{O}=\mathcal{E}$ is $F$-stable. Indeed, since $(s)_{\G^*}$ and $(u)_{C_{\G^*}(s)}$ are $F$-stable, $F^*A \in \hat{\G}_{s,u}$ by [ref]. Moreover, $F^*A|_\mathcal{O}=F^*\mathcal{E} \simeq \mathcal{E} = A|_{\mathcal{O}}$. Hence $F^*A \simeq A$ according to the theorem.
    \item Geck stated that the results holds for any pair special element $g=su \in G^*$ such that $|\Gamma_{(u)_{C_{\G^*}(s)}}|=|A_\G(u)|$. Hezard has shown in [ref] that such an element $g$ exists. 
    \end{enumerate}

    We can now state the following result, although the statement only involves irreducible characters of $G$, the proof relies on the results on character sheaves introduce above.

    \begin{Pro}
      Let $\mathcal{O}$ be an $F$-stable unipotent class and $u_1, \dots ,u_d$ be representatives of the $G$-conjugacy classes of $\mathcal{O}^F$. There exists $g=su \in G^*$ special such that
      \begin{itemize}
      \item $\mathcal{O}=\Phi((g)_{\G^*})$
      \item There exists $\rho_1, \dots, \rho_d \in \Irr_{s,u}(G)$ such that the matrix $(\rho_i(u_j))_{1\leq i,j \leq d}$ is invertible.
    \end{itemize}

    \end{Pro}

\end{Rk}

\begin{proof}
By \ref{sec:character-sheaves-1}, $I_{\mathcal{O}}^F$ has $d$ elements $\mathcal{E}_1, \dots, \mathcal{E}_d$. Let $g=su \in G^*$ the special element of Theorem \ref{sec:character-sheaves}. Then there exists $A_1, \dots, A_d \in \hat{\G}_{s,u}$ such that $A_i|_{\mathcal{O}}=\mathcal{E}_i$ and $A_1, \dots, A_d \in \hat{G}_{s,u}$ by remark \ref{sec:character-sheaves-2} 2). By  the first part of remark \ref{sec:character-sheaves-2}, we can assume that $\chi_{A_i}=\mathcal{E}_i$, hence by the Theorem \ref{sec:character-sheaves-1}, $(\chi_{A_i}(u_j))_{1 \leq i,j \leq d}$ is invertible. Shoji prove in [ref] that  each $\chi_{A_i}$ is a linear combination of the characters in $\Irr_{s,u}(G)$ so there exists $\rho_1, \dots, \rho_d$ such that the matrix $(\rho_i(u_j))_{1 \leq i, j \leq d}$ is invertible.
\end{proof}

We can state the main results of this part:

\begin{The}\label{sec:theorem-gggrs}
  Assume that the center of $\G$ is connected. Let $\mathcal{O}$ be an $F$-stable unipotent class of $\G$ and $u_1, \dots, u_d$ be representatives of $G$-conjugacy classes of $\mathcal{O}^F$ and assume that $A_{\G}(u_1)$ is abelian. Then there exists $g=su \in G^*$ special such that $\mathcal{O}=\Phi((g)_{\G^*})$ and $\chi_1, \dots, \chi_d \in \mathcal{E}(G,s)_u$ such that $(\chi_i^*, \gamma_{u_j})=\delta_{i,j}$ for $1 \leq i,j \leq d$.
\end{The}

\begin{proof}
  Let $g=su$ be as in [ref]. According to [ref], for any $\rho \in \mathcal{E}(G,s)_u$

  $$\sum \limits_{i=1}^d [A_\G(u_i):A_\G(u_i)^F](\rho^*,\gamma_{u_i})=n_\rho^{-1}|A_\G(u_1)|$$

  where $n_\rho$ is an integer such that $\rho(1)n_\rho$ is a polynomial in $q$ with integer coefficients (see [ref] for the exact definition). According to [ref], $n_\rho=|\bar{A}_{C_{\G^*}(s)}(u)|$ and $g$ has been chose such that $|\bar{A}_{C_{\G^*}(s)}(u)|=|A_\G(u_1)|$ hence we have

  $$\sum \limits_{i=1}^d [A_\G(u_i):A_\G(u_i)^F](\rho^*,\gamma_{u_i})=1$$

  Seach each term of the sum is positive, there is a unique $i \in \{1, \dots, d \}$ such that $(\rho^*, \gamma_{u_i})=1$ and $(\rho^*, \gamma_{u_j})=0$ for $j \neq i$. For $i \in \{1, \dots ,d \}$, let $I_i$ the subset of $\mathcal{E}(G,s)_u$ consisting of characters $\rho$ such that $(\rho, \gamma_{u,i})=1$ and $(\rho, \gamma_{u_j})$ for $j \neq i$. Assume that $I_r= \emptyset$ for some $r \in \{1, \dots, d \}$,ie for all $\rho \in \mathcal{E}(G,s)_u$, $(\rho^*,\gamma_{u_r})=0$ or $(\rho, D_G(\gamma_{u_r}))=0$. Writing the definition of scalar product for the last equality we have

  $$|G|^{-1}|\sum \limits_{g \in G} \overbar{\rho(g)}D_G(\gamma_{u_r})(g)$$

  Moreover, $D_G(\gamma_{u_r})$ is $0$ on non unipotent element and by [ref] $D_G(\gamma_{u_r})$ is non zero only on unipotent class $\mathcal{O'}$ such that $\mathcal{O}$ is contained in the closure of $\mathcal{O'}$.  On the other hand, by [ref] $\rho$ is zero on $\mathcal{O'}$ unless $\mathcal{O}=\mathcal{O'}$ or dim $\mathcal{O'} <$ dim $\mathcal{O}$. Hence we only need to sum over element of $\mathcal{O}^F$ and we get

  $$ \sum \limits_{j=1}^d|C_G(u_j)|^{-1} \overbar{\rho(u_j)}D_G(\gamma_{u_r})(u_j)$$

  Applying this sum to characters $\rho_1, \dots, \rho_d$ of [ref], we get a linear relation on lines of the invertible matrix $(\rho_i(u_j))_{1\leq i,j \leq d}$ so $D_G(\gamma_{u_r}(u_j)=0$ for $j \in \{ 1, \dots, d \}$. According to [ref], for any $\mathcal{E} \in I_\mathcal{O}^F,$ $(D_G(\gamma_{u_r},Y_{\mathcal{E}})$ equals $ \zeta q^{b} \overbar{Y_{\mathcal{E}}(u_r)}$ where $\zeta$ is a 4th root of unity and $b$ is an integer depending on $\mathcal{E}$. Hence, $Y_{\mathcal{E}}(u_r)=0$ for any $\mathcal{E} \in I_\mathcal{O}^F$ but this is in contradiction with [ref] so we must have $I_i \neq \emptyset$ for $i \in \{1, \dots, d \}$. Hence for each $i \in \{1, \dots, d \}$ we chose $\chi_i \in I_i$ such that $(\chi_i^*,\gamma_{u_j})=\delta_{i,j}$ for $j \in \{1, \dots, d \}$.

\end{proof}

\subsection{disconnected center}
\label{sec:non-connected-center}

In [ref], Taylor generalized the theorem [ref] in the case of groups with disconnected center. We will briefly explain how this result was generalized to this case. Suppose that the hypothesis of Theorem [ref] holds except that $\G$ does not have connected center anymore. Let $\tilde{\G}$ be a regular embedding of $\G$ [ref] and $\tilde{G}=\tilde{\G}^F$ the associated finite reductive group. Let $\mathcal{O}$ be a $F$-stable conjugacy class of $\G$ and let us write the partition of $\mathcal{O}^F$ into $\tilde{G}$-conjugacy classes: $\mathcal{O}=\mathcal{\tilde{O}}_1 \sqcup \dots \sqcup \dots \mathcal{\tilde{O}}_d$. And for $i \in \{1, \dots, d \}$, let us decompose $\mathcal{\tilde{O}}_i \cap G = \mathcal{O}_{i,1} \cup \dots \cup \mathcal{O}_{i,k_i}$ into $G$-classes, where $k_i$ depends on $i$.  for each $i \in \{1, \dots, d\}$ and $j \in \{1, \dots, k_i \}$, let $u_{i,j}$ be a representative of $\mathcal{O}_{i,j}$. Using the notations of [ref], for a unipotent element $u \in \tilde{G}$, we will denote by $\gamma_{u}$ the associated GGGR of $G$ and by $\tilde{\gamma}_{u}$ the associated GGGR of $\tilde{G}$. Recall that  $\tilde{\gamma}_u=\mbox{Ind}_G^{\tilde{G}}(\gamma_u)$.

According to [ref], we can find a representative $u \in \mathcal{O}^F$ such that $F$ acts trivially on $A_{\tilde{\G}}(u)$. By [ref], the $\tilde{G}$-classes of $\mathcal{O}^F$ are in bijection with $F$-classes of $A_{\tilde{\G}}(u)$ and since $A_{\tilde{\G}}(u)$ is an abelien group on which $F$ acts trivially, we have that $d=A_{\tilde{\G}}(u)$. Taylor showed in [ref], by a case by case analysis, the existence a special element $\tilde{g}=\tilde{s}\tilde{v} \in \tilde{G}^*$ satisfying the hypothesis of \ref{sec:character-sheaves-2} 3) and many other properties. Hence, we can use \ref{sec:theorem-gggrs}  to exhibit irreducible characters $\tilde{\chi}_{1,1}, \dots , \tilde{\chi}_{d,1} \in \mathcal{E}(\tilde{G},\tilde{s})_{\tilde{v}}$ such that $(\tilde{\chi}^*_{i,1}, \tilde{\gamma}_{u_{x,1}})=\delta_{i,x}$ for $i,x \in \{1, \dots, d \}$ and using Frobenius reciprocity and the fact that $\tilde{\gamma}_{u_{x,1}}=\mbox{Ind}_G^{\tilde{G}}(\gamma_{u_{x,1}})$ we have $\delta_{i,x}=(\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i,1}),\gamma_{u_{x,1}})=1$. 

Using the property P5 in [ref], it can be proved that the number of irreducible components of $\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i,1}) $ is $|Z_\G(u)^F|$ where $Z_\G(u)$ is the image of $C_\G(u)$ in $A_\G(u)$. Moreover, according to [ref], we can write

$$\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i,1})=\chi_{i,1}+ \dots \chi_{i,|Z_\G(u)^F|}$$ 

Where the right part is a sum of distinct irreducible characters of $G$. Hence, using

$$\delta_{i,x}=\sum \limits_{j=1}^{|Z_\G(u)^F|}(\chi^*_{i,j},\gamma_{u_{x,1}})$$

We can suppose for more convenience, that $(\chi_{i,j}^*,\gamma_{u_{i,1}})=\delta_{j,1}$. According to [ref], we can chose $t_{i,1}=1, \dots, t_{i,|Z_\G(u)^F|} \in \tilde{T}$ such that $\chi_{i,j}^*=\chi_{i,1}^{* t_{i,j}}$. Using [ref], we have
$$1=(\chi^*_{i,j},\gamma_{u_{i,1}})=(\chi^{*t_{i,j}}_{i,1},\gamma_{u_{i,1}}^{t_{i,j}})=(\chi^*_{i,j},\gamma_{t_{i,j}u_{i,1}t_{i,j}^{-1}})$$. Moreover, it can be proved that if $j \neq k$, $t_{i,j}u_{i,1}t_{i,j}^{-1}$ and $t_{i,k}u_{i,1}t_{i,k}^{-1}$ are in distinct $G$-classes, hence $k_i \leq |Z_\G(u)^F|$. We can use the inequality below to deduce that $k_i=|Z_\G(u)^F|$:

$$|A_\G(u)^f=|Z_\G(u)^F|A_{\tilde{G}}(u)|=d|Z_\G(u)^F|\leq \sum \limits_{i=1}^d k_i=|A_\G(u)^F|$$

where the first equality comes from [ref]. Now, by changing the numbering if necessary, we can suppose that $u_{x,y}=t_{x,y}u_{x,1}t_{x,y}^{-1}$ for $x \in \{1, \dots, d \}$ and $y \in \{1, \dots, |Z_\G(u)^F| \}$. We have $(\chi_{i,j}^*,\gamma_{u_{x,y}})=(\chi_{i,j}^*{t_{x,y}^{-1}},\gamma_{u_{x,1}})$. Since $\chi_{i,j}^*{t_{x,y}^{-1}}$ is an irreducible component of $\mbox{Res}^{\tilde{G}}_G(\tilde{\chi}^*_{i,1})$, we have that $(\chi_{i,j}^*,\gamma_{u_{x,y}})=0$ if $i\neq x$. We can prove simlarly that $(\chi_{i,j}^*,\gamma_{u_{i,y}})$ if and only if $j=y$ and we are done.

\end{document}

