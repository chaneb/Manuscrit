\chapter*{Introduction}
\fancyhead[L]{\thepage}
\fancyhead[R]{\scshape \nouppercase{Introduction}}
\label{cha:introduction}
\adjustmtc \addcontentsline{toc}{chapter}{Introduction}
\subsection*{Representations of finite groups}
\label{sec:repr-finite-groups}


Let $G$ be a finite group and $K$ be a field. A \emph{representation} of $G$ over $K$ is a % finite dimentional $K$-vector space $V$ together with a group morphism $G \to \mbox{GL}(V)$. In other words, a representation is a
finite-dimensional $K$-vector space on which $G$ acts by linear automorphisms. Let $KG$ be the $K$-algebra whose basis consists of elements of $G$ and whose multiplication law is induced by the group law of $G$. Then, representations of $G$ can be viewed as finitely generated $KG$-modules.
Since a representation is a vector space $V$ on which $G$ acts, it is natural to look at the non-zero subvector spaces which are stable by $G$. These are the \emph{subrepresentations} of $G$. The representations without non-zero proper subrepresentation are said to be \emph{irreducible}.

When the field has characteristic zero (\emph{e.g.} $K=\C$), the representation theory of $G$ is much simpler. In that case, by Maschke's theorem, any representation of $V$ over $K$ can be written as a sum 
  $$V=\bigoplus\limits_{i=1}^nV_i,$$
where $V_1, \dots, V_r$ are irreducible subrepresentations of $V$. In other words, the ordinary (characteristic zero) representation theory of $G$ is governed by the irreducible representations. \index{$R_K(G)$}These can be studied via the Grothendieck group $R_K(G)$ of $G$ which is the abelian group such that:
\begin{itemize}
\item Generators are the finitely generated $KG$-modules. We denote by $[V]$ the image of a $KG$-module $V$ in $R_K(G)$.
\item Relations are  $[V]=[V_1]+[V_2]$ whenever we have a short exact sequence $0 \to V_1 \to V \to V_2 \to 0$.
\end{itemize}
Note that we have $R_K(G)=\bigoplus\limits_{V \in \Irr_K(G)} \Z [V]$ where $\Irr_K(G)$ is a set of representatives of isomorphism classes of irreducible representations of $G$ over $K$. Maschke's theorem states that, when $K$ has characteristic zero, the isomorphism class of a finitely generated $K G$-module is completely determined by its image in $R_K(G)$.

For representations over a field of positive characteristic (\emph{modular} representations), the situation is much more complicated. From now on, we assume that $K$ has characteristic $0$, and we denote by $k$ a field of characteristic $\ell$. We will assume that both $K$ and $k$ are algebraically closed. When $\ell$ divides the order of $G$, the image of a representation in $R_k(G)$ determines only its composition factors. However, we can use connections between representations of $G$ over $K$ and $k$ to deduce information on representations over $k$ from what we know on representations over $K$. The first step toward this goal is the decomposition matrix. To a representation $V$ over $K$ (an \emph{ordinary} representation), we can associate some modular representation $\bar{V}$ which is constructed as a "reduction mod $\ell$" of $V$. The  \emph{$\ell$-decomposition map} of $G$ is the map $d: R_K(G) \to R_k(G)$ defined by $d([V])=[\bar{V}]$. Let $\{V_1, \dots, V_n\}$ (resp. $\{W_1, \dots, W_r \}$) be a set of representatives of isomorphism classes of irreducible representations of $G$ over $K$ (resp.\ over $k$). For $j=1, \dots, n$, let us write $d([V_j])=\sum \limits_{i=1}^r d_{i,j}[W_i]$. Then the matrix $(d_{i,j})$ is called the \emph{$\ell$-decomposition matrix} of $G$. Let $\mathcal{B} =\{E_1, \dots, E_r\}$ be a set of $r$ distinct irreducible representations of $G$ over $K$. Let us write the decomposition matrix as follows:


$$(d_{i,j})=\begin{blockarray}{cccc}
    &W_1 & \dots & W_r \\
   \begin{block}{c[ccc]}
  E_1&&&\\
   \vdots&&\mbox{\large $D$}&\\
   E_r&&&      \\
  \cline{1-4}
  \vdots &&\textrm{\Large *}&\\
\end{block} 
\end{blockarray}.
$$

We say that $\mathcal{B}$ is a \emph{basic set} of $G$ if the matrix $D$ is invertible. If in addition $D$ is lower unitriangular, we say that $D$ is a \emph{unitriangular basic set} of $G$. If $\mathcal{B}$ is a unitriangular basic set, $D$ has the following form.

$$D=
\begin{blockarray}{cccccc}
  &W_1 & &\dots& & W_r \\
  \begin{block}{c[ccccc]}
    E_1  & \quad 1 &&&  &   \\
    &&&&\textrm{\Large 0}&\\
    \vdots & & &\ddots &    &\\
    &&\textrm{\Large *}&&&\\
  E_r &&&&&  1 \\
\end{block}
\end{blockarray}.$$
Then we obtain a natural parametrisation of $\Irr_k(G)$ by $\mathcal{B}$ given by $W_i \leftrightarrow E_i$. It is an open problem % to know
whether any group admits a (unitriangular) basic set of ordinary representations.

\subsection*{Finite groups of Lie type}
\label{sec:finite-groups-lie}

Finite groups of Lie type are finite groups arising as rational points of a connected reductive group defined over $\F_q$ where $q$ is a power of a prime number $p$. That construction allows us to transfer the geometric structure of connected reductive groups into finite groups of Lie type: each element has a Jordan decomposition, each finite group of Lie type has a BN-pair, finite groups of Lie type are classified in terms of root data and graph automorphisms. Let us set the notation for finite groups of Lie type. If $\G$ is a connected reductive group defined over $\F_q$ and $F: \G \to \G$ is the corresponding Frobenius map then we denote by $G:=\G^F:=\{g \in \G \mid F(g)=g \}$ the corresponding finite group of Lie type. For example, if we set $\G:=\mbox{GL}_n(\bar{\F}_q)$ and
$$F: \G \to \G, \quad (a_{i,j}) \mapsto (a_{i,j}^q),$$
then $G:=\mbox{GL}_n(q)$ is a finite group of Lie type. More generally, most finite classical groups over finite fields are finite groups of Lie type (the special linear groups, the special orthogonal groups, the symplectic groups, the spin groups, $\dots$). In 1976, by constructing varieties associated to finite reductive group and by studying the $\ell$-adic cohomology attached to those varieties, Deligne and Lusztig constructed virtual representations which we now refer to as \emph{Deligne--Lusztig representations}. In 1984, Lusztig went further and provided a parametrisation of $\Irr_K(G)$. This was done as follows.

\begin{itemize}
\item Lusztig provided a partition of $\Irr_K(G)$ parametrised by semi-simple elements of a dual group. More precisely, to a reductive group $\G$ defined over $\F_q$ we can associate another reductive group $\G^*$ defined over $\F_q$, said to be \emph{dual} to $\G$ which has the dual root datum. We denote by $G^*$ the finite group of Lie type associated to $\G^*$. For example $\mbox{GL}_n(q)$ is self-dual, the dual of $\mbox{SL}_n(q)$ is $\mbox{PGL}_n(q)$. We have a partition.

  $$\Irr_K(G)=\bigsqcup \mathcal{E}(G,s)$$

  where $s$ runs over representatives of conjugacy classes of semi-simple elements of $G^*$ (for instance, if $G=\mbox{GL}_n(q)$, semi-simple elements are diagonalisable matrices). The set $\mathcal{E}(G,s)$ is the \emph{rational Lusztig series} associated to $s$. Elements of $\mathcal{E}(G,1)$ are called \emph{unipotent} representations.

  
\item In \cite{lusztig_g._characters_1984} Lusztig classified unipotent characters of finite groups of Lie type in terms of some unipotent classes of $\G$ (we recall that unipotent classes are conjugacy classes containing upper unitriangular matrices). Using the Springer correspondence, Lusztig defined the notion of special unipotent classes. To each special unipotent class $(u)_\G$ he associated a set $\mathcal{M}(u)$ and showed that $\mathcal{E}(G,1)$ is parametrised by pairs $((u)_\G,x)$ where $(u)_\G$ is an $F$-stable special unipotent conjugacy class of $\G$ and $x \in \mathcal{M}(u)$.
  
\item Finally, Lusztig showed that the study of irreducible characters can be reduced to the study of unipotent characters. More precisely, 
    he constructed a bijection $$\mathcal{E}(G,s) \simeq \mathcal{E}(C_{G^*}(s),1).$$

\end{itemize}
 To summarise, when $Z(\G)$ is connected, irreducible characters can be parametrised by triples $((s),(u),x)$ where $(s)$ is a semi-simple class of the finite group $G^*$, $(u)$ is an $F$-stable special unipotent class of $C_{\G^*}(s)$ and $x \in \mathcal{M}(u)$.

  \subsection*{Unipotent blocks and basic sets}
\label{sec:unip-blocks-basic}

From now on, we work in \emph{non-defining characteristic}, \emph{i.e.} we assume $\ell$ does not divide  $p$. There exists a partition of both $\Irr_K(G)$ and $\Irr_k(G)$ into $\ell$-blocks. The decomposition map preserves the block partition. Therefore, we can study the decomposition matrices and basic sets of $G$ blockwise. Let $s$ be a semi-simple element of $G^*$ of order prime to $\ell$. Let

$$B_s:=\bigcup \mathcal{E}(G,st)$$
where $t$ runs over semi-simple elements of $C_{G^*}(s)$ whose order is a power of $\ell$. Then Broué--Michel showed in \cite{broue_bloc_1989} that 
  $B_s$ is a union of $\ell$-blocks. Blocks contained in $B_1$ are called unipotent $\ell$-blocks.

  In the past 30 years, the study of decomposition matrices and basic sets for finite groups of Lie type has been an active research area. In order to summarise the known results on the subject, we need to introduce the notion of \emph{good prime} and \emph{bad prime} for $\G$. The prime number $\ell$ is good for $\G$ if it does not divide the order of its fundamental group. The prime number $\ell$ is bad otherwise. The behavior of the basic sets and the decomposition matrices of $G$ depends heavily on whether $\ell$ is good or bad for $\G$. In \cite{dipper_decomposition_1985-1}, Dipper showed the existence of a unitriangular basic set for $G$ when $G=\mbox{GL}_n(q)$. Then, Geck generalised the work of Dipper for $\mbox{GU}_n(q)$ by an entirely different method using generalised Gelfand--Graev representations introduced by Kawanaka \cite{kawanaka_generalized_1985}. He even showed that $\mathcal{E}(G,s)$ is a unitriangular basic set for $B_s$. Geck--Hiss and Geck generalised partially those results in \cite{geck_basic_1991, geck_basic_1993} to most cases by showing that $\mathcal{E}(G,s)$ gives a basic set of $B_s$ whenever $\ell$ is good for $\G$ and does not divide the order of $(Z(\G) / Z(\G)^\circ)^F$ where $Z(\G)$ is the center of $\G$. Furthermore, they conjectured in \cite{geck_modular_1997} that when $\ell$ is good, $\mathcal{E}(G,1)$ forms a unitriangular basic set for unipotent blocks.


If $\ell$ is bad, the situation is different. For example, if $G=\mbox{SL}_2(3)$ and $\ell=2$, there are not enough unipotent characters to form a basic set for $B_1$. In \cite{kleshchev_representations_2009}, using Clifford Theory, Kleshchev--Tiep showed the existence of a basic set for SL$_n(q)$. Denoncin in \cite{denoncin_stable_2017} generalised this result to SU$_n(q)$. In \cite{geck_basic_1994}, using generalised Gelfand--Graev representations and results of Lusztig on character sheaves, Geck showed the existence of a basic set for unipotent blocks of classical groups whose center is connected when $\ell=2$. We generalise this result for groups with disconnected center.
\bigskip
\begin{Theb}{}{}
  Let $\G$ be a reductive group, defined over $\F_q$ where $q$ is odd. Assume that $\G$ is simple and of type $B$, $C$ or $D$ except spin of half-spin. Then there exists a unitriangular basic set for unipotent $2$-blocks of $G$.
\end{Theb}
\medskip
The proof, following Geck's ideas can be summarised into three steps.

\begin{itemize}
\item We show that the number of unipotent modular representations equals the number of unipotent classes of $G$.
\item Let $\Gamma_1, \dots, \Gamma_r$ be the generalised Gelfand--Graev representations of $G$. There exist  $\chi_1, \dots, \chi_r \in \Irr_K(G)$ such that $(\langle \chi_i, \Gamma_j \rangle)_{i,j}$ is lower unitriangular. 
\item By using the fact that $\chi_1, \dots, \chi_r$ lie in Lusztig series associated to $2$-elements, we note that they belong to unipotent blocks. Then, by a classical result of modular representation theory, the scalar product property above allows us to conclude that $\chi_1, \dots, \chi_r$ is a unitriangular basic set for $B_1$.
\end{itemize}

\subsection*{Counting unipotent modular representations}
\label{sec:count-unip-modul}

Since unipotent characters form a basic set of $B_1$ when $\ell$ is good and does not divide $|(Z(\G)/Z(\G)^\circ)^F|$, Lusztig's parametrisation of unipotent representations can be used to count unipotent modular representations. If $\ell=2$ and $G$ is classical, unipotent representations can be counted by unipotent classes of the finite group $G$. We introduce a method to count unipotent modular representations generalising previous cases. Then, we conjecture that our method is effective for every finite group of Lie type. Let us give more details about this method. We first generalise the definition of special classes. In \cite{lusztig_g._characters_1984}, using the Springer correspondence and $j$-induction of representations of Weyl groups, Lusztig defined a surjective map $\Phi$ from the set of special conjugacy classes of $\G^*$ onto the set of unipotent classes of $\G$. Let $(u)_\G$ be an $F$-stable unipotent class of $\G$. The class $(u)_\G$ is \emph{$\ell$-special} if there exists $g \in \G^*$ with Jordan decomposition $g=sv$ such that

\begin{itemize}
\item $\Phi((g)_{\G^*})=(u)_\G$,
\item $s$ is an isolated $\ell$-element of $\G^*$ (\emph{i.e.} the order of $s$ is a power of $\ell$ and $C_{\G^*}(s)^\circ$ is not contained in any proper Levi subgroup of $\G^*$),
\item $v$ is special in $C_{\G^*}(s)^\circ$.
\end{itemize}
In particular, special classes are $\ell$-special since the neutral element is an isolated $\ell$-element for all prime $\ell$. If $\ell$ is good and does not divides $|(Z(\G)/Z(\G)^\circ)^F|$, $\ell$-special classes are the special ones. As Lusztig did with special classes, to each $\ell$-special class $(u)_\G$, we associate a set $\mathcal{M}_\ell(u)$ which we conjecture to parametrise a family of unipotent representations over $k$.
\vspace*{0.3cm}

\begin{Conb}{}{}
The number of irreducible unipotent modular representations of $G$ is equal to the number of pairs $((u)_\G,x)$ where $(u)_\G$ is an $F$-stable unipotent $\ell$-special class of $\G$ and $x \in \mathcal{M}_\ell(u)$. 
\end{Conb}

 \vspace*{0.3cm}
The conjecture holds when $\ell$ is good and does not divide the order of $(Z(\G)/Z(\G)^\circ)^F$ since there exists a bijection between $\mathcal{M}_\ell(u)$ and $\mathcal{M}(u)$. We show that the conjecture holds in the following additional cases.

 \vspace*{0.3cm}
\begin{Theb}{}{}
\label{The:count-unip}
  Let $\G$ be a connected reductive group defined over $\F_q$ where $q$ is the power of a good prime number $p$. Assume we are in one of the following cases.
\begin{itemize}
\item $\G$ is $\mathrm{SL}_n(\overbar{\mathbb{F}}_p)$ (hence, $G$ is either $\mathrm{SL}_n(q)$ or $\mathrm{SU}_n(q)$) for any $\ell \leq n$.
\item $\G$ is simple of type $B, C$ or $D$, for $\ell=2$. 
\item $\G$ is a simple exceptional group of adjoint type for any bad prime number $\ell$.
\end{itemize}

Then, the above conjecture holds.

\end{Theb}

\subsection*{Application to groups of small rank}

The last part of the thesis has two goals:

\begin{itemize}
\item Use the result above to exhibit unitriangular basic sets.
\item Use work of Dudas from \cite{dudas_decomposition_2014}, to get small upper bounds on decomposition numbers. 
\end{itemize}

The results of Dudas are especially effective when we have a unitriangular decomposition matrix with a few missing coefficients. We focus on two cases: $G=\mbox{Sp}_4(q)$ and $G_2(q)$. We assume that $q$ is a power of a good prime for $G$.


\paragraph{Case $G=\mathrm{Sp}_4(q)$ and $\ell=2$.} For irreducible representations of $G$ over $K$, we use the notation from Srinivasan's character table of $G$ \cite{srinivasan_characters_1968}. The above theorem on basic sets for classical groups provides a basic set for unipotent blocks of $G$. Moreover, we are able to detect in which Lusztig series and families each representation of this basic set lies. Using White's 2-decomposition matrix of $G$ \cite{white_2-decomposition_1990} and Dudas's result we are able to complete the decomposition matrix.

\begin{The*}
  Assume $\ell=2$ and $q$ is odd. Then, $\mathcal{B}=\{1_G,\theta_3,\theta_4,\theta_9,\theta_{10},\Phi_3,\Phi_4 \}$ is a unitriangular basic set for $B_1$. The decomposition matrix of $B_1$ with respect to $\mathcal{B}$ is

  $$
 \begin{array}{c|ccccccc|}
    & P_1 & P_2 & P_3 & P_4 & P_5 & P_6 & P_7 \\
    \hline
    1_G                   & 1 & & & & & & \\
    \theta_3                    &1& 1 & & & & & \\
    \theta_4                    &1& & 1 & & & &\\
     \theta_9                    &2&1&1&1 & & & \\
     \theta_{10}                  & & & & &1 &  &\\
     \Phi_3                  & &1 & & &x &1 &\\
     \Phi_4                  &  & &1& &x& & 1 \\
     \hline
   \end{array}
   $$

   where $0 \leq x \leq 1$.
    
 \end{The*}
 
\paragraph{Case $G=G_2(q)$ and $\ell=2,3$.}
We do not have a theorem providing a unitriangular basic set for $G$ but our conjecture allows us to detect which irreducible representations we should pick to hypothetically form a basic set for $G$. Using the 2 and 3-decomposition matrices computed by Hiss--Shamash in \cite{Hiss20102BLOCKSA2} and \cite{HISS1990371}, and using the same results of Dudas we get the following result. We use the notation of \cite[\S 13]{carter_finite_1985} for irreducible characters of $G$.

\begin{The*}\item
  
  \begin{enumerate}
  \item Assume $\ell=2$. Then, $$\mathcal{B}_2=\{ \phi_{1,0}, \phi_{1,6}, \phi_{1,3}', \phi_{1,3}'', G_2[1], G_2[-1], G_2[\theta], G_2[\theta^2], \chi_{s,(2,2)} \}$$ is a unitriangular basic set for $B_1$. Here, $\chi_{s,(2,2)}$ denotes a character from a Lusztig series associated to a non-trivial isolated semi-simple 2-element $s$. The characters $G_2[\theta]$ and $G_2[\theta^2]$ lie in blocks of defect zero and the decomposition matrix of the principal block with respect to $\mathcal{B}_2$ is

    $$
    \begin{array}{c|ccccccc|}
      & P_1 & P_2 & P_3 & P_4 & P_5 & P_6 & P_7 \\
     \hline
      \phi_{1,0}                       & 1 & & & & & & \\
      \chi_{s,(2,2)}             & 1 & 1 & & & & & \\
      G_2[-1]         & & & 1 & & & &\\
       G_2[1]           & & & & 1 & & &\\
        \{\phi_{1,3}',\phi_{1,3}'' \} & 1 & & & & 1 & & \\
                            & 1 & & & & & 1 &\\
       \phi_{1,6}                 & 1 & &\alpha & \beta & 1 & 1 & 1 \\
     \hline
   \end{array}
   $$

   where
   \begin{itemize}
   \item $0 \leq \alpha \leq 2$ and $0 \leq \beta \leq (q+2)/3$ if $q=1$ mod 4
   \item $1 \leq \alpha \leq 2$ and $1 \leq \beta \leq (q+2)/3$ if $q=-1$ mod 4.
\end{itemize}

 \item Assume $\ell=3$. Then, $$\mathcal{B}_3=\{\phi_{1,0}, \phi_{1,6}, \phi_{1,3}', \phi_{1,3}'', \phi_{2,2},  G_2[-1],G_2[1], \chi_{t,3} \}$$ is a unitriangular basic set for $B_1$. Here, $\chi_{t,3}$ denotes a character lying in a Lusztig series associated to a non-trivial isolated semi-simple 3-element $t$. The decomposition matrix of $B_1$ with respect to $\mathcal{B}_3$ depends on the value of $q$ mod 3.

   

   \begin{itemize}
   \item Assume $q=1$ mod 3. Then, $G_2[1]$ lies in a block of defect zero and the decomposition matrix of the principal block is

     $$
   \begin{array}{c|ccccccc}
     & P_1 & P_2 & P_3 & P_4 & P_5 & P_6 & P_7 \\
     \hline
      \phi_{1,0}                       & 1 & & & & & & \\
      \chi_{t,3}                       & 1 & 1 & & & & & \\
      G_2[1]            & & & 1 & & & &\\
              \{\phi_{1,3}',\phi_{1,3}'' \} &  &1  & & 1 & & & \\
                            & & & \alpha & & 1& &  \\
              \phi_{2,2}                 &1 & & &  & &1  & \\
    \phi_{1,6}                 & 1 & & \beta&  \gamma &  &1&1 \\
     \hline
   \end{array}
   $$

   
where $1 \leq \alpha \leq 2$, $0 \leq \beta \leq q-2$ and $1 \leq \gamma \leq 2$.
   
 \item Assume $q=-1$ mod 3. Then $\phi_{2,2}$ lies in a block of defect zero and the decomposition matrix of the principal block is
   $$
   \begin{array}{c|ccccccc}
       & P_1 & P_2 & P_3 & P_4 & P_5 & P_6 & P_7 \\
     \hline
     \phi_{1,0}                       & 1 & & & & & & \\
      \chi_{t,3}                       & & 1 & & & & & \\
      G_2[1]            & & & 1 & & & &\\
              \{\phi_{1,3}',\phi_{1,3}'' \} &1 &1 &  & 1& & &\\
                             &1 & &1  & & 1& &  \\
               G_2[-1]                 & & & &  & &1  & \\
      \phi_{1,6}                 & 1 & 1& \alpha &  \beta &1& \gamma &1 \\
     \hline
   \end{array}
   $$

   where $1 \leq \alpha \leq q+1$ and $(\beta, \gamma) \in \{(1,1),(1,2),(2,1)\}$.
 \end{itemize}

  \end{enumerate}
\end{The*}

\subsection*{Outline of the Thesis}
\label{sec:outline-thesis}

This thesis is divided into four chapters. In Chapter 1, we gather useful definitions and results on representations of finite groups of Lie type. We first review general results on representation theory of finite groups, including decomposition matrices, basic sets, blocks and Clifford Theory. Then, we introduce connected reductive algebraic groups and we recall the main results on their structure and their classification. That allows us to define the $\F_q$-structure of a connected reductive algebraic group and define finite groups of Lie type and their main properties. We move on to Deligne--Lusztig varieties and their $\ell$-adic cohomology from which we can define Deligne--Lusztig virtual representations. After listing their principal properties, we define the Lusztig rational series and give the Jordan decomposition theorem. Finally, we state two results explaining how Deligne--Lusztig theory behaves with respect to modular representation theory. The first one is a theorem by Broué--Michel which allows us to define the union of blocks $B_s$. The second one is a theorem of Bonnafé--Rouquier giving, under some restriction on $s$, a Morita equivalence between $B_s$ and unipotent blocks of some Levi subgroup of $G$. It can be viewed as a modular version of the Jordan decomposition.\\

The second chapter is devoted to our conjecture. After introducing families of representations of Weyl group and families of unipotent representations, we give Lusztig's classification theorem in term of families. Then, we introduce the Springer correspondence between unipotent classes of $\G$ and irreducible characters of the Weyl group $W$ of $\G$. The Springer correspondence allows us to define the special unipotent classes of $ \G$ and to interpret Lusztig's classification theorem in terms of special unipotent classes. Then we introduce the $\ell$-special unipotent classes of $\G$ and we conjecture that unipotent modular representations can be counted in terms of these $\ell$-special classes. We show that our conjecture holds for SL$_n(q)$ and SU$_n(q)$ for any $\ell$ using results of Kleshchev--Tiep and Denoncin. Then we focus on simple classical groups, \emph{i.e.} simple groups of type $B$, $C$ or $D$ when $\ell=2$. We show that proving our conjecture is equivalent to proving that the number of unipotent modular representations of $G$ is equal to the number of unipotent classes of $G$. The latter is done by generalising a result of Geck in \cite{geck_basic_1994}. Finally, we show, by a case-by-case analysis that the conjecture holds for simple adjoint exceptional groups.\\

The aim of Chapter 3 is to show the existence of a unitriangular basic set for unipotent blocks of classical groups when $\ell=2$. We show that it is enough to prove the existence of some projectives and irreducible characters such that their scalar product respects the unitriangularity condition mentioned previously. We explain the construction of generalised Gelfand--Graev representations (GGGRs) and we review the properties we will need from them. The GGGRs will give the projective characters we need. Then, we introduce character sheaves of a connected reductive group and local systems on unipotent classes. We give a brief review of Geck--Hézard results \cite{geck_unipotent_2008} on character sheaves which will allow us to show the existence of a basic set for groups with connected center. The next section explains how Taylor generalised Hézard's results to groups with disconnected center using regular embeddings. Finally, in the last section we show the existence of a unitriangular basic set for every classical group with the exception of spin and half-spin groups.\\

In the last chapter, we start by introducing Dudas's result on computation of decomposition numbers. We first recall Rickard's and Bonnafé--Rouquier's results \cite{PMIHES_1994__80__81_0, bonnafe_categories_2003} on cohomology complexes of Deligne--Lusztig varieties. In \cite{dudas_note_2013}, to each element $w$ of the Weyl group, Dudas attached a virtual projective representation $P_w$ from which we can extract information on the decomposition numbers. Then, we focus on the case $G=\mbox{Sp}_4(q)$ and $\ell=2$. After giving some details on the combinatorics of representations of the Weyl group of $G$ and on the Jordan decomposition of some Lusztig series, we state our result on basic sets and decomposition matrices of unipotent blocks of $G$. We use Dudas's method to get a better bound on the remaining unknown coefficient of the decomposition matrix. Finally, we consider the case where $G=\mbox{G}_2(q)$. We review information on isolated elements and irreducible characters of $G$. Then, as we did for Sp$_4(q)$, we give results on basic sets and decomposition matrices of $G$ for $\ell=2$ and 3.

% Local Variables:
% mode: latex
% TeX-master: "main"
% End:
