
\section{Finite groups of Lie type}
\label{sec:finite-groups-lie}

\subsection{Reductive algebraic groups}
\label{sec:algebraic-groups}
We recall some elementary notions related to algebraic groups. The reader can find more details and proofs of most of the results mentioned below in \cite{carter_finite_1985}, \cite{digne_representations_1991}, \cite{geck_introduction_2003} and \cite{malle_linear_2011}.

\paragraph{Definitions and properties.}
\label{sec:def-reductive-groups}



Let $p$ be a prime number, $q=p^a$ be a power of $p$. A \emph{linear algebraic group} over $\bar{\F_q}$ is an affine algebraic variety over $\bar{\F}_q$ equipped with a group structure such that the multiplication and the inverse map are morphisms of algebraic varieties. From now on, every algebraic group that we consider will be linear and over $\bar{\F_q}$. A morphism of algebraic groups is a group morphism between algebraic groups which is also a morphism of varieties.

\begin{Ex}\item
  \begin{enumerate}
  \item The group $(\bar{\F}_q,+)$ is  called the \emph{additive group} and is denoted by $\G_a=\G_a(\bar{\F}_q)$.
  \item The group $(\bar{\F}_q, \cdot)$ is called the \emph{multiplicative group} and is denoted by $\G_m=\G_m(\bar{\F}_q)$.
  \item The group $\mbox{GL}_n:=\mbox{GL}_n(\bar{\F}_q)$ is a linear algebraic group. Moreover, all classical groups SL$_n$, SO$_{n}$, Sp$_{2n}$ are linear algebraic groups.
  \end{enumerate}
\end{Ex}

Let $\G$ be a linear algebraic group. The connected component of $\G$ containing the identity is denoted be $\G^\circ$. An element of $\G$ is \emph{semi-simple} (resp. \emph{unipotent)} if its image in some embedding of $\G$ in $\mbox{GL}_n$ is semi-simple (resp. unipotent). That property does not depend on the embedding. Let $g \in \G$, $g$ has a unique decomposition $g=su$ where $s$ is semi-simple, $u$ is unipotent and $su=us=g$, this is the \emph{Jordan decomposition} of $g$. There is a unique maximal closed connected solvable normal subgroup of $\G$, the \emph{radical} of $\G$, denoted by $R(\G)$.  The set $R_u(\G)$ of unipotent elements of $R(\G)$ is a normal closed connected subgroup of $R(\G)$ called the \emph{unipotent radical} of $\G$. We say that $\G$ is \emph{reductive} if $R_u(\G)=\{1\}$. We say that $\G$ is \emph{semi-simple} if $\G$ is connected and $R(\G)=\{1\}$. Semi-simple groups are reductive. The linear algebraic group $\G$ is \emph{simple} if $\G$ is connected but has no proper closed connected normal non-trivial subgroup.

\begin{Ex}
\item 
  \begin{enumerate}
  \item $\G=\mbox{GL}_n$ is reductive but not semi-simple. Indeed $R(\G)=Z(\G)$ is the subgroup of scalar matrices.
  \item $\G=\mbox{SL}_n$ is semi-simple.
  \end{enumerate}
\end{Ex}


% \fcolorbox{white}{gray}
% {\begin{minipage}{0.9\textwidth}
%   \center{ \textbf{From now on, all algebraic groups will be connected and reductive.}}
% \end{minipage}}

A \emph{torus} of $\G$ is a closed subgroup of $\G$ isomorphic to a product of copies of $\G_m$. A torus of $\G$ is \emph{maximal} if it is not contained in any larger torus, it can be shown that all maximal tori are conjugate under $\G$. For any maximal torus $\T$, we define its \emph{Weyl group} $W:=N_\G(\T)/\T$ . It is a finite group which does not depend on the choice of $\T$. A \emph{Borel subgroup} of $\G$ is a maximal closed connected solvable subgroup of $\G$. Every maximal torus $\T$ is contained in a Borel subgroup $\B$ and all pairs $(\T, \B)$ where $\T$ is a maximal torus in a Borel subgroup $\B$ are conjugate under $\G$.

\medskip

Let us fix a pair $(\T, \B)$ where $\T$ is a maximal torus and $\B$ is a Borel subgroup of $\G$ containing $\T$ and let $\U:=R_u(\B)$. Then $\U$ is normal in $\B$ and $\B$ is the semidirect product of $\U$ by $\T$.

\paragraph{Root subgroups and root data}
\label{sec:root-system}


From now on, unless otherwise stated, we assume that $\G$ is connected and reductive. Let $X:=X(\T):=\mbox{Hom}(\T,\G_m)$ be the group of \emph{characters} of $\T$ and $Y:=Y(\T):=\mbox{Hom}(\G_m, \T)$ be the group of \emph{cocharacters of $\T$}. Those groups are isomorphic to $\Z^n$ where $n=\mbox{dim} \T$. For $\chi \in X$ and $\gamma \in Y$, there exists $m \in \Z$ such that $\chi \circ \gamma (t)=t^m$ for any $t\in \G_m$. Then we can define a $\Z$-bilinear form $X(\T) \times Y(\T) \to \Z$ by setting $(\chi, \gamma)=m$. This form provides a duality between $X$ and $Y$. The Weyl group acts on $X$ and $Y$ as follows:
$$^w\chi(t):=\chi(^w t) \quad \forall w \in W, \chi \in X, t \in \T,$$
$$^w \gamma(\lambda):= ^w(\gamma(\lambda)) \quad \forall w \in W, \gamma \in Y, \lambda \in \mathbf{G}_m.$$
We consider the minimal connected unipotent subgroups of $\G$ normalised by $\T$ (see \cite[0.31]{digne_representations_1991}). Those groups are isomorphic to $\G_a$. Since Aut$(\G_a) \simeq \G_m$, the action of $\T$ on one of those groups induces a character $\alpha$. Distinct subgroups will provide distinct characters. The group corresponding to $\alpha$ will be denoted by $\U_\alpha$. We denote by $\Phi$ the set of characters obtained this way. These are the \emph{roots} of $\G$ with respect to $\T$ and $\U_\alpha$ is the \emph{root subgroup} of $\G$ corresponding to $\alpha$. Let $\B$ be a Borel subgroup of $\G$ containing $\T$ and $\U=R_u(\B)$. Then there exists a subset $\Phi^+$ of $\Phi$ such that $\U=\prod\limits_{\alpha \in \Phi^+} \U_\alpha$, $\Phi^+$ is the set of \emph{positive roots} corresponding to $\B$. We have $\Phi=\Phi^+ \sqcup -\Phi^+$.


 For $\alpha \in \Phi$, there exists a surjective morphism $\phi: \mbox{SL}_2(\bar{\F}_q) \to \langle \U_\alpha, \U_{-\alpha} \rangle$ sending upper (resp. lower) triangular matrices to $\U_{\alpha}$ (resp. $\U_{-\alpha}$) and diagonal matrices into $\T$. The cocharacter sending $\lambda$ to
$$\phi \left(
\begin{pmatrix}
  \lambda & 0 \\
  0 &\lambda^{-1}
\end{pmatrix} \right),$$
denoted by $\alpha^{\vee}$, is the \emph{coroot} of $\alpha$. We denote by $\Phi^\vee:=\{\alpha^\vee \mid \alpha \in \Phi \}$ the set of coroots. The quadruple $(X, \Phi, Y, \Phi^\vee)$ has the following properties:

\begin{itemize}
\item $X$ and $Y$ are free $\Z$-modules in duality.
\item There exists a bijection $\Phi \to \Phi^\vee, \; \alpha \to \alpha^\vee$ such that $(\alpha, \alpha^\vee)=2$ for each $\alpha \in \Phi$ 
\item The maps $s_\alpha: X \to X, \; \chi \mapsto \chi -(\chi,\alpha^\vee)\alpha$ and $s_{\alpha^\vee}: Y \to Y, \; \gamma \to \gamma -(\alpha, \gamma)\alpha^\vee$ satisfy $s_\alpha(\Phi)=\Phi$ and $s_{\alpha^\vee}(\Phi^\vee)=\Phi^\vee$.
\end{itemize}

We say that the quadruple $(X, \Phi, Y, \Phi^\vee)$ is a \emph{root datum}. Let $(X', \Phi', Y', {\Phi'^\vee})$ be another root datum. A  isomorphism  is a group isomorphism $\phi: X \to X'$ such that $\phi(\Phi)=\Phi'$ and such that $\phi^*({\Phi'}^\vee)=\Phi^\vee$ where $\phi^*$ is the adjoint map of $\phi$.


\begin{The}[{\cite[0.45]{digne_representations_1991}}]
  \label{The:classification}
  Two connected reductive algebraic groups are isomorphic if and only if their root data are isomorphic. Every root datum is the root datum of some reductive group.
\end{The}

Let $V:= \Z \Phi \otimes_\Z \R$, we identify $V^*$ with $\Z \Phi^{\vee} \otimes_\Z \R$. For $v \in V$ and $v^* \in V^*$ we set $(v,v^*):=v^*(v)$. Then $(V, \Phi)$ is a \emph{root system.}.
% \begin{itemize}
% \item $\Phi$ generates the $\R$-vector space $V$. 
% \item For $\alpha \in \Phi$,  $(\alpha,\alpha^\vee)=2$ and $\Phi$ is stable by the reflection $s_\alpha: V \to V$ defined by $x \mapsto x -(x, \alpha^\vee)\alpha$. 
% \item $\alpha^\vee(\Phi) \subset \Z$ for any $\alpha \in \Phi$.
% \end{itemize}
The set of positive roots $\Phi^+$ gives a \emph{basis} $\Delta$ of $\Phi$. The basis $\Delta$ is a subset of $\Phi^+$ such that each root can be written uniquely as a linear combination of elements of $\Delta$ with integer coefficients all of the same sign. The roots of $\Delta$ are said \emph{simple}. There is a faithul action of $W$ on $\Phi$ which induces a injective morphism $W \to \mathrm{GL}(V)$ whose image is generated by the $s_\alpha's$ for $\alpha \in \Phi$. For $\alpha  \in \Phi$, we will still denote by $s_\alpha$ the element of $W$ whose image in $\mathrm{GL}(V)$ is $s_\alpha$. The group $W$ is defined by generators and relations as follows. Let $\alpha_1, \dots, \alpha_n$ be the simple roots and $m_{ij}$ be the order of $s_{\alpha_i}s_{\alpha_j}$, then
$$W=\langle s_{\alpha_i},\dots,s_{\alpha_n} \mid s_{\alpha_i}^2=1, \,  (s_{\alpha_i}s_{\alpha_j})^{m_{ij}}=1 \rangle.$$
A root system is \emph{irreducible} if there is no partition of $\Phi$ into orthogonal subsets $\Phi_1$ and $\Phi_2$ such that $\Phi_1$ and $\Phi_2$ are root systems of the spaces they generate. To the root system $\Phi$, we can associate a graph called the \emph{Dynkin diagram} of $\Phi$. The Dynkin diagram has $n$ vertices corresponding to simple roots $\alpha_1, \dots, \alpha_n$. Vertices $i$ and $j$ are connected by $n_{ij}$ edges where $n_{ij}:=(\alpha_i,\alpha_j^\vee)(\alpha_j,\alpha_i^\vee)$. If $n_{ij}=2$ or 3, we attach an arrow from $i$ to $j$ when $(\alpha_j, \alpha_i^\vee) \neq -1$. The root system $\Phi$ is irreducible if and only if its Dynkin diagram is connected. We describe below all Dynkin diagrams of irreducible root systems.

 $$\begin{array}{cccc}
 A_n&  \dynkin{A}{}&\quad  B_n & \dynkin{B}{} \\
 C_n& \dynkin{C}{}& \quad D_n & \dynkin{D}{}\\
 G_2 & \dynkin{G}{2} &\quad F_4 & \dynkin{F}{4}
 \end{array}$$
 $$E_6 \quad \dynkin{E}{6} \quad \quad E_7 \quad \dynkin{E}{7} \quad \quad E_8 \quad\dynkin{E}{8}$$

 \vspace*{0.3cm}


  We say that two reductive groups are \emph{isogenous} it they have same dynkin diagram.
 
\paragraph{Fundamental group.}

Assume that $\G$ is semi-simple and let $\Omega:=\mbox{Hom}(\Z \Phi^\vee, \Z)$. Then $X$ can naturally be viewed as a subgroup of $\Omega$. The groups $\Omega / X$ is the \emph{fundamental group} of $\G$. We say that $\G$ is \emph{adjoint} if $X= \Z \Phi$, in that case the fundamental group is maximal and $Z(\G)=\{1\}$. We say that $\G$ is \emph{simply connected} if the fundamental group is trivial. We denote by $\G_{ad}$ (resp. $\G_{sc}$) an algebraic group with root system $\Phi$ which is adjoint (resp. simply connected). For each semi-simple group $\G$ with root system $\Phi$, there exist morphisms whith central finite kernel

$$\G_{sc} \twoheadrightarrow \G \twoheadrightarrow \G_{ad}.$$
Moreover, the kernel of the second morphism if $Z(\G)$. We list below the adjoint and simply connected groups for each Dynkin diagram of classical type (see \cite[1.11]{carter_finite_1985} for the complete list).

\begin{itemize}
\item Type $A_n$ : $\Omega /\Z \Phi$ is a cyclic group of order $n+1$. Moreover, $\G_{ad}$ is isomorphic to PGL$_{n+1}$ and $\G_{sc}$ is isomorphic to SL$_{n+1}$.
\item Type $B_n$ : $\Omega / \Z \Phi$ is isomorphic to $\Z / 2 \Z$ so the only possibilities are the adjoint and simply connected groups. We have $\G_{ad} \simeq \mbox{SO}_{2n+1}$ and $\G_{sc} \simeq \mbox{Spin}_{2n+1}$.
\item Type $C_n$: $\Omega / \Z \Phi$ is isomorphic to $\Z / 2 \Z$ so the only possibilities are the adjoint and simply connected groups. We have $\G_{ad} \simeq \mbox{PCSp}_{2n}$ and $\G_{sc} \simeq \mbox{Sp}_{2n}$.
\item Type $D_n$:
  $$\Omega / \Z \Phi \simeq  \begin{cases}    
    \Z /2 \Z \times \Z / 2 \Z \mbox{ if $n$ is even,}\\
    \Z / 4 \Z \mbox{ if $n$ is odd.}
\end{cases}$$
Moreover, $\G_{ad} \simeq \mbox{P(CO}_{2n}^\circ)$ and $\G_{sc} \simeq \mbox{Spin}_{2n}$.
\end{itemize}



\paragraph{Parabolic and Levi subgroups. }
\label{sec:parab-levi-subgr}

Assume that $\G$ is connected and reductive. We are now interested in a particular class of subgroups of $\G$. Let $I$ be a subset of $\{1, \dots, n \}$,  the \emph{parabolic subgroup} of $W$ corresponding to $I$ is the group:
$$W_I=\langle (s_{\alpha_i})_{i \in I} \rangle.$$
Let $N_I(\T)$ be the subgroup of $N_\G(\T)$ containing $\T$ such that $N_I (\T) / \T=W_I$. Then the \emph{standard parabolic subgroups} of $\G$ are the groups of the form $\P_I=\B N_I(\T)\B$. A parabolic subgroup of $\G$ is a subgroup of $\G$ conjugate to a standard parabolic subgroup. It can be shown that parabolic subgroups of $\G$ are exactly the closed subgroups of $\G$ containing Borel subgroups. 
Let $\P$ be a parabolic subgroup of $\G$. Then $\P$ has a decomposition into semi-direct product
$\P=\M.R_u(\P)$. Such a subgroup $\M$ is called a \emph{Levi subgroup} of $\P$. It is a connected reductive group whose Weyl group is $W_I$. Set $\Delta_I:= \{ \alpha_i \mid i \in I \}$. Let $V_I$ be the subspace of $V$ generated by $\Delta_I$ and $\Phi_I$ be the set of roots contained in $V_I$.  Then $(V_I, \Phi_I)$ is a root system with Weyl group $W_I$.

\subsection{Finite groups of Lie type}
\label{sec:steinberg-map}


\paragraph{Frobenius endomorphism.}


Let $X \subset \bar{\F}_q^n$ be an affine variety over $\bar{\F}_q$. Let $I\subset \bar{\F}_q[T_1, \dots, T_n]$ be the ideal of polynomials vanishing on $X$ and $A:=\bar{\F}_q[T_1, \dots, T_n]/I$ be the algebra of $X$. 

\begin{Def} Let $F: X \to X$ be a morphism of variety and $F^*:A \to A$ be the corresponding algebra homomorphism. We say that $X$ is \emph{defined over} $\F_q$ (or has an $\F_q$-structure) if the following conditions hold. There exists an $F$ as above such that:
  \begin{itemize}
  \item $F^*$ is injective,
  \item $F^*(A)=\{a^q \mid a \in A \}$,
  \item $\forall a \in A$, there exists $m \geq 1$ such that $(F^*)^m(a)=a^{q^m}$.
  \end{itemize}
  We say that $F$ is the \emph{Frobenius map} corresponding to the $\F_q$-structure of $X$.
\end{Def}

Let $X$, $Y$ be two affine varieties defined over $\F_q$, $F_X$, $F_Y$ be their Frobenius maps and $\varphi:X \to Y$ be a morphism of varieties. We say that $\varphi$ is defined over $\F_q$ (or rational) if $\varphi \circ F_X = F_Y \circ \varphi$. Let $\G$ be a linear algebraic group. We say that $\G$ is defined over $\F_q$ if the affine variety $\G$ is defined over $\F_q$ and the Frobenius map is a group morphism. Let $F$ be a Frobenius map of $\G$, we denote by $G:=\G^F$ the group of fixed points of $\G$ under $F$. The finite groups of the form $G=\G^F$ where $\G$ is a connected reductive group and $F$ is a Frobenius map of $\G$ are called \emph{finite groups of Lie type} or \emph{finite reductive groups}. 

\begin{Ex}
\item 
  \begin{itemize}
  \item Let $\G=\G_m$. The Frobenius endomorphisms of $\G$ are of the form $x \mapsto x^{\varepsilon q}$ where $\varepsilon=\pm 1$. If $\varepsilon=1$,  then $G= \F_q^\times$. If $\varepsilon=-1$, $G$ is the subgroup $\mu_{q+1}$ of $(q+1)$-th roots of unity of $\G_m$.
  \item Let $\G=\G_a$, the only Frobenius endomorphisms of $\G$ are of the form $x \mapsto x^{q}$. Then $G$ is the additive group $(\F_{q},+)$.
  \item Let $\G=\mbox{GL}_n$ and

    $$F_q:\G \to \G, \: (a_{i,j}) \mapsto (a_{i,j}^q).$$ Then $G=\mbox{GL}_n(q)$ is the group of invertible $n$ by $n$ matrices with coefficients in $\F_q$. More generally, classical matrix groups are usually stable by $F_q$ and provide finite groups of Lie type. For example, the groups SL$_n(q)$, SO$_{2n+1}(q)$, Sp$_{2n}(q)$ can be constructed this way.
  \item We keep $\G=\mbox{GL}_n$ but instead of considering the above Frobenius map $F_q$, we will consider the following morphism
    $$F: \G \to \G, \quad A \to F_q(A^{-1})^{tr}.$$
   The morphism $F$ is the composition of $F_q$ with the map sending a matrix to the transpose of its inverse. Then $F$ is a Frobenius map. The finite group $G \leq \mbox{GL}_n(q)$ denoted by GU$_n(q)$ is the \emph{general unitary group} over $\F_{q^2}$. If we consider $\G:=\mbox{SL}_n$ instead of GL$_n$, then $G$, denoted by SU$_n(q)$, is the \emph{special unitary group}.
  \end{itemize}
\end{Ex}

\paragraph{Lang-Steinberg theorem and applications.}

\begin{The}[Lang-Steinberg, {\cite[3.10]{digne_representations_1991}}]
  \label{The:lang-steinb}
  Let $\G$ be a connected reductive group and $F$ be a Frobenius map of $\G$. Then the map $\mathcal{L}: \G \to \G, \; g \mapsto g^{-1}F(g),$ is surjective.
\end{The}



\begin{Cor}[{\cite[3.12,3.21]{digne_representations_1991}}]
  \label{Cor:lang-steinb-orbits}
  Let $\G$ be a connected reductive group defined over $\F_q$, $F$ be a Frobenius map and $X$ be a set on which $\G$ and $F$ act. Assume that $F(g.x)=g.F(x)$ for all $g \in \G$ and  $x \in X$. Let $\mathcal{O}$ be a non-empty $F$-stable $\G$-orbit in $X$. Then
  \begin{enumerate}
  \item The set of fixed points $\mathcal{O}^F$ is non-empty.
  \item There is a well-defined map sending the $G$-orbit of $g.x \in \mathcal{O}^F$ to the $F$-conjugacy class of the image of $\mathcal{L}(g)$ in $\mathrm{Stab}_{\G}(x)/\mathrm{Stab}_\G(x)^\circ$ and this is a one-to-one correspondence.
  \end{enumerate}
\end{Cor}

\begin{Ex}
  \label{Ex:app-lang-steinb}
\item 
  \begin{enumerate}
  \item Let $\mathcal{B}$ be the set of Borel subgroups of $\G$. Then according to Corollary \ref{Cor:lang-steinb-orbits} 1., $\mathcal{B}^F$ is non-empty so there exists an $F$-stable Borel subgroup of $\G$.
  \item Let $\B$ be an $F$-stable Borel subgroup of $\G$, if we apply Corollary \ref{Cor:lang-steinb-orbits} to the set of maximal tori lying in $\B$, we see that $\B$ contains an $F$-stable maximal torus. Such a torus is said to be \emph{maximally split}.
  \item Let $\P$ be an $F$-stable parabolic subgroup of $\G$. Then $\P$ contains an $F$-stable Levi subgroup.
  \item Let $\mathcal{O}=(g)_{\G}$ be an $F$-stable conjugacy class of $\G$. Then $\mathcal{O}^F$ is non-empty. Using Corollary \ref{Cor:lang-steinb-orbits} 2., we can parametrise the $G$-conjugacy classes of $\mathcal{O}^F$ by $F$-conjugacy classes of $C_\G(g)/C_\G(g)^\circ$. In particular, when $C_\G(g)$ is connected all the elements of $\mathcal{O}^F$ are conjugate under $G$.
  \item Let us fix an $F$-stable maximal torus $\T$ of $\G$ and let $W:=N_\G(\T)/\T$ be the associated Weyl group. The $G$-conjugacy classes of $F$-stable maximal tori are parametrised by the $F$-conjugacy classes of $W$. Then an $F$-stable maximal torus $\T_w$ corresponding to the $F$-class of $w \in W$ has the form $^g \T$ where $g^{-1}F(g)=n_w$ and $n_w$ is a representative of $w$ in $N_\G(\T)$. Moreover, $T_w:=\T_w^F$ is isomorphic as a finite group to
    $$ \T^{wF}:=\{t \in \T \mid n_wF(t)n_w^{-1}=t\}.$$ 
  \item Let $\mathbf{H}$ be an $F$-stable closed and connected subgroup of $\G$. Then, $(\G / \mathbf{H})^F \simeq G/H$. Indeed, the natural map $G/H \to (\G/\mathbf{H})^F$ is injective since if $g_1, g_2 \in G$ have the same image, $g_1g_2^{-1} \in H$. By Corollary \ref{Cor:lang-steinb-orbits} 1., any left $\mathbf{H}$-coset contains an $F$-stable element so the map is surjective. 
  \end{enumerate}
\end{Ex}

\paragraph{Classification of finite groups of Lie type.} Let $\G$ be a connected reductive group, $F$ be a Frobenius morphism on $\G$ and $(\T, \B)$ be an $F$-stable maximal torus contained in an $F$-stable Borel subgroup of $\G$, $\Phi$ be the corresponding root system and $\Delta$ the basis of $\Phi$ induced by $\B$. Let $\U:=R_u(\B)=\prod \limits_{\alpha \in \Phi^+} \U_\alpha$. According to \cite[22.2]{malle_linear_2011}, the unipotent group $\U$ is $F$-stable and $F$ induces a permutation of the groups $\U_\alpha$ with the following property: there exists a permutation $\rho$ of $\Phi^+$ such that $F(\U_\alpha)=\U_{\rho(\alpha)}$ if $\alpha \in \Phi^+$. Then $\rho$ induces a graph automorphism of the Dynkin diagram of $\Phi$ and $F$ acts on $X=X(\T)$ and $Y=Y(\T)$ as follows:
$$^F\chi(t)=\chi(F(t)) \quad \forall \chi \in X, t \in \T,$$
$$^F\gamma(\lambda)=F(\gamma(\lambda)) \quad \forall \gamma \in Y, \lambda \in \G_m.$$
We denote by $\rho_X$ the automorphism of $X$ induced by $\rho$. Then, we have $F = q \rho_x^{-1}$.

\begin{The}[{\cite[22.5]{malle_linear_2011}}]
  \label{The:class-finite-groups}
Let $\G$ be a simple algebraic group, $F$ be a Frobenius map of $\G$ and $\rho$ be as above. Then $G$ is determined, up to isomorphism, by $\rho$ and $q$.
\end{The}

We list below the non-trivial graph automorphisms induced by Frobenius maps for connected Dynkin diagrams.

   $$ ^2 A_n \quad  \begin{dynkinDiagram}[]{A}{}\invol{1}{4}\end{dynkinDiagram} \quad \quad \quad     
   ^2 D_n \quad \begin{dynkinDiagram}{D}{}\draw[latex-latex] (root 6) to
      [out=-35,in=35]  (root 5);\end{dynkinDiagram}$$
   $$  ^3 D_4 \quad \begin{dynkinDiagram}{D}{4}                                                                         \draw[latex-] (root 1) to [out=80,in=160] (root 4);
      \draw[latex-] (root 3) to [out=-160,in=-80] (root 1);
      \draw[latex-] (root 4) to  [out=-30,in=30] (root 3); 
  \end{dynkinDiagram}\quad \quad \quad \quad \quad
    ^2 E_6 \quad \begin{dynkinDiagram}{E}{6}\invol{1}{5}\end{dynkinDiagram} $$

\subsection{Isolated classes}
\label{sec:isolated-classes}

\begin{Def}
  Let $\G$ be a connected reductive group, let $g \in \G$ and $g=su$ be its Jordan decomposition. We say that $g$ is \emph{quasi-isolated} if $C_\G(s)$ is not contained in a Levi subgroup of a proper parabolic subgroup of $\G$. The element $g$ is \emph{isolated} if $C_\G(s)^\circ$ is not contained in a Levi subgroup of a proper parabolic subgroup of $\G$.  
\end{Def}

In \cite{bonnafe_quasi-isolated_2005},  Bonnafé gave a complete description of the conjugacy classes of quasi-isolated semi-simple elements in terms of the root datum of $\G$. To simplify, we only consider the case where $\G$ is adjoint and simple. Let $\iota:(\Q / \Z)_{p'} \bijto \overbar{\F}_q^\times$ be an isomorphism and $\tilde{\iota}$ be the composition
$$\begin{tikzcd}%[row sep=large, column sep = large]
  \Q \arrow[r] & \Q/\Z \arrow[r] & (\Q/\Z)_{p'}  \arrow[r, "\iota"] &\overbar{\F}_q^\times.
\end{tikzcd}$$
We define
$$\tilde{\iota}_\T: Y(\T) \otimes_\Z \Q \to \T, \;x \otimes_\Z \lambda \mapsto \lambda(\tilde{\iota}(x)).$$ According to Proposition 25 of \cite[VI]{MR0240238}, there exists a unique root $\tilde{\alpha}$, the \emph{highest root} of $\Phi$,  which we decompose as
$$\tilde{\alpha}=\sum\limits_{\alpha \in \Delta} n_\alpha \alpha, \quad n_\alpha \in \mathbb{N}$$
such that $\sum\limits_{\alpha \in \Delta} n_\alpha$ is maximal. Let $\alpha_0:=-\tilde{\alpha}$ and $\tilde{\Delta}=\Delta \cup \{ \alpha_0 \}$. Let $(w_{\alpha}^\vee)_{\alpha \in \Delta} \subset Y(\T) \otimes_\Z \Q$ be the dual basis of $\Delta \subset X(\T) \otimes_\Z \Q$. The element $w_\alpha^\vee$ is the \emph{fundamental coweight} associated to $\alpha$. By convention, we set $\omega_{\alpha_0}^\vee=0$ and $n_{\alpha_0}=1$. We denote by $N_W(\tilde{\Delta})$ the subgroup of $W$ normalising $\tilde{\Delta}$, \emph{i.e.}:
$$N_W(\tilde{\Delta}):=\{w \in W \mid  w(\tilde{\Delta})=\tilde{\Delta} \}.$$
  Let $\mathcal{Q}$ be the set of subsets $\Omega \subseteq \tilde{\Delta}$ such that the stabiliser of $\Omega$ in $N_W(\tilde{\Delta})$ acts transitively on $\Omega$ and such that $p$ does not divide $|\Omega|$. Given $\Omega \in  \mathcal{Q}$, we define

  $$t_\Omega:=\tilde{\iota}_\T\left (\sum \limits_{\alpha \in \Omega} \frac{1}{n_\alpha|\Omega|} \omega_{\alpha}^\vee \right).$$

  In order to state the classification theorem in a more convenient form we need the following definition.

  \begin{Def} We say that $p$ is \emph{bad} for $\G$ if there exists $\alpha \in \Delta$ such that $p$ divides $n_\alpha$. We say that $p$ is \emph{good} if it is not bad.
\end{Def}
The following table lists the bad primes for simple algebraic groups.


$$\begin{array}{c|ccccccccc}
  \G & A_n & B_n & C_n & D_n & G_2 & F_4 & E_6 & E_7 & E_8\\
  \hline
 \rule{0pt}{15pt} \mbox{bad primes} &&2&2&2&2,3&2,3&2,3&2,3&2,3,5
\end{array}$$

  
\begin{The}[{\cite[5.1]{bonnafe_quasi-isolated_2005}}]
\label{The:isolated-classes-1}
  Assume that $\G$ is adjoint and simple, $p$ is good and does not divides $|N_W(\tilde{\Delta})|$. Then the following holds:
  \begin{itemize}
  \item The map  $\mathcal{Q} \to \T, \Omega \mapsto t_\Omega$ induces a bijection between the set of $N_W(\tilde{\Delta})$-orbits of  $\mathcal{Q}$ and the set of conjugacy classes of quasi-isolated semi-simple elements in $\G$.
  \item For $\Omega \in  \mathcal{Q}$, we have
    \begin{enumerate}
    \item The Weyl group of $C_\G(t_\Omega)^\circ$ is generated by the reflections $s_\alpha$ for $\alpha \in \tilde{\Delta} \setminus \Omega$.
    \item $n_\alpha$ is constant for $\alpha \in \Omega$ and the order of $t_\Omega$ is $n_\alpha |\Omega|$.
    \item $t_\Omega$ is isolated if and only if $|\Omega|=1$.
    \end{enumerate}
  \end{itemize}  
\end{The}


We reproduce \cite{bonnafe_quasi-isolated_2005}, Table 2, describing quasi-isolated elements in simple adjoint classical groups. The second column describes the elements $\Omega$ of  $\mathcal{Q}$, the third one gives the order $o(t_\Omega)$ of $t_\Omega$. We follow the notation of \cite{bonnafe_quasi-isolated_2005}: we write $\Delta=\{\alpha_1,\ldots,\alpha_n \}$. For $\Omega:=\{\alpha_{n/2} \}$ in type $D_n$, the order of $t_\Omega$ in the original table of Bonnafé is 4. However, in \cite[4.1]{taylor_unipotent_2012} Taylor remarked that this element is actually of order 2.

$$\begin{array}{|clccc|}
    \hline
     \G & \Omega & o(t_\Omega) & C_G(t_\Omega)^\circ & \text{Isolated?}  \\
     \hline
   A_n & \{\alpha_{j(n+1)/d}\mid 0 \leq j \leq d-1 \}& d & (A_{(n+1)/d}-1)^d & \text{yes iff } d=1\\   
        & \text{for } d \mid n+1 & & & \\   
        & & & &  \\   
   B_n & \{\alpha_0 \}  & 1 & B_n & \text{yes} \\   
        & \{\alpha_0, \alpha_1 \}  & 2 & B_{n-1} &  \text{no} \\
        & \{\alpha_d \},2 \leq d \leq n  & 2 & D_d \times B_{n-d} & \text{yes} \\
        & & & &  \\
   C_n & \{ \alpha_0 \}  & 1 & C_n & \text{yes} \\   
        & \{ \alpha_d \}, 1 \leq d < n/2  & 2 & C_d \times C_{n-d} & \text{yes} \\
        & \{ \alpha_{n/2} \} \text{ ($n$ even)} & 2 & C_{n/2} \times C_{n/2} & \text{yes} \\
        & \{ \alpha_0, \alpha_n \} & 2 & A_{n-1} & \text{no} \\
        & \{ \alpha_d, \alpha_{n-d} \}, 1 \leq d <n/2 &  4 & (C_d)^2 \times A_{n-2d-1} & \text{no} \\
        & & &  & \\
   D_n & \{\alpha_0 \}  & 1 & D_n & \text{yes} \\
        & \{ \alpha_d \}, 1 \leq d < n/2  & 2 & D_d \times D_{n-d} & \text{yes} \\
        & \{ \alpha_{n/2} \} \text{ ($n$ even)}  & 2 & D_{n/2} \times D_{n/2} & \text{yes} \\   
        & \{ \alpha_d, \alpha_{n-d} \}, 1 \leq d <n/2 & 4 & (D_d)^2 \times A_{n-2d-1} & \text{no} \\
        & \{ \alpha_0, \alpha_1, \alpha_{n-1}, \alpha_n \} & 4 & A_{n-3} & \text{no} \\
        & \{\alpha_0, \alpha_1 \} &  2 & A_{n-1} & \text{no} \\
        & \{\alpha_0, \alpha_{n-1}  \} \text{ ($n$ even)}  & 2 & A_{n-1} & \text{no} \\
        & \{ \alpha_0, \alpha_n \} \text{ ($n$ even)}  & 2 & A_{n-1} & \text{no} \\   
   \hline
   \end{array}$$

   \begin{Rk}
  \label{Rk:isolated-classes-1}
 Let $\G$ be simple, $\G_{ad}$ be the adjoint quotient of $\G$ and $\pi: \G \to \G_{ad}$ the canonical morphism.  By \cite[2.3]{bonnafe_quasi-isolated_2005}, if $g$ is quasi-isolated then $\pi(g)$ is quasi-isolated. Moreover, $g$ is isolated if and only if $\pi(g)$ is isolated. Thus, even if the table above concerns $\G_{ad}$, we can deduce information on quasi-isolated elements of $\G$.
\end{Rk}

   
% Moreover, the quadruple $(X(\T),\Phi,Y(\T),\Phi^v)$ is a \emph{root datum}, ie it has the following properties:

% \begin{itemize}
% \item $X(\T)$ and $Y(\T)$ are two free $\Z$-modules of finite rank in duality with each other.
% \item $(V, \Phi)$ and $(V^*, \Phi^v)$ are root systems.
% \end{itemize}
% Roots, coroots
% Root datum, Root system, structure of Weyl groups

%%%Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
